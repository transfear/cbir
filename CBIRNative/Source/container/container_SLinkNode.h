#ifndef CONTAINER_SLINKNODE_H_
#define CONTAINER_SLINKNODE_H_

namespace container
{

	// For intrusive single-linked containers
	template <class T>
	class SLinkNode
	{
	public:

		SLinkNode() { }

		void SetNext(T* pNext) { mpNext = pNext; }

		T* GetNext() { return mpNext; }
		const T* GetNext() const { return mpNext; }

	private:
		T* mpNext = nullptr;
	};
}

#endif // CONTAINER_SLINKNODE_H_