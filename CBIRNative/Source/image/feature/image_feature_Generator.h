#ifndef IMAGE_FEATURE_GENERATOR_H_
#define IMAGE_FEATURE_GENERATOR_H_

#include "image_feature_GeneratorParams.h"
#include "image/image_JpegDecompressor.h"
#include "image/feature/image_feature_Feature.h"

namespace image::feature
{
	typedef std::pair<Feature, void*> GeneratorResult;

	class Generator
	{
	public:
		Generator(const GeneratorParams& params) : mParams(params) { }
		Generator(Generator&& other) : mParams(std::move(other.mParams)), mJpegDecompressor(std::move(other.mJpegDecompressor)) { }
		Generator& operator=(Generator&& other)
		{
			mParams = std::move(other.mParams);
			mJpegDecompressor = std::move(other.mJpegDecompressor);
			return *this;
		}
		
		GeneratorResult Generate();
		
	private:

		Generator(const Generator& other) = delete;
		Generator& operator=(const Generator&) = delete;

		GeneratorParams   mParams;
		JpegDecompressor  mJpegDecompressor;
	};

}

#endif // IMAGE_FEATURE_GENERATOR_H_