﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using CBIREngine.Source.Utils;

namespace CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Locality
{
	public class GlobalLocalityModel : BaseResultDescriptorModel
	{
		private readonly Region.BaseRegionModel mGlobalRegion;

		public GlobalLocalityModel(ref CBIRNative.DescriptorResult nativeDescript) : base(ref nativeDescript)
		{
			System.Diagnostics.Debug.Assert(nativeDescript.inParams.feature.localityDescription == CBIRNative.eLocalityDescription.kLocalityDescription_Global);
			System.Diagnostics.Debug.Assert(nativeDescript.numRegions == 1);

			CBIRNative.RegionResult[] regionArray = MarshalUtils.GetArray<CBIRNative.RegionResult>(nativeDescript.pRegionResults, 1);
			unsafe { mGlobalRegion = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[0], nativeDescript.inParams.feature.pRegionWeights[0]); }
		}

		public override int NumRegions
		{
			get { return 1; }
		}

		public override CBIRNative.eLocalityDescription LocalityType
		{
			get { return CBIRNative.eLocalityDescription.kLocalityDescription_Global; }
		}

		public Region.BaseRegionModel GlobalRegion
		{
			get { return mGlobalRegion; }
		}

		public override Region.BaseRegionModel GetRegionAt(int idx)
		{
			switch (idx)
			{
				case 0:
					return GlobalRegion;
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Invalid weight index: {0}", idx));
			return null;
		}
	}
}
