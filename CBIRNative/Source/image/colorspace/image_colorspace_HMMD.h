#ifndef IMAGE_COLORSPACE_HMMD_H_
#define IMAGE_COLORSPACE_HMMD_H_

#include "CBIRNative.h"
#include "utils/utils_basictypes.h"
#include "utils/utils_stdlibwrapper.h"
#include "image_colorspace_RGB.h"

namespace image::colorspace
{
	class HMMD
	{
	public:

		using InputOptions = HMMDHistogramParameters;
		enum { NumComponents = 4};

		static void GetDimSize(const InputOptions& inOptions, u32 outDims[NumComponents])
		{
			outDims[0] = inOptions.numBucketsH;
			outDims[1] = inOptions.numBucketsMin;
			outDims[2] = inOptions.numBucketsMax;
			outDims[3] = inOptions.numBucketsD;
		}

		static u32 GetVolumeSize(const InputOptions& inOptions)
		{
			return static_cast<u32>(inOptions.numBucketsH * inOptions.numBucketsMin * inOptions.numBucketsMax * inOptions.numBucketsD);
		}

		template <typename SrcColorSpace>
		static void U8ToF32(const u8* inRGB, f32 outHMMD[NumComponents]);
	};

	template <>
	static inline void HMMD::U8ToF32<RGB>(const u8* inRGB, f32 outHMMD[HMMD::NumComponents])
	{
		// See https://en.wikipedia.org/wiki/HSL_and_HSV

		const f32 fR = static_cast<f32>(inRGB[0]) / 255.0f;
		const f32 fG = static_cast<f32>(inRGB[1]) / 255.0f;
		const f32 fB = static_cast<f32>(inRGB[2]) / 255.0f;

		const f32 fMin   = std::min(fR, std::min(fG, fB));
		const f32 fMax   = std::max(fR, std::max(fG, fB));
		const f32 fDelta = fMax - fMin;

		outHMMD[1] = fMin;
		outHMMD[2] = fMax;
		outHMMD[3] = fDelta;

		if (fMin == fMax)
		{
			// Gray image
			outHMMD[0] = 0;
		}
		else
		{
			f32 fD, fH;
			if (fR == fMin)
			{
				fD = fG - fB;
				fH = 3.0f;
			}
			else if (fB == fMin)
			{
				fD = fR - fG;
				fH = 1.0f;
			}
			else
			{
				fD = fB - fR;
				fH = 5.0;
			}

			const f32 fChroma = (fH - fD / fDelta) / 6.0f;
			outHMMD[0] = fChroma;
		}
	}
}

#endif // IMAGE_COLORSPACE_HMMD_H_