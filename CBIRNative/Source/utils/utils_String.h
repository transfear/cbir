#ifndef UTILS_STRING_H_
#define UTILS_STRING_H_

#include "utils_basictypes.h"
#include "utils_stdlibwrapper.h"

namespace utils
{
	template <typename T = char>
	class StringT
	{
	public:

		StringT();
		StringT(const T* toCopy);
		StringT(const StringT<T>& toCopy);

		~StringT();

		u64 GetLength() const;

		T* GetRaw();
		const T* GetRaw() const;

		operator T*();
		operator const T*() const;

		void Append(const T* toCopy);

		const bool EndsWith(const T* end) const;
		const bool EndsWithI(const T* end) const;


	private:

		static bool CaseInsensitiveCompare(const T& a, const T& b);

		T*  mRaw;
		u64 mLength;

	};

	// Useful typedefs
	typedef StringT<char> String;

	typedef std::vector<String> StringVector;
	typedef std::stack<String>  StringStack;

	typedef std::unique_ptr<String> StringPtr;
	typedef std::vector<StringPtr>  StringPtrVector;
	typedef std::stack<StringPtr>   StringPtrStack;
}

#endif // UTILS_STRING_H_