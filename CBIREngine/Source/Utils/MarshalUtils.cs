﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CBIREngine.Source.Utils
{
	public static class MarshalUtils
	{
		public static T[] GetArray<T>(IntPtr aTblPtr, uint nRows)
		{
			var entrySize = Marshal.SizeOf(typeof(T));
			IntPtr oneRowPtr = new IntPtr(aTblPtr.ToInt64());
			T[] array = new T[nRows];

			for (uint i = 0; i < nRows; i++)
			{
				array[i] = (T)Marshal.PtrToStructure(oneRowPtr, typeof(T));
				oneRowPtr = new IntPtr(oneRowPtr.ToInt64() + entrySize);
			}

			return array;
		}
	}
}
