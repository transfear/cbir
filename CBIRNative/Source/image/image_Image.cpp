#include "image_Image.h"

namespace image
{
	void Image::Reset(u8* pRawData, u32 width, u32 height, u8 numComponents)
	{
		mRawData.reset(pRawData);
		mWidth = width;
		mHeight = height;
		mNumComponents = numComponents;
	}


	u32 Image::GetWidth() const
	{
		return mWidth;
	}

	u32 Image::GetHeight() const
	{
		return mHeight;
	}

	u8 Image::GetNumComponents() const
	{
		return mNumComponents;
	}

	const u8* Image::GetRawData() const
	{
		return mRawData.get();
	}
}