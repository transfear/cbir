﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.Locality
{
	public abstract class BaseLocalityModel : BaseModel
	{
		public abstract CBIRNative.eLocalityDescription LocalityDescription { get; }
		public abstract int NumWeights { get; }
		public abstract float GetWeightAt(int idx);

		public static BaseLocalityModel CreateFromEnum(CBIRNative.eLocalityDescription enumValue)
		{
			switch (enumValue)
			{
				case CBIRNative.eLocalityDescription.kLocalityDescription_Global:
					{
						return new GlobalLocalityModel();
					}
				case CBIRNative.eLocalityDescription.kLocalityDescription_CenterAndCorners:
					{
						return new CenterAndCornersLocalityModel();
					}
				case CBIRNative.eLocalityDescription.kLocalityDescription_3x3:
					{
						return new ThreeByThreeLocalityModel();
					}
				case CBIRNative.eLocalityDescription.kLocalityDescription_3Horizontal:
					{
						return new ThreeHorizontalLocalityModel();
					}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported locality format: {0}", enumValue.ToString()));
			return null;
		}

		public static BaseLocalityModel CreateFromParams(ref CBIRNative.FeatureDescriptor nativeDescript)
		{
			CBIRNative.eLocalityDescription enumValue = nativeDescript.localityDescription;
			switch (enumValue)
			{
				case CBIRNative.eLocalityDescription.kLocalityDescription_Global:
					{
						return new GlobalLocalityModel(ref nativeDescript);
					}
				case CBIRNative.eLocalityDescription.kLocalityDescription_CenterAndCorners:
					{
						return new CenterAndCornersLocalityModel(ref nativeDescript);
					}
				case CBIRNative.eLocalityDescription.kLocalityDescription_3x3:
					{
						return new ThreeByThreeLocalityModel(ref nativeDescript);
					}
				case CBIRNative.eLocalityDescription.kLocalityDescription_3Horizontal:
					{
						return new ThreeHorizontalLocalityModel(ref nativeDescript);
					}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported locality format: {0}", enumValue.ToString()));
			return null;
		}
	}
}
