#ifndef UTILS_ITERATOR_MEMORYITERATOR_H_
#define UTILS_ITERATOR_MEMORYITERATOR_H_

#include "utils/utils_stdlibwrapper.h"

namespace utils::iterator
{
	template <class Container>
	class ConstMemoryIterator
	{
	public:
		using iterator_category = std::random_access_iterator_tag;
		using value_type        = typename Container::value_type;
		using difference_type   = typename Container::difference_type;
		using pointer           = typename Container::const_pointer;
		using reference         = const value_type&;
		using _Tptr             = typename Container::pointer;
		
		ConstMemoryIterator() : mPtr()  { }
		ConstMemoryIterator(_Tptr arg) : mPtr(arg) { }

		reference operator*() const { return *mPtr; }

		pointer operator->() const
		{	
			return (std::pointer_traits<pointer>::pointer_to(**this));
		}

		ConstMemoryIterator& operator++()
		{
			++mPtr;
			return *this;
		}
		
		ConstMemoryIterator operator++(int)
		{
			ConstMemoryIterator temp = *this;
			++*this;
			return temp;
		}
		
		ConstMemoryIterator& operator--()
		{
			--mPtr;
			return *this;
		}

		ConstMemoryIterator operator--(int)
		{
			ConstMemoryIterator temp = *this;
			--*this;
			return temp;
		}

		ConstMemoryIterator& operator+=(difference_type offset)
		{
			mPtr += offset;
			return *this;
		}

		ConstMemoryIterator operator+(difference_type offset) const
		{	
			ConstMemoryIterator temp = *this;
			return (temp += offset);
		}

		ConstMemoryIterator& operator-=(difference_type offset)
		{
			return (*this += -offset);
		}

		ConstMemoryIterator operator-(difference_type offset) const
		{
			ConstMemoryIterator temp = *this;
			return (temp -= offset);
		}

		difference_type operator-(const ConstMemoryIterator& other) const
		{
			return (mPtr - other.mPtr);
		}

		reference operator[](difference_type offset) const
		{
			return (*(*this + offset));
		}

		bool operator==(const ConstMemoryIterator& other) const
		{
			return (mPtr == other.mPtr);
		}

		bool operator!=(const ConstMemoryIterator& other) const
		{
			return (!(*this == other));
		}

		bool operator<(const ConstMemoryIterator& other) const
		{
			return (mPtr < other.mPtr);
		}

		bool operator>(const ConstMemoryIterator& other) const
		{
			return (other < *this);
		}

		bool operator<=(const ConstMemoryIterator& other) const
		{
			return (!(other < *this));
		}

		bool operator>=(const ConstMemoryIterator& other) const
		{
			return (!(*this < other));
		}

	private:
		_Tptr mPtr;	// pointer to element
	};

	template <class Container>
	class MemoryIterator : public ConstMemoryIterator<Container>
	{
	public:

		using iterator_category = std::random_access_iterator_tag;
		using value_type        = typename Container::value_type;
		using difference_type   = typename Container::difference_type;
		using pointer           = typename Container::pointer;
		using reference         = value_type&;
		using base_class        = ConstMemoryIterator<Container>;

		MemoryIterator() { }
		MemoryIterator(pointer arg) : base_class(arg) { }

		reference operator*() const
		{
			return ((reference)**(base_class *)this);
		}

		pointer operator->() const
		{
			return (pointer_traits<pointer>::pointer_to(**this));
		}

		MemoryIterator& operator++()
		{
			++*(base_class *)this;
			return (*this);
		}

		MemoryIterator operator++(int)
		{
			MemoryIterator temp = *this;
			++*this;
			return temp;
		}

		MemoryIterator& operator--()
		{
			--*(base_class*)this;
			return *this;
		}

		MemoryIterator operator--(int)
		{
			MemoryIterator temp = *this;
			--*this;
			return (temp);
		}

		MemoryIterator& operator+=(difference_type offset)
		{
			*(base_class *)this += offset;
			return (*this);
		}

		MemoryIterator operator+(difference_type offset) const
		{
			MemoryIterator temp = *this;
			return (temp += offset);
		}

		MemoryIterator& operator-=(difference_type offset)
		{
			return (*this += -offset);
		}

		MemoryIterator operator-(difference_type offset) const
		{
			MemoryIterator temp = *this;
			return (temp -= offset);
		}

		difference_type operator-(const base_class& other) const
		{
			return (*(base_class *)this - other);
		}

		reference operator[](difference_type offset) const
		{
			return (*(*this + offset));
		}
	};

	template<class Container>
	inline ConstMemoryIterator<Container> operator+(typename ConstMemoryIterator<Container>::difference_type offset, ConstMemoryIterator<Container> iter)
	{
		return (iter += offset);
	}

	template<class Container>
	inline MemoryIterator<Container> operator+(typename MemoryIterator<Container>::difference_type offset, MemoryIterator<Container> iter)
	{
		return (iter += offset);
	}
}

#endif // UTILS_ITERATOR_MEMORYITERATOR_H_
