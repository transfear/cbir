﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CMD = CBIREngine.Source.Command.ImageComparison.InputParams.FeatureDescriptor.Locality;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.Locality;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor.Locality
{
	public class GlobalLocalityViewModel : BaseLocalityViewModel
	{
		private readonly MDL.GlobalLocalityModel mModel;
		public GlobalLocalityViewModel(MDL.GlobalLocalityModel model, CMD.ChangeLocalityCommand changeLocalityCmd) : base(model, changeLocalityCmd)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
		}

		#region Databindings
		public float Weight
		{
			get { return mModel.Weight; }
			set
			{
				mModel.Weight = value;
				OnPropertyChanged();
			}
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Weight":
					{
						OnPropertyChanged(e.PropertyName);
						break;
					}
			}
		}
		#endregion
	}
}
