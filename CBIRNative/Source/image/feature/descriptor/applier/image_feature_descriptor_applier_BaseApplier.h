#ifndef IMAGE_FEATURE_DESCRIPTOR_BASEAPPLIER_H_
#define IMAGE_FEATURE_DESCRIPTOR_BASEAPPLIER_H_

namespace image
{
	class Image;
	namespace feature
	{
		class Feature;
	}
}

#include "utils/utils_stdlibwrapper.h"

namespace image::feature::descriptor::applier
{
	class BaseApplier
	{
	public:
		virtual ~BaseApplier() { }
		virtual void Apply(const image::Image& inputImage, image::feature::Feature& outputFeature) const = 0;
		virtual u32 GetRegionCount() const = 0;

		u32 GetFeatureCount() const
		{
			return muiNumFeatures;
		}

	protected:
		u32 muiNumFeatures = 0;
	};
}

#endif // IMAGE_FEATURE_DESCRIPTOR_BASEAPPLIER_H_