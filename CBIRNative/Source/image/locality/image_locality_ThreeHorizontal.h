#ifndef IMAGE_LOCALITY_THREEHORIZONTAL_H_
#define IMAGE_LOCALITY_THREEHORIZONTAL_H_

#include "utils/utils_basictypes.h"
#include "utils/utils_stdlibwrapper.h"

namespace image::locality
{
	class ThreeHorizontal
	{
	public:
		enum
		{
			kNumRegions = 3
		};

		static constexpr u32 GetRegionIndex(u32, u32 y, u32, u32 h)
		//static constexpr u32 GetRegionIndex(u32 x, u32 y, u32 w, u32 h)
		{
			const u32 firstBoundY = h / 3;
			const u32 secndBoundY = 2 * firstBoundY;

			if (y < firstBoundY)
				return 0u;

			return y < secndBoundY ? 1u : 2u;
		}
	};
}

#endif // IMAGE_LOCALITY_THREEHORIZONTAL_H_