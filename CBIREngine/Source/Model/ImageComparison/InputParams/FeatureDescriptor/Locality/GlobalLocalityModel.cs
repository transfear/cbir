﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.Locality
{
	public class GlobalLocalityModel : BaseLocalityModel
	{
		private float mfWeight = 1.0f;

		public GlobalLocalityModel() { }
		public GlobalLocalityModel(ref CBIRNative.FeatureDescriptor featureDescriptor)
		{
			unsafe { mfWeight = featureDescriptor.pRegionWeights[0]; }
		}

		public float Weight
		{
			get { return mfWeight; }
			set
			{
				mfWeight = value;
				OnPropertyChanged();
			}
		}

		public override CBIRNative.eLocalityDescription LocalityDescription
		{
			get { return CBIRNative.eLocalityDescription.kLocalityDescription_Global; }
		}

		public override int NumWeights
		{
			get { return 1; }
		}

		public override float GetWeightAt(int idx)
		{
			System.Diagnostics.Debug.Assert(idx == 0, String.Format("Invalid weight index: {0}", idx));
			return Weight;
		}
	}
}
