#ifndef IMAGE_JOB_COMPARE_H_
#define IMAGE_JOB_COMPARE_H_

#include "CBIRNative.h"
#include "core/core_RelayThread.h"
#include "core/job/core_job_GroupLock.h"
#include "core/job/core_job_Job.h"
#include "core/job/core_job_JobSystem.h"
#include "image/distance/image_distance_Comparator.h"

namespace image::job
{
	class Compare : public core::job::Job
	{
	public:
		using Relay = core::RelayThread<CompareResult, core::job::JobSysCancelToken&>;

		template <typename ParamsContainer>
		Compare(const CompareOptions& opt, const char* srcImage, const ParamsContainer& paramsVec, Relay& relayThread, core::job::GroupLock& groupLock);

		virtual ~Compare();

	private:
		Compare(const Compare& other) = delete;
		Compare& operator=(const Compare&) = delete;

		virtual void ExecuteInternal() override;

		const CompareOptions&             mCompareOpt;
		const char*                       mSrcImage;
		Relay&                            mRelayThread;
		core::job::GroupLock&             mGroupLock;
		image::distance::ComparatorVector mComparators;
	};
}

#endif // IMAGE_JOB_COMPARE_H_