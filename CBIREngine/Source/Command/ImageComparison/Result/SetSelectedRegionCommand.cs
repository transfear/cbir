﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using VM = CBIREngine.Source.ViewModel.ImageComparison.Result;

namespace CBIREngine.Source.Command.ImageComparison.Result
{
	public class SetSelectedRegionCommand : ICommand
	{
		private readonly VM.ResultViewModel mVM;

		public event EventHandler CanExecuteChanged;

		public SetSelectedRegionCommand(VM.ResultViewModel vm)
		{
			mVM = vm;
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			VM.Descriptor.Region.BaseRegionViewModel selectedRegion = parameter as VM.Descriptor.Region.BaseRegionViewModel;
			mVM.SelectedRegion = selectedRegion;
		}

		public void RaiseCanExecuteChanged()
		{
			if (CanExecuteChanged != null)
			{
				CanExecuteChanged(this, EventArgs.Empty);
			}
		}
	}
}
