﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using CMD = CBIREngine.Source.Command.ImageComparison.InputParams.DistanceDescriptor;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams.DistanceDescriptor;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.DistanceDescriptor
{
	public abstract class BaseDistanceDescriptorViewModel : BaseViewModel
	{
		private readonly MDL.BaseDistanceDescriptorModel mModel;
		private readonly CMD.ChangeDistanceDescriptorCommand mChangeDescriptCommand;    // Owned by parent DescriptorParamsViewModel

		public BaseDistanceDescriptorViewModel(MDL.BaseDistanceDescriptorModel model, CMD.ChangeDistanceDescriptorCommand changeDescriptCmd)
		{
			mChangeDescriptCommand = changeDescriptCmd;

			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
		}

		public MDL.BaseDistanceDescriptorModel Model
		{
			get { return mModel; }
		}

		#region DataBindings
		public CBIRNative.eDistanceMethod DistanceType
		{
			get { return mModel.DistanceMethod; }
			set
			{
				if (value != mModel.DistanceMethod)
					mChangeDescriptCommand.Execute(value);
			}
		}

		// temp code, to remove
		public string Meh
		{
			get { return mModel.DistanceMethod.ToString(); }
		}
		#endregion

		#region HelperFunctions
		public static BaseDistanceDescriptorViewModel CreateFromModel(MDL.BaseDistanceDescriptorModel model, CMD.ChangeDistanceDescriptorCommand changeDescriptCommand)
		{
			switch (model.DistanceMethod)
			{
				case CBIRNative.eDistanceMethod.kDistanceMethod_ChiSquared:
					{
						return new ChiSquaredDistanceViewModel(model as MDL.ChiSquaredDistanceModel, changeDescriptCommand);
					}
				case CBIRNative.eDistanceMethod.kDistanceMethod_EarthMover:
					{
						return new EarthMoverDistanceViewModel(model as MDL.EarthMoverDistanceModel, changeDescriptCommand);
					}
				case CBIRNative.eDistanceMethod.kDistanceMethod_Euclidean:
					{
						return new EuclideanDistanceViewModel(model as MDL.EuclideanDistanceModel, changeDescriptCommand);
					}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported distance descriptor: {0}", model.DistanceMethod.ToString()));
			return null;
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
		}
		#endregion
	}
}
