#include "image_feature_descriptor_applier_ColorHistogramApplier.h"
#include "image/colorspace/image_colorspace_RGB.h"
#include "image/colorspace/image_colorspace_HSV.h"
#include "image/colorspace/image_colorspace_HMMD.h"
#include "image/locality/image_locality_Global.h"
#include "image/locality/image_locality_CenterAndCorners.h"
#include "image/locality/image_locality_ThreeByThree.h"
#include "image/locality/image_locality_ThreeHorizontal.h"
#include "math/histo/math_histo_MultiDimHistogram.h"
#include "utils/utils_MemoryStream.h"

// Define local types
// TODO: Should probably be moved elsewhere?
namespace
{
	template <typename T, u32 numElems>
	struct PixelElem
	{
		using ElemType = T;

		PixelElem() { }
		T data[numElems];
		operator const T*() const { return &data[0]; }
		operator T*() { return &data[0]; }

		PixelElem(const PixelElem&) = delete;
		PixelElem(PixelElem&&) = delete;
		PixelElem& operator=(const PixelElem&) = delete;
	};

	template <typename T>
	const T* Make_ConstPixelElem(typename const T::ElemType* rawData)
	{
		return reinterpret_cast<const T*>(rawData);
	}
}

namespace image::feature::descriptor::applier
{
	template <class LocalityClass, class ColorSpace>
	ColorHistogramApplier<LocalityClass, ColorSpace>::ColorHistogramApplier(typename const ColorSpace::InputOptions& params)
	: mParams(params),
	  muiNumBucketsPerHistogram(ColorSpace::GetVolumeSize(params))
	{
		muiNumFeatures = LocalityClass::kNumRegions * muiNumBucketsPerHistogram;
	}

	template <class LocalityClass, class ColorSpace>
	void ColorHistogramApplier<LocalityClass, ColorSpace>::Apply(const image::Image& inputImage, feature::Feature& outputFeature) const
	{
		using InElem  = PixelElem<u8, 3>;
		using OutElem = PixelElem<feat_elem, ColorSpace::NumComponents>;

		// Convert Image RGB to destination color space
		assert(inputImage.GetNumComponents() == 3);
		const u32 w = inputImage.GetWidth();
		const u32 h = inputImage.GetHeight();
		const u32 numPixels  = w * h;
		std::unique_ptr<OutElem[]> dstBuffer(std::make_unique<OutElem[]>(numPixels));

		const InElem*  rgbRawBuffer = Make_ConstPixelElem<InElem>(inputImage.GetRawData());
		      OutElem* dstRawBuffer = dstBuffer.get();
		for (u32 curPixel = 0; curPixel < numPixels; ++curPixel, ++rgbRawBuffer, ++dstRawBuffer)
		{
			ColorSpace::template U8ToF32<image::colorspace::RGB>(*rgbRawBuffer, *dstRawBuffer);
		}

		// Instantiate histograms based on region count, using buckets parameters
		constexpr u32 numRegions = LocalityClass::kNumRegions;
		u32 dimSizes[ColorSpace::NumComponents];
		ColorSpace::GetDimSize(mParams, dimSizes);

		const u32 uiNumBucketsPerHistogram = muiNumBucketsPerHistogram;
		const u32 totalHistoBuckets        = muiNumFeatures;

		using ColorHistogram = math::histo::MultiDimHistogramT<ColorSpace::NumComponents, 0, u32>;
		std::unique_ptr<u32[]> histoData(std::make_unique<u32[]>(totalHistoBuckets));
		ColorHistogram histoArray[numRegions];

		const u32 histoBufSize = sizeof(u32) * totalHistoBuckets;
		u32* pHistoBufStart = histoData.get();
		memset(pHistoBufStart, 0, histoBufSize);
		for (u32 curRegion = 0; curRegion < numRegions; ++curRegion, pHistoBufStart += uiNumBucketsPerHistogram)
		{
			histoArray[curRegion].Init(dimSizes, pHistoBufStart);
		}

		// Populate each histogram with region data
		dstRawBuffer = dstBuffer.get();
		for (u32 y = 0; y < h; ++y)
		{
			for (u32 x = 0; x < w; ++x, ++dstRawBuffer)
			{
				const u32 curRegion = LocalityClass::GetRegionIndex(x, y, w, h);
				ColorHistogram& curHisto = histoArray[curRegion];
				curHisto.AddEntry(*dstRawBuffer);
			}
		}

		// Normalize histograms
		std::unique_ptr<feat_elem[]> normalizedData(std::make_unique<feat_elem[]>(totalHistoBuckets));
		feat_elem* pNormalizedBuffer = normalizedData.get();

		     pHistoBufStart = histoData.get();
		u32* pHistoBufEnd   = pHistoBufStart + uiNumBucketsPerHistogram;
		for (u32 curRegion = 0; curRegion < numRegions; ++curRegion, pNormalizedBuffer += uiNumBucketsPerHistogram, pHistoBufStart = pHistoBufEnd, pHistoBufEnd += uiNumBucketsPerHistogram)
		{
			// Calculate sum for current histogram
			const u64 uiCurSum = std::accumulate(pHistoBufStart, pHistoBufEnd, 0u);

			assert(uiCurSum > 0);
			const feat_elem fInvSum = 1.0f / (float)uiCurSum;
			std::transform(pHistoBufStart, pHistoBufEnd, pNormalizedBuffer,
				[=](const u32 uiCurBucketVal) -> feat_elem
				{
					return static_cast<feat_elem>(uiCurBucketVal) * fInvSum;
				}
			);
		}

		// Setup output feature with histogram data
		assert(outputFeature.GetFeatureCount() == muiNumFeatures);
		outputFeature.SetData(normalizedData);
	}

	template <class LocalityClass, class ColorSpace>
	u32 ColorHistogramApplier<LocalityClass, ColorSpace>::GetRegionCount() const
	{
		return LocalityClass::kNumRegions;
	}

	//--------------------------------------------------------------------------------------
	// Force template instantiation	
	#define COLORHIST_INST(colorspace) \
		template class ColorHistogramApplier<image::locality::Global, colorspace>; \
		template class ColorHistogramApplier<image::locality::CenterAndCorners, colorspace>; \
		template class ColorHistogramApplier<image::locality::ThreeByThree, colorspace>; \
		template class ColorHistogramApplier<image::locality::ThreeHorizontal, colorspace>; \

	COLORHIST_INST(image::colorspace::RGB);
	COLORHIST_INST(image::colorspace::HSV);
	COLORHIST_INST(image::colorspace::HMMD);
}