#include "image_distance_Descriptor.h"
#include "applier/image_distance_applier_Euclidean.h"
#include "applier/image_distance_applier_ChiSquared.h"
#include "applier/image_distance_applier_EarthMover.h"

namespace image::distance
{
	Descriptor::Descriptor(const DistanceDescriptor& descriptOpt, const f32* pfRegionWeights, u32 uiNumRegions)
		: mpfRegionWeights(pfRegionWeights), muiNumRegions(uiNumRegions)
	{
		switch (descriptOpt.distanceMethod)
		{
			case kDistanceMethod_Euclidean:
			{
				mBaseApplier = std::make_unique<applier::Euclidean>();
				break;
			}

			case kDistanceMethod_ChiSquared:
			{
				mBaseApplier = std::make_unique<applier::ChiSquared>();
				break;
			}

			case kDistanceMethod_EarthMover:
			{
				mBaseApplier = std::make_unique<applier::EarthMover>();
				break;
			}

			default:
			{
				assert(false);
			}
		}
	}

	dist_elem Descriptor::Apply(const image::feature::Feature& first, const image::feature::Feature& second, RegionResult* pRegionResults) const
	{
		const u32 uiNumRegions = muiNumRegions;
		assert(uiNumRegions == first.GetRegionCount());
		assert(uiNumRegions == second.GetRegionCount());

		dist_elem dSum = 0.0f;
		for (u32 uiCurRegion = 0; uiCurRegion < uiNumRegions; ++uiCurRegion)
		{
			const dist_elem dRegionDist   = mBaseApplier->Apply(uiCurRegion, first, second);
			const dist_elem fRegionWeight = mpfRegionWeights[uiCurRegion];
			const dist_elem dWeightedDist = dRegionDist * fRegionWeight;

			RegionResult& curRegion = pRegionResults[uiCurRegion];
			curRegion.distance = dRegionDist;

			// TODO: Save other stuff (histogram, etc)

			dSum += dWeightedDist;
		}
		
		return dSum;
	}

}

