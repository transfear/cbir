#ifndef UTILS_MEMORYSTREAM_H_
#define UTILS_MEMORYSTREAM_H_

#include "utils_AnyPtr.h"
#include "utils_basictypes.h"
#include "utils_stdlibwrapper.h"

namespace utils
{
	class MemoryStream
	{
	public:
		MemoryStream() { }

		MemoryStream(void* pBuf, u64 uiStreamSize) : mpData(pBuf), mCursor(pBuf), muiStreamSize(uiStreamSize) { }

		void From(void* pBuf, u64 uiStreamSize)
		{
			mpData = pBuf;
			mCursor.get<void*>() = pBuf;
			muiStreamSize = uiStreamSize;
		}

		void* GetData() const
		{
			return mpData;
		}

		u64 GetStreamSize() const
		{
			return muiStreamSize;
		}

		const AnyPtr& GetCursor() const
		{
			return mCursor;
		}

		template <typename T> void Write(const T& data)
		{
			assert(mCursor.get<u8*>() + sizeof(data) <= reinterpret_cast<u8*>(mpData) + muiStreamSize);
			*mCursor.get<T*>()++ = data;
		}

		void Write(const void* pBuf, u64 size)
		{
			assert(mCursor.get<u8*>() + size <= reinterpret_cast<u8*>(mpData) + muiStreamSize);
			memcpy(mCursor.get<void*>(), pBuf, size);
			mCursor.get<u8*>() += size;
		}

		template <typename T> T Read()
		{
			assert(mCursor.get<u8*>() + sizeof(T) <= reinterpret_cast<u8*>(mpData) + muiStreamSize);
			return *mCursor.get<T*>()++;
		}

		void Read(void* pBuf, u64 size)
		{
			assert(mCursor.get<u8*>() + size <= reinterpret_cast<u8*>(mpData) + muiStreamSize);
			memcpy(pBuf, mCursor.get<void*>(), size);
			mCursor.get<u8*>() += size;
		}

		template <typename T> MemoryStream& operator << (const T& data)
		{
			Write<T>(data);
			return *this;
		}

		template <typename T> MemoryStream& operator >> (T& data)
		{
			data = Read<T>();
			return *this;
		}

	private:

		void*  mpData = nullptr;
		AnyPtr mCursor = AnyPtr(nullptr);
		u64    muiStreamSize = 0;
	};
}

#endif // UTILS_MEMORYSTREAM_H_