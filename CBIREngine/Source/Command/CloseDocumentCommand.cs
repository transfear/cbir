﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using MDL = CBIREngine.Source.Model;
using VM  = CBIREngine.Source.ViewModel;

namespace CBIREngine.Source.Command
{
    public class CloseDocumentCommand : ICommand
	{
		private readonly VM.MainViewModel mVM;
		private readonly MDL.MainModel    mModel;
		public event EventHandler CanExecuteChanged;

		public CloseDocumentCommand(VM.MainViewModel vm)
		{
			mVM = vm;
			mModel = vm.Model;
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			VM.BaseDocumentViewModel docVM    = parameter as VM.BaseDocumentViewModel;
			MDL.BaseDocumentModel    toRemove = docVM.Model;
			mModel.Documents.Remove(toRemove);
		}

		public void RaiseCanExecuteChanged()
		{
			if (CanExecuteChanged != null)
			{
				CanExecuteChanged(this, EventArgs.Empty);
			}
		}
	}
}
