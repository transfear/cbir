﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBIREngine.Source.Model
{
	public class MainModel : BaseModel
	{
		private readonly ObservableCollection<BaseDocumentModel> mDocuments = new ObservableCollection<BaseDocumentModel>();

		public ObservableCollection<BaseDocumentModel> Documents
		{
			get { return mDocuments; }
		}

		public MainModel() { }
	}
}
