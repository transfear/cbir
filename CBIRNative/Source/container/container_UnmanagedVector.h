#ifndef CONTAINER_UNMANAGEDVECTOR_H_
#define CONTAINER_UNMANAGEDVECTOR_H_

#include "utils/utils_stdlibwrapper.h"
#include "utils/iterator/utils_iterator_MemoryIterator.h"

namespace container
{
	template <class T>
	class UnmanagedVector
	{
	public:
		using value_type             = T;
		using pointer                = T*;
		using const_pointer          = const T*;
		using reference              = T&;
		using const_reference        = const T&;
		using size_type              = size_t;
		using difference_type        = ptrdiff_t;
		using iterator               = utils::iterator::MemoryIterator<UnmanagedVector<T>>;
		using const_iterator         = utils::iterator::ConstMemoryIterator<UnmanagedVector<T>>;
		using reverse_iterator       = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		UnmanagedVector(void* pBuf) : mpStart(static_cast<T*>(pBuf)), mpCur(static_cast<T*>(pBuf)) { }
		UnmanagedVector(u32 uiNumToInit, void* pBuf) : mpStart(static_cast<T*>(pBuf)), mpCur(static_cast<T*>(pBuf))
		{
			T* const pEndAt = mpCur + uiNumToInit;
			while (mpCur != pEndAt)
			{
				new (mpCur) T();
				++mpCur;
			}
		}
		UnmanagedVector(u32 uiNumToInit, const T& toCopy, void* pBuf) : mpStart(static_cast<T*>(pBuf)), mpCur(static_cast<T*>(pBuf))
		{
			T* const pEndAt = mpCur + uiNumToInit;
			while (mpCur != pEndAt)
			{
				new (mpCur) T(toCopy);
				++mpCur;
			}
		}

		UnmanagedVector(const UnmanagedVector& other, void* pBuf) : mpStart(static_cast<T*>(pBuf)), mpCur(static_cast<T*>(pBuf))
		{
			T* pCur = other.mpStart;
			while (pCur != other.mpCur)
				emplace_back(pCur);
		}

		UnmanagedVector(UnmanagedVector&& other) : mpStart(other.mpStart), mpCur(other.mpCur)
		{
			other.mpStart = other.mpCur = nullptr;
		}

		UnmanagedVector& operator=(const UnmanagedVector& other)
		{
			clear();

			T* pCur = other.mpStart;
			while (pCur != other.mpCur)
				emplace_back(pCur);

			return *this;
		}

		UnmanagedVector& operator=(UnmanagedVector&& other)
		{
			clear();

			mpStart = other.mpStart;
			mpCur   = other.mpCur;

			other.mpStart = other.mpCur = nullptr;
			return *this;
		}

		~UnmanagedVector()
		{
			clear();
		}

		template <class... Args>
		decltype(auto) emplace_back(Args&&... args)
		{
			new (mpCur) T(std::forward<Args>(args)...);
			++mpCur;
		}

		void push_back(const T& elem)
		{	
			emplace_back(elem);
		}

		void push_back(T&& elem)
		{	
			emplace_back(std::move(elem));
		}

		void pop_back()
		{
			--mpCur;
			mpCur->~T();
		}

		void clear()
		{
			while (mpCur != mpStart)
			{
				--mpCur;
				mpCur->~T();
			}
		}

		void swap(UnmanagedVector& other)
		{
			if (this != std::addressof(other))
			{
				std::swap(mpStart, other.mpStart);
				std::swap(mpCur, other.mpCur);
			}
		}

		T* data()
		{
			return mpStart;
		}

		const T* data() const
		{
			return mpStart;
		}

		iterator begin()
		{
			return iterator(mpStart);
		}

		const_iterator begin() const
		{
			return const_iterator(mpStart);
		}

		iterator end()
		{
			return iterator(mpCur);
		}

		const_iterator end() const
		{
			return const_iterator(mpCur);
		}

		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}

		const_reverse_iterator rbegin() const
		{
			return const_reverse_iterator(end());
		}

		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}

		const_reverse_iterator rend() const
		{
			return const_reverse_iterator(begin());
		}

		const_iterator cbegin() const
		{
			return begin();
		}

		const_iterator cend() const
		{
			return end();
		}

		const_reverse_iterator crbegin() const
		{
			return rbegin();
		}

		const_reverse_iterator crend() const
		{
			return rend();
		}

		bool empty() const
		{
			return mpCur == mpStart;
		}

		size_type size() const
		{
			return static_cast<size_type>(mpCur - mpStart);
		}
		
		T& operator[](const size_type pos)
		{
			return mpStart[pos];
		}

		const T& operator[](const size_type pos) const
		{
			return mpStart[pos];
		}

		T& at(const size_type pos)
		{
			return mpStart[pos];
		}

		const T& at(const size_type pos) const
		{
			return mpStart[pos];
		}

		T& front()
		{
			return *mpStart;
		}

		const T& front() const
		{
			return *mpStart;
		}

		T& back()
		{
			return mpCur[-1];
		}

		const T& back() const
		{
			return mpCur[-1];
		}

	private:

		UnmanagedVector(const UnmanagedVector& other) = delete;

		T* const mpStart;
		T* mpCur;
	};

	template<class T>
	inline void swap(UnmanagedVector<T>& a, UnmanagedVector<T>& b)
	{
		a.swap(b);
	}

	template<class T>
	inline bool operator==(const UnmanagedVector<T>& a, const UnmanagedVector<T>& b)
	{
		return (a.size() == b.size()) && std::equal(a.begin(), a.end(), b.begin());
	}

	template<class T>
	inline bool operator!=(const UnmanagedVector<T>& a, const UnmanagedVector<T>& b)
	{
		return !(a == b);
	}

	template<class T>
	inline bool operator<(const UnmanagedVector<T>& a, const UnmanagedVector<T>& b)
	{
		return std::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end());
	}

	template<class T>
	inline bool operator>(const UnmanagedVector<T>& a, const UnmanagedVector<T>& b)
	{
		return b < a;
	}

	template<class T>
	inline bool operator<=(const UnmanagedVector<T>& a, const UnmanagedVector<T>& b)
	{
		return !(b < a);
	}

	template<class T>
	inline bool operator>=(const UnmanagedVector<T>& a, const UnmanagedVector<T>& b)
	{
		return !(a < b);
	}
}

#endif // CONTAINER_UNMANAGEDVECTOR_H_