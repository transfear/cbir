#include "core_job_JobQueue.h"
#include "utils/utils_stdlibwrapper.h"
#include "concurrent/concurrent_ScopedLock.h"

namespace core::job
{
	JobQueue::JobQueue()
	{

	}

	JobQueue::~JobQueue()
	{
		assert(mpFirst == nullptr);
		assert(mpLast  == nullptr);
	}

	void JobQueue::Enqueue(Job* pJob)
	{
		assert(pJob->GetNext() == nullptr);	// Make sure it's not already queued

		Job* prevLast = mpLast;
		mpLast = pJob;

		if (prevLast == nullptr)
		{
			// Queue is empty
			assert(mpFirst == nullptr);
			mpFirst = mpLast;
		}
		else
		{
			prevLast->SetNext(pJob);
		}
	}

	Job* JobQueue::Dequeue()
	{
		Job* toReturn = mpFirst;
			
		if (toReturn == nullptr)
		{
			// Queue is empty
			assert(mpLast == nullptr);
			return nullptr;
		}

		Job* pNextJob = toReturn->GetNext();

		// Single item in queue
		if (toReturn == mpLast)
		{
			assert(pNextJob == nullptr);
			mpLast = nullptr;
		}
			
		mpFirst = pNextJob;
		return toReturn;
	}
}