﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.Locality
{
	public class ThreeByThreeLocalityModel : BaseLocalityModel
	{
		private float mfTopLeft     = 1.0f / 9.0f;
		private float mfTopCenter   = 1.0f / 9.0f;
		private float mfTopRight    = 1.0f / 9.0f;
		private float mfCenterLeft  = 1.0f / 9.0f;
		private float mfCenter      = 1.0f / 9.0f;
		private float mfCenterRight = 1.0f / 9.0f;
		private float mfBotLeft     = 1.0f / 9.0f;
		private float mfBotCenter   = 1.0f / 9.0f;
		private float mfBotRight    = 1.0f / 9.0f;
		
		public ThreeByThreeLocalityModel() { }
		public ThreeByThreeLocalityModel(ref CBIRNative.FeatureDescriptor featureDescriptor)
		{
			unsafe
			{
				mfTopLeft     = featureDescriptor.pRegionWeights[0];
				mfTopCenter   = featureDescriptor.pRegionWeights[1];
				mfTopRight    = featureDescriptor.pRegionWeights[2];
				mfCenterLeft  = featureDescriptor.pRegionWeights[3];
				mfCenter      = featureDescriptor.pRegionWeights[4];
				mfCenterRight = featureDescriptor.pRegionWeights[5];
				mfBotLeft     = featureDescriptor.pRegionWeights[6];
				mfBotCenter   = featureDescriptor.pRegionWeights[7];
				mfBotRight    = featureDescriptor.pRegionWeights[8];
			}
		}

		public float TopLeft
		{
			get { return mfTopLeft; }
			set
			{
				mfTopLeft = value;
				OnPropertyChanged();
			}
		}

		public float TopCenter
		{
			get { return mfTopCenter; }
			set
			{
				mfTopCenter = value;
				OnPropertyChanged();
			}
		}

		public float TopRight
		{
			get { return mfTopRight; }
			set
			{
				mfTopRight = value;
				OnPropertyChanged();
			}
		}

		public float CenterLeft
		{
			get { return mfCenterLeft; }
			set
			{
				mfCenterLeft = value;
				OnPropertyChanged();
			}
		}

		public float Center
		{
			get { return mfCenter; }
			set
			{
				mfCenter = value;
				OnPropertyChanged();
			}
		}

		public float CenterRight
		{
			get { return mfCenterRight; }
			set
			{
				mfCenterRight = value;
				OnPropertyChanged();
			}
		}

		public float BotLeft
		{
			get { return mfBotLeft; }
			set
			{
				mfBotLeft = value;
				OnPropertyChanged();
			}
		}

		public float BotCenter
		{
			get { return mfBotCenter; }
			set
			{
				mfBotCenter = value;
				OnPropertyChanged();
			}
		}

		public float BotRight
		{
			get { return mfBotRight; }
			set
			{
				mfBotRight = value;
				OnPropertyChanged();
			}
		}
		
		public override CBIRNative.eLocalityDescription LocalityDescription
		{
			get { return CBIRNative.eLocalityDescription.kLocalityDescription_3x3; }
		}

		public override int NumWeights
		{
			get { return 9; }
		}

		public override float GetWeightAt(int idx)
		{
			switch (idx)
			{
				case 0:
					return TopLeft;
				case 1:
					return TopCenter;
				case 2:
					return TopRight;
				case 3:
					return CenterLeft;
				case 4:
					return Center;
				case 5:
					return CenterRight;
				case 6:
					return BotLeft;
				case 7:
					return BotCenter;
				case 8:
					return BotRight;
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Invalid weight index: {0}", idx));
			return -1.0f;
		}
	}
}
