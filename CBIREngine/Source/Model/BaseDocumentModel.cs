﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBIREngine.Source.Model
{
	public abstract class BaseDocumentModel : BaseModel
	{
		//phil3: temp
		static int i = 0;
		public BaseDocumentModel()
		{
			mName = String.Format("New Document {0}", i);
			++i;
		}

		private String mName = "New Document";
		private String mFilePath = String.Empty;

		#region DataBindings
		public String Name
		{
			get { return mName; }
			set
			{
				mName = value;
				OnPropertyChanged();
			}
		}

		public String FilePath
		{
			get { return mFilePath; }
			set
			{
				mFilePath = value;
				OnPropertyChanged();
			}
		}
		#endregion
	}
}
