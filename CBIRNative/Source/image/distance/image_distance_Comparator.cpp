#include "image_distance_Comparator.h"

namespace image::distance
{
	dist_elem Comparator::Compare(RegionResult* pRegionResults)
	{
		const image::feature::GeneratorResult genRes = std::move(mGenerator.Generate());
		
		const Descriptor& descript = mParams.GetDescriptor();
		const image::feature::Feature& reference = mParams.GetRefFeature();
		const image::feature::Feature& toCompare = genRes.first;

		const dist_elem distance = descript.Apply(reference, toCompare, pRegionResults);
		return distance;
	}
}
