﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MDL      = CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Locality;
using RegionVM = CBIREngine.Source.ViewModel.ImageComparison.Result.Descriptor.Region;

namespace CBIREngine.Source.ViewModel.ImageComparison.Result.Descriptor.Locality
{
	public class ThreeHorizontalLocalityViewModel : BaseResultDescriptorViewModel
	{
		private readonly MDL.ThreeHorizontalLocalityModel mModel;

		private readonly RegionVM.BaseRegionViewModel mTopVM;
		private readonly RegionVM.BaseRegionViewModel mCenterVM;
		private readonly RegionVM.BaseRegionViewModel mBotVM;

		public ThreeHorizontalLocalityViewModel(MDL.ThreeHorizontalLocalityModel model, ResultViewModel parent) : base(model, parent)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;

			mTopVM    = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.Top, parent);
			mCenterVM = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.Center, parent);
			mBotVM    = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.Bot, parent);
		}

		public override RegionVM.BaseRegionViewModel GetRegionAt(int idx)
		{
			switch (idx)
			{
				case 0:
					return mTopVM;
				case 1:
					return mCenterVM;
				case 2:
					return mBotVM;
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Invalid region index: {0}", idx));
			return null;
		}

		public RegionVM.BaseRegionViewModel Top
		{
			get { return mTopVM; }
		}
		public RegionVM.BaseRegionViewModel Center
		{
			get { return mCenterVM; }
		}
		public RegionVM.BaseRegionViewModel Bot
		{
			get { return mBotVM; }
		}

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
		}
		#endregion
	}
}
