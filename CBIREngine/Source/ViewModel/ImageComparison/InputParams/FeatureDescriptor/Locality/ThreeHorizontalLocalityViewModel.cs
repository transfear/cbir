﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CMD = CBIREngine.Source.Command.ImageComparison.InputParams.FeatureDescriptor.Locality;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.Locality;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor.Locality
{
	public class ThreeHorizontalLocalityViewModel : BaseLocalityViewModel
	{
		private readonly MDL.ThreeHorizontalLocalityModel mModel;
		public ThreeHorizontalLocalityViewModel(MDL.ThreeHorizontalLocalityModel model, CMD.ChangeLocalityCommand changeLocalityCmd) : base(model, changeLocalityCmd)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
		}

		#region DataBindings
		public float Top
		{
			get { return mModel.Top; }
			set
			{
				mModel.Top = value;
				OnPropertyChanged();
			}
		}

		public float Center
		{
			get { return mModel.Center; }
			set
			{
				mModel.Center = value;
				OnPropertyChanged();
			}
		}

		public float Bot
		{
			get { return mModel.Bot; }
			set
			{
				mModel.Bot = value;
				OnPropertyChanged();
			}
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Top":
				case "Center":
				case "Bot":
					{
						OnPropertyChanged(e.PropertyName);
						break;
					}
			}
		}
		#endregion
	}
}
