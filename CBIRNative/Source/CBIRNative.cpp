
#include "CBIRNative.h"

#include "core/core_CompareEngine.h"

int CompareImage(const CompareOptions* opt, int numWorkers)
{
	core::CompareEngine engine(numWorkers);
	const int parseResult = engine.CompareImage(opt);
	return parseResult;
}
