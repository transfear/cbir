﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CBIREngine.Source.Utils;
using CMD = CBIREngine.Source.Command.ImageComparison;
using MDL = CBIREngine.Source.Model.ImageComparison;

namespace CBIREngine.Source.ViewModel.ImageComparison
{
	public class ImageComparisonViewModel : BaseDocumentViewModel
	{
		private readonly MDL.ImageComparisonModel mModel;
		private readonly CMD.StartStopCommand mStartStopCmd;
		private readonly CMD.Result.OpenResultImageCommand mOpenResultCmd;
		private readonly ObservableRangeCollection<Result.ResultViewModel> mResults = new ObservableRangeCollection<Result.ResultViewModel>();

		private readonly InputParams.InputParamsViewModel mInputParams;
		private Result.ResultViewModel mSelectedResult;

		public ImageComparisonViewModel(MDL.ImageComparisonModel model) : base(model)
		{
			mModel = model;
			mModel.PropertyChanged           += Model_PropertyChanged;
			mModel.Results.CollectionChanged += Model_ResultListChanged;

			mInputParams   = new InputParams.InputParamsViewModel(mModel.InputParams);
			mStartStopCmd  = new CMD.StartStopCommand(this);
			mOpenResultCmd = new CMD.Result.OpenResultImageCommand();
		}

		#region DataBindings
		public new MDL.ImageComparisonModel Model
		{
			get { return mModel; }
		}

		public InputParams.InputParamsViewModel InputParams
		{
			get { return mInputParams; }
		}
		
		public string StartStopBtnText
		{
			get{ return (mModel.Engine == null) ? "Start" : "Stop"; }
		}

		public CMD.StartStopCommand StartStopCmd
		{
			get { return mStartStopCmd; }
		}

		public CMD.Result.OpenResultImageCommand OpenResultCmd
		{
			get { return mOpenResultCmd; }
		}

		public ObservableRangeCollection<Result.ResultViewModel> Results
		{
			get { return mResults; }
		}

		public Result.ResultViewModel SelectedResult
		{
			get { return mSelectedResult; }
			set
			{
				mSelectedResult = value;
				OnPropertyChanged();
			}
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Engine":
					{
						OnPropertyChanged("StartStopBtnText");
						break;
					}
			}
		}

		private void Model_ResultListChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			switch (e.Action)
			{
				case NotifyCollectionChangedAction.Add:
					{
						foreach (MDL.Result.ResultModel newEntry in e.NewItems)
						{
							Result.ResultViewModel vmEntry = new Result.ResultViewModel(newEntry);
							mResults.Add(vmEntry);
						}
						break;
					}

				case NotifyCollectionChangedAction.Remove:
					{
						foreach (MDL.Result.ResultModel oldEntry in e.OldItems)
						{
							Result.ResultViewModel vmEntry = mResults.First(x => x.Model == oldEntry);
							mResults.Remove(vmEntry);

							if (vmEntry == this.SelectedResult)
								this.SelectedResult = null;
						}
						break;
					}

				case NotifyCollectionChangedAction.Reset:
					{
						mResults.Clear();
						ObservableRangeCollection<MDL.Result.ResultModel> srcList = (ObservableRangeCollection<MDL.Result.ResultModel>)sender;
						foreach (MDL.Result.ResultModel newEntry in srcList)
						{
							Result.ResultViewModel vm = new Result.ResultViewModel(newEntry);
							mResults.Add(vm);
						}

						this.SelectedResult = null;
						break;
					}

				default:
					{
						System.Diagnostics.Debug.Assert(false, String.Format("Unhandled action type: {0}", e.Action.ToString()));
						break;
					}
			}
		}
		#endregion
	}
}
