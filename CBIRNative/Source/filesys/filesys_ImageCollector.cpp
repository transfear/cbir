
#include "utils/utils_stdlibwrapper.h"
#include "filesys_ImageCollector.h"
#include "filesys_Utils.h"

namespace filesys
{
	template <typename T>
	ImageCollectorT<T>::ImageCollectorT(const char* path, bool recursive)
		: mRootPath(path), mRecursiveLookup(recursive)
	{

	}

	template <typename T>
	bool ImageCollectorT<T>::CollectImages()
	{
		mCollectedImages.clear();

		// Check if root path is valid
		if (!std::filesystem::is_directory(mRootPath))
			return false;

		::utils::StringPtr rootPath(std::make_unique< ::utils::String>(mRootPath));
		
		::utils::StringPtrStack allPaths;
		allPaths.push(std::move(rootPath));

		// Loop as long as we haven't reach the maximum image count,
		// and that there are other subfolders to analyze
		int  iAddedImages  = 0;
		bool bLimitReached = false;
		while (!bLimitReached && !allPaths.empty())
		{
			::utils::StringPtr curFolder = std::move(allPaths.top());
			allPaths.pop();

			std::filesystem::path curFolderPath(curFolder->GetRaw());
			
			// Loop on all stuff in this folder
			for (const std::filesystem::directory_entry& entry : std::filesystem::directory_iterator(curFolderPath))
			{
				const std::filesystem::path& curElem = entry.path();
				const std::string curElemStr = curElem.generic_string();

				::utils::StringPtr elemStr(std::make_unique< ::utils::String>(curElemStr.c_str()));

				if (std::filesystem::is_directory(curElem))
				{
					if (mRecursiveLookup)
						allPaths.push(std::move(elemStr));
				}
				else if (std::filesystem::is_regular_file(curElem))
				{
					// Check if file is of a supported format
					const bool isSupported = std::any_of(std::begin(T::kFiletypes), std::end(T::kFiletypes), [&elemStr](const char* fileType)
					{
						return elemStr->EndsWithI(fileType);
					});

					if (!isSupported)
						continue;

					mCollectedImages.push_back(std::move(elemStr));

					// Stop adding images, we've reach our debug limit
					++iAddedImages;
					if (iAddedImages == T::kMaxImageCount)
					{
						bLimitReached = true;
						break;
					}
				}
			}
		}

		return true;
	}

	template <typename T>
	const ::utils::StringPtrVector& ImageCollectorT<T>::GetCollectedImages() const
	{
		return mCollectedImages;
	}


	// Forced instantiation
	template class ImageCollectorT<>;
}