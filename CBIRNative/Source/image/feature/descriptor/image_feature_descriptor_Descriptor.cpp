#include "image_feature_descriptor_Descriptor.h"
#include "applier/image_feature_descriptor_applier_ColorHistogramApplier.h"
#include "applier/image_feature_descriptor_applier_GLCMApplier.h"
#include "image/locality/image_locality_Global.h"
#include "image/locality/image_locality_CenterAndCorners.h"
#include "image/locality/image_locality_ThreeByThree.h"
#include "image/locality/image_locality_ThreeHorizontal.h"
#include "image/colorspace/image_colorspace_HSV.h"
#include "image/colorspace/image_colorspace_RGB.h"
#include "image/colorspace/image_colorspace_HMMD.h"

namespace image::feature::descriptor
{
	template <class L> void Descriptor::InitApplier(const FeatureDescriptor& descriptOpt)
	{
		// initialize our descriptor applier
		switch (descriptOpt.descriptionMethod)
		{
		case kDescriptionMethod_ColorHistogram:
			{
				InitColorHistogram<L>(descriptOpt.colorHist);
				break;
			}
		case kDescriptionMethod_GLCM:
			{
				mBaseApplier = std::make_unique<applier::GLCMApplier>();
				break;
			}
		default:
			{
				assert(false);
			}
		}
	}

	template <class L> void Descriptor::InitColorHistogram(const ColorHistogramParameters& colorHistOpt)
	{
		switch (colorHistOpt.colorSpace)
		{
			case kHistogramColorSpace_RGB:
			{
				mBaseApplier = std::make_unique<applier::ColorHistogramApplier<L, image::colorspace::RGB>>(colorHistOpt.rgbParams);
				break;
			}
			case kHistogramColorSpace_HSV:
			{
				mBaseApplier = std::make_unique<applier::ColorHistogramApplier<L, image::colorspace::HSV>>(colorHistOpt.hsvParams);
				break;
			}
			case kHistogramColorSpace_HMMD:
			{
				mBaseApplier = std::make_unique<applier::ColorHistogramApplier<L, image::colorspace::HMMD>>(colorHistOpt.hmmdParams);
				break;
			}
			default:
			{
				assert(false);
			}
		}
	}

	Descriptor::Descriptor(const FeatureDescriptor& descriptOpt)
	{
		switch (descriptOpt.localityDescription)
		{
			case kLocalityDescription_Global:
			{
				InitApplier<locality::Global>(descriptOpt);
				break;
			}

			case kLocalityDescription_CenterAndCorners:
			{
				InitApplier<locality::CenterAndCorners>(descriptOpt);
				break;
			}

			case kLocalityDescription_3Horizontal:
			{
				InitApplier<locality::ThreeHorizontal>(descriptOpt);
				break;
			}

			case kLocalityDescription_3x3:
			{
				InitApplier<locality::ThreeByThree>(descriptOpt);
				break;
			}

			default:
			{
				assert(false);
			}
		}
	}

	void Descriptor::Apply(const image::Image& inputImage, image::feature::Feature& outputFeature) const
	{
		mBaseApplier->Apply(inputImage, outputFeature);
	}
}