﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CBIREngine.Source.Model;
using CBIREngine.Source.Command;

namespace CBIREngine.Source.ViewModel
{
	public class MainViewModel : BaseViewModel
	{
		private readonly Model.MainModel mModel = new Model.MainModel();
		private readonly ObservableCollection<BaseDocumentViewModel> mDocuments = new ObservableCollection<BaseDocumentViewModel>();
		private int mSelectedDocumentIdx = 0;

		public MainViewModel()
		{
			mModel.Documents.CollectionChanged += Model_DocumentListChanged;

			mNewImgComparisonCmd = new Command.ImageComparison.NewComparisonCommand(this);
			mCloseDocumentCmd    = new Command.CloseDocumentCommand(this);
		}


		#region Commands
		private Command.ImageComparison.NewComparisonCommand mNewImgComparisonCmd;
		public Command.ImageComparison.NewComparisonCommand NewImageComparisonCommand
		{
			get { return mNewImgComparisonCmd; }
		}

		private Command.CloseDocumentCommand mCloseDocumentCmd;
		public Command.CloseDocumentCommand CloseDocumentCommand
		{
			get { return mCloseDocumentCmd; }
		}
		#endregion

		#region DataBindings
		public Model.MainModel Model
		{
			get { return mModel; }
		}

		public ObservableCollection<BaseDocumentViewModel> Documents
		{
			get { return mDocuments; }
		}
		
		public int SelectedDocumentIdx
		{
			get { return mSelectedDocumentIdx; }
			set
			{
				mSelectedDocumentIdx = value;
				OnPropertyChanged();
			}
		}
		#endregion

		#region PrivateHelpers
		private BaseDocumentViewModel CreateDocumentViewModel(Model.BaseDocumentModel model)
		{
			BaseDocumentViewModel docVM = null;
			if (model is Model.ImageComparison.ImageComparisonModel)
				docVM = new ImageComparison.ImageComparisonViewModel(model as Model.ImageComparison.ImageComparisonModel);

			return docVM;
		}
		#endregion

		#region ModelListener
		private void Model_DocumentListChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			switch (e.Action)
			{
				case NotifyCollectionChangedAction.Add:
					{
						foreach (Model.BaseDocumentModel newEntry in e.NewItems)
						{
							BaseDocumentViewModel vmEntry = CreateDocumentViewModel(newEntry);
							mDocuments.Add(vmEntry);
						}
						break;
					}

				case NotifyCollectionChangedAction.Remove:
					{
						foreach (Model.BaseDocumentModel oldEntry in e.OldItems)
						{
							BaseDocumentViewModel vmEntry = mDocuments.First(x => x.Model == oldEntry);
							mDocuments.Remove(vmEntry);
						}
						break;
					}

				default:
					{
						Debug.Assert(false, String.Format("Unhandled action type: {0}", e.Action.ToString()));
						break;
					}
			}
		}
		#endregion

	}
}
