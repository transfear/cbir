#ifndef CONCURRENT_SPINLOCK_H_
#define CONCURRENT_SPINLOCK_H_

#include "utils/utils_stdlibwrapper.h"
#include "concurrent_ScopedLock.h"

namespace concurrent
{
	class SpinLock
	{
	public:
		SpinLock() { }

		void Acquire()
		{
			while (mLock.test_and_set(std::memory_order_acquire));  // acquire lock and spin
		}

		void Release()
		{
			mLock.clear(std::memory_order_release);	// release lock
		}

	private:
		SpinLock(const SpinLock&) = delete;
		SpinLock& operator=(const SpinLock&) = delete;

		std::atomic_flag mLock = ATOMIC_FLAG_INIT;
	};

	typedef ScopedLock<SpinLock> ScopedSpinLock;
}

#endif // CONCURRENT_SPINLOCK_H_