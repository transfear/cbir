﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram
{
	public abstract class BaseColorHistogramModel : BaseFeatureDescriptorModel
	{
		public override CBIRNative.eDescriptionMethod DescriptionMethod
		{
			get { return CBIRNative.eDescriptionMethod.kDescriptionMethod_ColorHistogram; }
		}

		public abstract CBIRNative.eHistogramColorSpace ColorSpace { get; }

		public static BaseColorHistogramModel CreateFromEnum(CBIRNative.eHistogramColorSpace enumValue)
		{
			switch (enumValue)
			{
				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HSV:
					{
						return new HSVHistogramModel();
					}

				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_RGB:
					{
						return new RGBHistogramModel();
					}

				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HMMD:
					{
						return new HMMDHistogramModel();
					}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported color space: {0}", enumValue.ToString()));
			return null;
		}

		public static BaseColorHistogramModel CreateFromParams(ref CBIRNative.ColorHistogramParameters nativeHisto)
		{
			CBIRNative.eHistogramColorSpace enumValue = nativeHisto.colorSpace;
			switch (enumValue)
			{
				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HSV:
					{
						return new HSVHistogramModel(ref nativeHisto.union.hsvParams);
					}

				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_RGB:
					{
						return new RGBHistogramModel(ref nativeHisto.union.rgbParams);
					}

				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HMMD:
					{
						return new HMMDHistogramModel(ref nativeHisto.union.hmmdParams);
					}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported color space: {0}", enumValue.ToString()));
			return null;
		}
	}
}
