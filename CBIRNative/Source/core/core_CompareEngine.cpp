
#include "core_CompareEngine.h"
#include "core/core_RelayThread.h"
#include "core/job/core_job_GroupLock.h"
#include "container/container_UnmanagedVector.h"
#include "filesys/filesys_Cache.h"
#include "filesys/filesys_ImageCollector.h"
#include "image/job/image_job_Compare.h"
#include "image/feature/descriptor/image_feature_descriptor_Descriptor.h"
#include "utils/iterator/utils_iterator_EmplaceIterator.h"
#include "utils/utils_ScopedAllocFragments.h"
#include "utils/utils_stdlibwrapper.h"
#include "utils/utils_String.h"

namespace core
{
	int CompareEngine::CompareImage(const CompareOptions* opt)
	{
		if (opt == nullptr)
			return -1;

		const u32                         uiNumDescriptParams   = opt->numDescriptors;
		const DescriptorParameters* const pDescriptParamsBegin  = opt->descriptParams;
		const DescriptorParameters* const pDescriptParamsEnd    = pDescriptParamsBegin + uiNumDescriptParams;

		const auto localAllocs = utils::scoped_alloc_fragments
		(
			sizeof(filesys::CacheSettings)                 * uiNumDescriptParams,
			sizeof(image::feature::descriptor::Descriptor) * uiNumDescriptParams,
			sizeof(image::distance::Descriptor)            * uiNumDescriptParams,
			sizeof(image::feature::Feature)                * uiNumDescriptParams,
			sizeof(image::distance::ComparatorParams)      * uiNumDescriptParams
		);

		// Validate our descriptor weights are valid
		const bool bDescriptSumWeightValid = std::accumulate(pDescriptParamsBegin, pDescriptParamsEnd, 0.0f, [](float fAccum, const auto& descript) { return fAccum + descript.weight; }) > 0.0f;
		const bool bDescriptWeightValid    = std::all_of(pDescriptParamsBegin, pDescriptParamsEnd, [](const auto& descript) { return descript.weight >= 0.0f; });
		if (!bDescriptSumWeightValid && !bDescriptWeightValid)
			return -2;

		// Create our cache handles
		container::UnmanagedVector<filesys::CacheSettings> cacheBasePaths(localAllocs.get<0>());
		const bool        bCompressData  = opt->compressCache;
		const char* const rootSearchPath = opt->rootImageFolder;
		
		std::transform(pDescriptParamsBegin, pDescriptParamsEnd, utils::iterator::EmplaceIterator(cacheBasePaths),
			[=](const DescriptorParameters& param)
			{
				::utils::StringPtr cacheBasePath = filesys::Cache::GetCachePath(opt->cacheFolder, param.feature, bCompressData);
				return std::make_tuple(std::move(cacheBasePath), rootSearchPath, bCompressData);
			}
		);
		

		// Instantiate wanted descriptors from ParseOptions
		container::UnmanagedVector<image::feature::descriptor::Descriptor> featDescriptVec(localAllocs.get<1>());
		container::UnmanagedVector<image::distance::Descriptor>            distDescriptVec(localAllocs.get<2>());
		for (u32 uiCurDescriptParam = 0; uiCurDescriptParam < uiNumDescriptParams; ++uiCurDescriptParam)
		{
			const DescriptorParameters& curDesc  = pDescriptParamsBegin[uiCurDescriptParam];
			const FeatureDescriptor&    featDesc = curDesc.feature;
			const DistanceDescriptor&   distDesc = curDesc.distance;
			featDescriptVec.emplace_back(featDesc);
			
			const image::feature::descriptor::Descriptor& featDescriptor = featDescriptVec.back();

			// Validate region weights
			const u32        uiNumRegions        = featDescriptor.GetRegionCount();
			const f32* const pRegionWeightsBegin = featDesc.pRegionWeights;
			const f32* const pRegionWeightsEnd   = pRegionWeightsBegin + uiNumRegions;

			const bool bSumValid        = std::accumulate(pRegionWeightsBegin, pRegionWeightsEnd, 0.0f) > 0.0f;
			const bool bEachWeightValid = std::all_of(pRegionWeightsBegin, pRegionWeightsEnd, [](f32 i) { return i >= 0.0f; });
			if (!bSumValid && !bEachWeightValid)
				return -3;

			distDescriptVec.emplace_back(distDesc, pRegionWeightsBegin, uiNumRegions);
		}
		
		// TODO: move to a thread (job must not be deleted when popped)
		// Generate features for our reference image
		container::UnmanagedVector<image::feature::Feature> refFeatures(localAllocs.get<3>());
		for (u32 uiCurDescriptParam = 0; uiCurDescriptParam < uiNumDescriptParams; ++uiCurDescriptParam)
		{
			const filesys::CacheSettings&                 cacheSettings  = cacheBasePaths[uiCurDescriptParam];
			const image::feature::descriptor::Descriptor& featDescriptor = featDescriptVec[uiCurDescriptParam];

			const image::feature::GeneratorParams refParams(cacheSettings, opt->srcImagePath, featDescriptor);

			image::feature::Generator generator(refParams);
			image::feature::GeneratorResult srcResult = std::move(generator.Generate());
			image::feature::Feature& srcFeature = srcResult.first;

			refFeatures.push_back(std::move(srcFeature));
		}
		
		// Find all images in root folders
		filesys::ImageCollector collector(rootSearchPath, opt->checkSubFolders);
		if (!collector.CollectImages())
			return -4;

		core::job::JobSysCancelToken cancelToken(mJobSys);
		image::job::Compare::Relay relayThread(opt->cb, opt->userData, cancelToken);

		// Push a compare job for each image
		core::job::GroupLock compareJobsLock;
		const utils::StringPtrVector& images = collector.GetCollectedImages();
		for (const utils::StringPtr& curImagePath : images)
		{
			const char* const rawImagePath = curImagePath->GetRaw();
			container::UnmanagedVector<image::distance::ComparatorParams> curImageCmpParams(localAllocs.get<4>());

			for (u32 uiCurDescriptParam = 0; uiCurDescriptParam < uiNumDescriptParams; ++uiCurDescriptParam)
			{
				const filesys::CacheSettings&                 cacheSettings  = cacheBasePaths[uiCurDescriptParam];
				const image::feature::descriptor::Descriptor& featDescriptor = featDescriptVec[uiCurDescriptParam];
				const image::distance::Descriptor&            distDescriptor = distDescriptVec[uiCurDescriptParam];
				const image::feature::Feature&                refFeature     = refFeatures[uiCurDescriptParam];

				const image::feature::GeneratorParams genParams(cacheSettings, rawImagePath, featDescriptor);
				curImageCmpParams.emplace_back(refFeature, genParams, distDescriptor);
			}

			image::job::Compare* compareJob = new image::job::Compare(*opt, rawImagePath, curImageCmpParams, relayThread, compareJobsLock);
			mJobSys.PushJob(compareJob);
		}

		// Wait until all parse jobs are completed
		compareJobsLock.WaitForAllJobs();

		return 0;
	}
}

