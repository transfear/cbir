#ifndef CORE_JOB_DUMMYJOB_H_
#define CORE_JOB_DUMMYJOB_H_

#include "core_job_Job.h"

namespace core::job
{
	class DummyJob : public Job
	{
	private:
		virtual void ExecuteInternal() override { }
	};
}

#endif	// CORE_JOB_DUMMYJOB_H_