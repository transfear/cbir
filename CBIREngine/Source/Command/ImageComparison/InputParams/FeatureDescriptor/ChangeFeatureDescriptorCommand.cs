﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using API;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams;
using VM  = CBIREngine.Source.ViewModel.ImageComparison.InputParams;

namespace CBIREngine.Source.Command.ImageComparison.InputParams.FeatureDescriptor
{
	public class ChangeFeatureDescriptorCommand : ICommand
	{
		private readonly VM.DescriptorParamsViewModel mVM;
		private readonly MDL.DescriptorParamsModel    mModel;
		public event EventHandler CanExecuteChanged;

		public ChangeFeatureDescriptorCommand(VM.DescriptorParamsViewModel vm)
		{
			mVM    = vm;
			mModel = mVM.Model;
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			List<object> paramList = parameter as List<object>;

			MDL.FeatureDescriptor.BaseFeatureDescriptorModel newModel = MDL.FeatureDescriptor.BaseFeatureDescriptorModel.CreateFromParams(paramList);
			mModel.FeatureDescriptor = newModel;
		}

		public void RaiseCanExecuteChanged()
		{
			if (CanExecuteChanged != null)
			{
				CanExecuteChanged(this, EventArgs.Empty);
			}
		}
	}
}
