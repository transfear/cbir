﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using CBIREngine.Source.Utils;

namespace CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Locality
{
	public class CenterAndCornersLocalityModel : BaseResultDescriptorModel
	{
		private readonly Region.BaseRegionModel mTopLeft;
		private readonly Region.BaseRegionModel mTopRight;
		private readonly Region.BaseRegionModel mBotLeft;
		private readonly Region.BaseRegionModel mBotRight;
		private readonly Region.BaseRegionModel mCenter;
		
		public CenterAndCornersLocalityModel(ref CBIRNative.DescriptorResult nativeDescript) : base(ref nativeDescript)
		{
			System.Diagnostics.Debug.Assert(nativeDescript.inParams.feature.localityDescription == CBIRNative.eLocalityDescription.kLocalityDescription_CenterAndCorners);
			System.Diagnostics.Debug.Assert(nativeDescript.numRegions == 5);

			CBIRNative.RegionResult[] regionArray = MarshalUtils.GetArray<CBIRNative.RegionResult>(nativeDescript.pRegionResults, 5);
			unsafe
			{ 
				mTopLeft  = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[0], nativeDescript.inParams.feature.pRegionWeights[0]);
				mTopRight = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[1], nativeDescript.inParams.feature.pRegionWeights[1]);
				mBotLeft  = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[2], nativeDescript.inParams.feature.pRegionWeights[2]);
				mBotRight = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[3], nativeDescript.inParams.feature.pRegionWeights[3]);
				mCenter   = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[4], nativeDescript.inParams.feature.pRegionWeights[4]);
			}
		}

		public override int NumRegions
		{
			get { return 5; }
		}

		public override CBIRNative.eLocalityDescription LocalityType
		{
			get { return CBIRNative.eLocalityDescription.kLocalityDescription_CenterAndCorners; }
		}

		public Region.BaseRegionModel TopLeft
		{
			get { return mTopLeft; }
		}

		public Region.BaseRegionModel TopRight
		{
			get { return mTopRight; }
		}

		public Region.BaseRegionModel BotLeft
		{
			get { return mBotLeft; }
		}

		public Region.BaseRegionModel BotRight
		{
			get { return mBotRight; }
		}

		public Region.BaseRegionModel Center
		{
			get { return mCenter; }
		}

		public override Region.BaseRegionModel GetRegionAt(int idx)
		{
			switch (idx)
			{
				case 0:
					return TopLeft;
				case 1:
					return TopRight;
				case 2:
					return BotLeft;
				case 3:
					return BotRight;
				case 4:
					return Center;
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Invalid weight index: {0}", idx));
			return null;
		}
	}
}
