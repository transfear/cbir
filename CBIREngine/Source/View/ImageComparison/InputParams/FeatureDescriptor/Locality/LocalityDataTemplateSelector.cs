﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

using API;
using VM = CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor.Locality;

namespace CBIREngine.Source.View.ImageComparison.InputParams.FeatureDescriptor.Locality
{
	class LocalityDataTemplateSelector : DataTemplateSelector
	{
		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			FrameworkElement fe = container as FrameworkElement;
			VM.BaseLocalityViewModel vm = item as VM.BaseLocalityViewModel;

			if (vm != null)
			{
				switch (vm.LocalityType)
				{
					case CBIRNative.eLocalityDescription.kLocalityDescription_Global:
						{
							return fe.FindResource("GlobalDataTemplate") as DataTemplate;
						}

					case CBIRNative.eLocalityDescription.kLocalityDescription_CenterAndCorners:
						{
							return fe.FindResource("CenterAndCornersDataTemplate") as DataTemplate;
						}

					case CBIRNative.eLocalityDescription.kLocalityDescription_3Horizontal:
						{
							return fe.FindResource("ThreeHorizontalDataTemplate") as DataTemplate;
						}

					case CBIRNative.eLocalityDescription.kLocalityDescription_3x3:
						{
							return fe.FindResource("ThreeByThreeDataTemplate") as DataTemplate;
						}
				}
			}

			return base.SelectTemplate(item, container);
		}
	}
}
