﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using MDL = CBIREngine.Source.Model.ImageComparison.InputParams;
using VM  = CBIREngine.Source.ViewModel.ImageComparison.InputParams;

namespace CBIREngine.Source.Command.ImageComparison.InputParams
{
    public class NewDescriptorParamsCommand : ICommand
    {
		private readonly VM.InputParamsViewModel  mVM;
		private readonly MDL.InputParamsModel     mModel;
		public event EventHandler CanExecuteChanged;

		public NewDescriptorParamsCommand(VM.InputParamsViewModel vm)
		{
			mVM    = vm;
			mModel = mVM.Model;
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			// Add a new default descriptor
			mModel.Descriptors.Add(new MDL.DescriptorParamsModel());
		}

		public void RaiseCanExecuteChanged()
		{
			if (CanExecuteChanged != null)
			{
				CanExecuteChanged(this, EventArgs.Empty);
			}
		}
	}
}
