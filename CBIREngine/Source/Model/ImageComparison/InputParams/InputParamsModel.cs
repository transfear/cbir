﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CBIREngine.Source.Utils;

namespace CBIREngine.Source.Model.ImageComparison.InputParams
{
    public class InputParamsModel : BaseModel
	{
		private String mSourceImage = System.IO.Path.GetFullPath("..\\..\\Images\\Flickr\\5894765861_4703f0b2f6_z.jpg");	// String.Empty
		private String mImageLib    = System.IO.Path.GetFullPath("..\\..\\Images"); //String.Empty;
		private String mCacheFolder = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "cache");
		private int    mNumThreads  = Environment.ProcessorCount;
		private bool   mbCheckSubfolders   = true;
		private bool   mbCompressCache     = true;

		private readonly ObservableRangeCollection<DescriptorParamsModel> mDescriptors = new ObservableRangeCollection<DescriptorParamsModel>();

		public InputParamsModel()
		{
			// Start with a default descriptor
			mDescriptors.Add(new DescriptorParamsModel());
		}
		
		public ObservableRangeCollection<DescriptorParamsModel> Descriptors
		{
			get { return mDescriptors; }
		}

		public String SourceImage
		{
			get { return mSourceImage; }
			set
			{
				mSourceImage = value;
				OnPropertyChanged();
			}
		}
		
		public String ImageLib
		{
			get { return mImageLib; }
			set
			{
				mImageLib = value;
				OnPropertyChanged();
			}
		}

		public String CacheFolder
		{
			get { return mCacheFolder; }
			set
			{
				mCacheFolder = value;
				OnPropertyChanged();
			}
		}

		public int NumThreads
		{
			get { return mNumThreads; }
			set
			{
				mNumThreads = value;
				OnPropertyChanged();
			}
		}

		public bool CheckSubfolders
		{
			get { return mbCheckSubfolders; }
			set
			{
				mbCheckSubfolders = value;
				OnPropertyChanged();
			}
		}

		public bool CompressCache
		{
			get { return mbCompressCache; }
			set
			{
				mbCompressCache = value;
				OnPropertyChanged();
			}
		}
	}
}
