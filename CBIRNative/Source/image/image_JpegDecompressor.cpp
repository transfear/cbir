#include "image_JpegDecompressor.h"
#include "utils/utils_externalwrapper.h"

namespace image
{

	void JpegDecompressor::Decompress(FILE* fp)
	{
		assert(fp != nullptr);

		jpeg_decompress_struct cinfo;
		jpeg_error_mgr jerr;

		cinfo.err = jpeg_std_error(&jerr);  

		jpeg_create_decompress(&cinfo);
		jpeg_stdio_src(&cinfo, fp);

		jpeg_read_header(&cinfo, TRUE);
		jpeg_start_decompress(&cinfo);

		// Assumes RGB
		assert(cinfo.output_components == 3);
		assert(cinfo.out_color_space == JCS_RGB);
		
		const u32 rowStride = cinfo.output_width * cinfo.output_components;

		u8* pRawData = new u8[rowStride * cinfo.output_height];
		mImage.Reset(pRawData, cinfo.output_width, cinfo.output_height, static_cast<u8>(cinfo.output_components));

		u8* rowptr[1];
		while (cinfo.output_scanline < cinfo.output_height)
		{
			rowptr[0] = pRawData + rowStride * cinfo.output_scanline;
			jpeg_read_scanlines(&cinfo, rowptr, 1);
		}

		jpeg_finish_decompress(&cinfo);
		jpeg_destroy_decompress(&cinfo);
	}


	const Image& JpegDecompressor::GetDecompressedImage() const
	{
		return mImage;
	}
}