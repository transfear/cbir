﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using InputParams = CBIREngine.Source.ViewModel.ImageComparison.InputParams;
using MDL         = CBIREngine.Source.Model.ImageComparison.Result.Descriptor;

namespace CBIREngine.Source.ViewModel.ImageComparison.Result.Descriptor
{
	public abstract class BaseResultDescriptorViewModel : BaseViewModel
	{
		private readonly MDL.BaseResultDescriptorModel mModel;
		
		private readonly InputParams.DescriptorParamsViewModel mInputParams;
		private readonly ResultViewModel mParentResult;

		public abstract Region.BaseRegionViewModel GetRegionAt(int idx);


		public BaseResultDescriptorViewModel(MDL.BaseResultDescriptorModel model, ResultViewModel parent)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;

			mParentResult = parent;

			mInputParams = new InputParams.DescriptorParamsViewModel(mModel.InputParams, null);
		}

		public ResultViewModel ParentResult
		{
			get { return mParentResult; }
		}

		public MDL.BaseResultDescriptorModel Model
		{
			get { return mModel; }
		}
		public int NumRegions
		{
			get { return mModel.NumRegions; }
		}

		public double DistToRef
		{
			get { return mModel.DistToRef; }
		}

		public CBIRNative.eLocalityDescription LocalityType
		{
			get { return mModel.LocalityType; }
		}

		public double WeightedDistToRef
		{
			get { return mModel.DistToRef * mInputParams.Weight; }
		}

		public InputParams.DescriptorParamsViewModel Descriptor
		{
			get { return mInputParams; }
		}

		#region HelperFunctions
		public static BaseResultDescriptorViewModel CreateFromModel(MDL.BaseResultDescriptorModel model, ResultViewModel parent)
		{
			CBIRNative.eLocalityDescription enumValue = model.InputParams.FeatureDescriptor.Locality.LocalityDescription;
			switch (enumValue)
			{
				case CBIRNative.eLocalityDescription.kLocalityDescription_Global:
				{
						MDL.Locality.GlobalLocalityModel derivedModel = model as MDL.Locality.GlobalLocalityModel;
						return new Locality.GlobalLocalityViewModel(derivedModel, parent);
				}
				case CBIRNative.eLocalityDescription.kLocalityDescription_CenterAndCorners:
				{
						MDL.Locality.CenterAndCornersLocalityModel derivedModel = model as MDL.Locality.CenterAndCornersLocalityModel;
						return new Locality.CenterAndCornersLocalityViewModel(derivedModel, parent);
				}
				case CBIRNative.eLocalityDescription.kLocalityDescription_3x3:
				{
					MDL.Locality.ThreeByThreeLocalityModel derivedModel = model as MDL.Locality.ThreeByThreeLocalityModel;
					return new Locality.ThreeByThreeLocalityViewModel(derivedModel, parent);
				}
				case CBIRNative.eLocalityDescription.kLocalityDescription_3Horizontal:
				{
					MDL.Locality.ThreeHorizontalLocalityModel derivedModel = model as MDL.Locality.ThreeHorizontalLocalityModel;
					return new Locality.ThreeHorizontalLocalityViewModel(derivedModel, parent);
				}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported locality descriptor: {0}", enumValue.ToString()));
			return null;
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "NumRegions":
					{
						OnPropertyChanged(e.PropertyName);
						break;
					}
				case "DistToRef":
					{
						OnPropertyChanged(e.PropertyName);
						break;
					}
			}
		}
		#endregion
	}
}
