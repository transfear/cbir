﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace CBIREngine.Source.Command
{
	public class BrowseFolderCommand : ICommand
	{
		public event EventHandler CanExecuteChanged;

		private readonly Action<string> mSetFilePathAction;
		private readonly Func<string>   mGetInitialDirectoryFn;
		private readonly string         mDialogDescription;

		public BrowseFolderCommand(Action<string> setFolderPathAction, Func<string> getInitialDirectoryFn, string dialogDescription)
		{
			mSetFilePathAction     = setFolderPathAction;
			mGetInitialDirectoryFn = getInitialDirectoryFn;
			mDialogDescription     = dialogDescription;
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			FolderBrowserDialog dialog = new FolderBrowserDialog();
			dialog.Description  = mDialogDescription;
			dialog.SelectedPath = mGetInitialDirectoryFn();
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				string folderPath = dialog.SelectedPath;
				mSetFilePathAction(folderPath);
			}
		}

		public void RaiseCanExecuteChanged()
		{
			if (CanExecuteChanged != null)
			{
				CanExecuteChanged(this, EventArgs.Empty);
			}
		}
	}
}
