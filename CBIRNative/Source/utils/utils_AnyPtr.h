#ifndef UTILS_ANYPTR_H_
#define UTILS_ANYPTR_H_

class AnyPtr
{
public:
	
	template <typename T> AnyPtr(T data) : mData(reinterpret_cast<void*>(data)) { }
	
	template <typename T> T& get()
	{
		return reinterpret_cast<T&>(mData);
	}
	
private:
	void* mData;
};


#endif // UTILS_ANYPTR_H_