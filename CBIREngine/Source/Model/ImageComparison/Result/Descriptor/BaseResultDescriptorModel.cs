﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.Result.Descriptor
{
	public abstract class BaseResultDescriptorModel : BaseModel
	{
		private readonly double mDistToRef;
		private readonly InputParams.DescriptorParamsModel mInputParams;

		public abstract int NumRegions { get; }
		public abstract Region.BaseRegionModel GetRegionAt(int idx);
		public abstract CBIRNative.eLocalityDescription LocalityType { get; }

		public BaseResultDescriptorModel(ref CBIRNative.DescriptorResult nativeDescript) 
		{
			mDistToRef   = nativeDescript.distance;
			mInputParams = new InputParams.DescriptorParamsModel(ref nativeDescript.inParams);
		}

		public double DistToRef
		{
			get { return mDistToRef; }
		}

		public InputParams.DescriptorParamsModel InputParams
		{
			get { return mInputParams; }
		}

		#region HelperFunctions
		public static BaseResultDescriptorModel CreateFromParams(ref CBIRNative.DescriptorResult nativeDescript)
		{
			BaseResultDescriptorModel toReturn = null;
			switch (nativeDescript.inParams.feature.localityDescription)
			{
				case CBIRNative.eLocalityDescription.kLocalityDescription_Global:
				{
					toReturn = new Locality.GlobalLocalityModel(ref nativeDescript);
					break;
				}

				case CBIRNative.eLocalityDescription.kLocalityDescription_CenterAndCorners:
				{
					toReturn = new Locality.CenterAndCornersLocalityModel(ref nativeDescript);
					break;
				}

				case CBIRNative.eLocalityDescription.kLocalityDescription_3Horizontal:
				{
					toReturn = new Locality.ThreeHorizontalLocalityModel(ref nativeDescript);
					break;
				}

				case CBIRNative.eLocalityDescription.kLocalityDescription_3x3:
				{
					toReturn = new Locality.ThreeByThreeLocalityModel(ref nativeDescript);
					break;
				}

				default:
				{
					System.Diagnostics.Debug.Assert(false, String.Format("Unexpected locality value: {0}", nativeDescript.inParams.feature.localityDescription.ToString()));
					break;
				}
			}

			return toReturn;
		}
		#endregion
	}
}
