#ifndef UTILS_STDLIBWRAPPER_H_
#define UTILS_STDLIBWRAPPER_H_

#pragma warning(push, 3)

#pragma warning(disable: 4265)
#pragma warning(disable: 4365)
#pragma warning(disable: 4530)
#pragma warning(disable: 4548)
#pragma warning(disable: 4577)
#pragma warning(disable: 4623)
#pragma warning(disable: 4625)
#pragma warning(disable: 4626)
#pragma warning(disable: 4774)
#pragma warning(disable: 4820)
#pragma warning(disable: 5026)
#pragma warning(disable: 5027)

#define _CRT_SECURE_NO_WARNINGS 1    // to prevent warning C4996 on fopen

#include <algorithm>
#include <any>
#include <array>
#include <atomic>
#include <cassert>
#include <condition_variable>
#include <cstdio>
#include <cstring>
#include <execution>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <mutex>
#include <numeric>
#include <optional>
#include <set>
#include <stack>
#include <thread>
#include <type_traits>
#include <tuple>
#include <utility>
#include <variant>
#include <vector>

#pragma warning(pop)


#include "utils/utils_basictypes.h"
typedef std::unique_ptr<u8> BytePtr;
typedef std::vector<u8>     ByteVector;

#endif // UTILS_STDLIBWRAPPER_H_