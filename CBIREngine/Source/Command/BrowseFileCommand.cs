﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using Microsoft.Win32;

namespace CBIREngine.Source.Command
{
	public class BrowseFileCommand : ICommand
	{
		public event EventHandler CanExecuteChanged;

		private readonly Action<string> mSetFilePathAction;
		private readonly Func<string>   mGetInitialDirectoryFn;
		private readonly string         mDialogName;
		private readonly string         mFileFilter;

		public BrowseFileCommand(Action<string> setFilePathAction, Func<string> getInitialDirectoryFn, string dialogName, ICollection<string> allowedFiles)
		{
			mSetFilePathAction     = setFilePathAction;
			mGetInitialDirectoryFn = getInitialDirectoryFn;
			mDialogName            = dialogName;
			if (allowedFiles == null)
			{
				mFileFilter = "All files (*.*)|*.*";
			}
			else
			{
				string fileFilter = String.Empty;
				foreach (string s in allowedFiles)
					fileFilter += String.Format("*.{0};", s);
				fileFilter = fileFilter.Remove(fileFilter.Length - 1);

				mFileFilter = String.Format("Allowed files ({0})|{0}", fileFilter);
			}
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			OpenFileDialog openFileDialog   = new OpenFileDialog();
			openFileDialog.Filter           = mFileFilter;
			openFileDialog.InitialDirectory = mGetInitialDirectoryFn();
			openFileDialog.Title            = mDialogName;

			if (openFileDialog.ShowDialog() == true)
			{
				string fileName = openFileDialog.FileName;
				mSetFilePathAction(fileName);
			}
		}

		public void RaiseCanExecuteChanged()
		{
			if (CanExecuteChanged != null)
			{
				CanExecuteChanged(this, EventArgs.Empty);
			}
		}
	}
}
