﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using CMD = CBIREngine.Source.Command.ImageComparison.InputParams.FeatureDescriptor;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor
{
	public abstract class BaseFeatureDescriptorViewModel : BaseViewModel
	{
		private readonly MDL.BaseFeatureDescriptorModel     mModel;
		private readonly CMD.ChangeFeatureDescriptorCommand mChangeDescriptCommand;    // Owned by parent DescriptorParamsViewModel
		private readonly CMD.Locality.ChangeLocalityCommand mChangeLocalityCommand;

		private Locality.BaseLocalityViewModel mLocalityVM;

		public BaseFeatureDescriptorViewModel(MDL.BaseFeatureDescriptorModel model, CMD.ChangeFeatureDescriptorCommand changeDescriptCmd)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;

			mChangeDescriptCommand = changeDescriptCmd;
			mChangeLocalityCommand = new CMD.Locality.ChangeLocalityCommand(this);

			RecreateLocalityVM();
		}

		#region DataBindings
		public MDL.BaseFeatureDescriptorModel Model
		{
			get { return mModel; }
		}

		public CBIRNative.eDescriptionMethod FeatureType
		{
			get { return mModel.DescriptionMethod; }
			set
			{
				if (value != mModel.DescriptionMethod)
					mChangeDescriptCommand.Execute(new List<object>() { value });
			}
		}

		public Locality.BaseLocalityViewModel LocalityVM
		{
			get { return mLocalityVM; }
			set
			{
				mLocalityVM = value;
				OnPropertyChanged();
			}
		}
		#endregion

		#region HelperFunctions
		public static BaseFeatureDescriptorViewModel CreateFromModel(MDL.BaseFeatureDescriptorModel model, CMD.ChangeFeatureDescriptorCommand changeDescriptCommand)
		{
			switch (model.DescriptionMethod)
			{
				case CBIRNative.eDescriptionMethod.kDescriptionMethod_ColorHistogram:
					{
						MDL.ColorHistogram.BaseColorHistogramModel histogramModel = model as MDL.ColorHistogram.BaseColorHistogramModel;
						return ColorHistogram.BaseColorHistogramViewModel.CreateFromModel(histogramModel, changeDescriptCommand);
					}
				case CBIRNative.eDescriptionMethod.kDescriptionMethod_GLCM:
					{
						return new GLCM.GLCMDescriptorViewModel(model as MDL.GLCM.GLCMDescriptorModel, changeDescriptCommand);
					}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported feature descriptor: {0}", model.DescriptionMethod.ToString()));
			return null;
		}

		private void RecreateLocalityVM()
		{
			LocalityVM = Locality.BaseLocalityViewModel.CreateFromModel(mModel.Locality, mChangeLocalityCommand);
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Locality":
					{
						RecreateLocalityVM();
						break;
					}
			}
		}
		#endregion
	}
}
