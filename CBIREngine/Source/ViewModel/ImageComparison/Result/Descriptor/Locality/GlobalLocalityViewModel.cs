﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MDL      = CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Locality;
using RegionVM = CBIREngine.Source.ViewModel.ImageComparison.Result.Descriptor.Region;

namespace CBIREngine.Source.ViewModel.ImageComparison.Result.Descriptor.Locality
{
	public class GlobalLocalityViewModel : BaseResultDescriptorViewModel
	{
		private readonly MDL.GlobalLocalityModel mModel;
		private readonly RegionVM.BaseRegionViewModel mGlobalRegionVM;

		public GlobalLocalityViewModel(MDL.GlobalLocalityModel model, ResultViewModel parent) : base(model, parent)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;

			mGlobalRegionVM = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.GlobalRegion, parent);
		}

		public override RegionVM.BaseRegionViewModel GetRegionAt(int idx)
		{
			switch (idx)
			{
				case 0:
					return mGlobalRegionVM;
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Invalid region index: {0}", idx));
			return null;
		}

		public RegionVM.BaseRegionViewModel GlobalRegion
		{
			get { return mGlobalRegionVM; }
		}

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
		}
		#endregion
	}
}
