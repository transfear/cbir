#ifndef FILESYS_IMAGECOLLECTOR_H_
#define FILESYS_IMAGECOLLECTOR_H_

#include "utils/utils_String.h"

namespace filesys
{
	class ImageCollectorDefaultConfig
	{
		public:
			static constexpr u32 kMaxImageCount = std::numeric_limits<u32>::max();
			static constexpr const char* const kFiletypes[] = { ".jpg", ".jpeg" };
	};

	template <typename T = ImageCollectorDefaultConfig> class ImageCollectorT
	{
		public:
			ImageCollectorT(const char* path, bool recursive = true);

			bool CollectImages();

			const ::utils::StringPtrVector& GetCollectedImages() const;

		private:

			::utils::StringPtrVector mCollectedImages;

			const char* mRootPath;
			bool        mRecursiveLookup;


			char padding[7];
	};

	typedef ImageCollectorT<> ImageCollector;
}

#endif // FILESYS_IMAGECOLLECTOR_H_