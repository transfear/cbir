﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.Locality
{
	public class CenterAndCornersLocalityModel : BaseLocalityModel
	{
		private float mfTopLeft  = 0.2f;
		private float mfTopRight = 0.2f;
		private float mfBotLeft  = 0.2f;
		private float mfBotRight = 0.2f;
		private float mfCenter   = 0.2f;

		public CenterAndCornersLocalityModel() { }
		public CenterAndCornersLocalityModel(ref CBIRNative.FeatureDescriptor featureDescriptor)
		{
			unsafe
			{
				mfTopLeft  = featureDescriptor.pRegionWeights[0];
				mfTopRight = featureDescriptor.pRegionWeights[1];
				mfBotLeft  = featureDescriptor.pRegionWeights[2];
				mfBotRight = featureDescriptor.pRegionWeights[3];
				mfCenter   = featureDescriptor.pRegionWeights[4];
			}
		}

		public float TopLeft
		{
			get { return mfTopLeft; }
			set
			{
				mfTopLeft = value;
				OnPropertyChanged();
			}
		}

		public float TopRight
		{
			get { return mfTopRight; }
			set
			{
				mfTopRight = value;
				OnPropertyChanged();
			}
		}

		public float BotLeft
		{
			get { return mfBotLeft;}
			set
			{
				mfBotLeft = value;
				OnPropertyChanged();
			}
		}

		public float BotRight
		{
			get { return mfBotRight; }
			set
			{
				mfBotRight = value;
				OnPropertyChanged();
			}
		}

		public float Center
		{
			get { return mfCenter; }
			set
			{
				mfCenter = value;
				OnPropertyChanged();
			}
		}

		public override CBIRNative.eLocalityDescription LocalityDescription
		{
			get { return CBIRNative.eLocalityDescription.kLocalityDescription_CenterAndCorners; }
		}

		public override int NumWeights
		{
			get { return 5; }
		}

		public override float GetWeightAt(int idx)
		{
			switch (idx)
			{
				case 0:
					return TopLeft;
				case 1:
					return TopRight;
				case 2:
					return BotLeft;
				case 3:
					return BotRight;
				case 4:
					return Center;
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Invalid weight index: {0}", idx));
			return -1.0f;
		}
	}
}
