﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using Local = CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.Locality;

namespace CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor
{
	public abstract class BaseFeatureDescriptorModel : BaseModel
	{
		public abstract CBIRNative.eDescriptionMethod DescriptionMethod { get; }

		private Local.BaseLocalityModel mLocality = new Local.GlobalLocalityModel();

		public BaseFeatureDescriptorModel() { }

		public Local.BaseLocalityModel Locality
		{
			get { return mLocality; }
			set
			{
				mLocality = value;
				OnPropertyChanged();
			}
		}

		#region HelperFunctions
		public static BaseFeatureDescriptorModel CreateFromParams(List<object> paramList)
		{
			CBIRNative.eDescriptionMethod descriptMethod = (CBIRNative.eDescriptionMethod)paramList.ElementAt(0);
			switch (descriptMethod)
			{
				case CBIRNative.eDescriptionMethod.kDescriptionMethod_ColorHistogram:
					{
						CBIRNative.eHistogramColorSpace colorSpace = (paramList.Count > 1) ? (CBIRNative.eHistogramColorSpace)paramList.ElementAt(1) : CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HSV;
						return ColorHistogram.BaseColorHistogramModel.CreateFromEnum(colorSpace);
					}
				case CBIRNative.eDescriptionMethod.kDescriptionMethod_GLCM:
					{
						return new GLCM.GLCMDescriptorModel();
					}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported feature descriptor: {0}", descriptMethod.ToString()));
			return null;
		}

		public static BaseFeatureDescriptorModel CreateFromParams(ref CBIRNative.FeatureDescriptor nativeDescript)
		{
			BaseFeatureDescriptorModel toReturn = null;

			CBIRNative.eDescriptionMethod descriptMethod = nativeDescript.descriptionMethod;
			switch (descriptMethod)
			{
				case CBIRNative.eDescriptionMethod.kDescriptionMethod_ColorHistogram:
				{
					toReturn = ColorHistogram.BaseColorHistogramModel.CreateFromParams(ref nativeDescript.descriptor.colorHist);
					break;
				}
				case CBIRNative.eDescriptionMethod.kDescriptionMethod_GLCM:
				{
					System.Diagnostics.Debug.Assert(false, "todo");
					toReturn = new GLCM.GLCMDescriptorModel();
					break;
				}
				default:
				{
					System.Diagnostics.Debug.Assert(false, String.Format("Unsupported feature descriptor: {0}", descriptMethod.ToString()));
					break;
				}
			}

			toReturn.Locality = Local.BaseLocalityModel.CreateFromParams(ref nativeDescript);

			return toReturn;
		}
		#endregion

	}
}
