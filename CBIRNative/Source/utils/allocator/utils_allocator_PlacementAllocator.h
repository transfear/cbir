#ifndef UTILS_ALLOCATOR_PLACEMENTALLOCATOR
#define UTILS_ALLOCATOR_PLACEMENTALLOCATOR

#include "utils/utils_stdlibwrapper.h"

namespace utils::allocator
{
#if defined(_ITERATOR_DEBUG_LEVEL) && _ITERATOR_DEBUG_LEVEL > 0
	static_assert(false);	// On MSVC, PlacementAllocator can only be used without debug iterators
#endif

	template <typename T>
	class PlacementAllocator : public std::allocator<T>
	{
	public:
		 PlacementAllocator(void* pMem) : std::allocator<T>(), mpMem(pMem) {  }
		 PlacementAllocator(const PlacementAllocator& _other) : std::allocator<T>(_other), mpMem(_other.mpMem) {  }
		~PlacementAllocator() { }

		typedef size_t size_type;
		typedef T* pointer;
		typedef const T* const_pointer;

		template<typename U>
		struct rebind
		{
			typedef PlacementAllocator<U> other;
		};

		pointer allocate(size_type num, const void* hint [[maybe_unused]] = 0)
		{
			char* p = new (mpMem) char[num * sizeof(T)];
			return (T*)p;
		}

		void deallocate(pointer, size_type ) { }
		
		void* GetMem() const { return mpMem;  }

	private:
		void* mpMem;
	};

	template <typename T>
	inline PlacementAllocator<T> Make_PlacementAllocator(void* pMem)
	{
		return PlacementAllocator<T>(pMem);
	}

	template <typename T>
	using PlacementVector = std::vector<T, PlacementAllocator<T>>;
}

#endif // UTILS_ALLOCATOR_PLACEMENTALLOCATOR