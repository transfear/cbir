#ifndef IMAGE_DISTANCE_APPLIER_EUCLIDIAN_H_
#define IMAGE_DISTANCE_APPLIER_EUCLIDIAN_H_

#include "image_distance_applier_BaseApplier.h"

namespace image::distance::applier
{
	class Euclidean : public BaseApplier
	{
	public:
		Euclidean() { }

		virtual dist_elem Apply(u32 uiRegionIdx, const image::feature::Feature& first, const image::feature::Feature& second) const override;

	};
}

#endif // IMAGE_DISTANCE_APPLIER_EUCLIDIAN_H_