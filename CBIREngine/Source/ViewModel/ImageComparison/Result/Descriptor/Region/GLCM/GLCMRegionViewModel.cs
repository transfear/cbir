﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MDL = CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Region.GLCM;

namespace CBIREngine.Source.ViewModel.ImageComparison.Result.Descriptor.Region.GLCM
{
	public class GLCMRegionViewModel : BaseRegionViewModel
	{
		private readonly MDL.GLCMRegionModel mModel;

		public GLCMRegionViewModel(MDL.GLCMRegionModel model, ResultViewModel parentResult) : base(model, parentResult)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
		}

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				default:
				{
					//TODO
					break;
				}
			}
		}
		#endregion
	}
}
