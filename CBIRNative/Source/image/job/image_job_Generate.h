#ifndef IMAGE_JOB_GENERATE_H_
#define IMAGE_JOB_GENERATE_H_

#include "core/job/core_job_WaitableJobType.h"
#include "image/feature/image_feature_Generator.h"

namespace image::job
{
	class Generate : public core::job::WaitableJobType<Generate>
	{
	public:
		Generate(const image::feature::GeneratorParams& params) : mGenerator(params) { }
		virtual ~Generate();

		const image::feature::GeneratorResult& GetResult() const
		{
			assert(mResult.has_value());
			return mResult.value();
		}

	private:
		Generate(const Generate& other) = delete;
		Generate& operator=(const Generate&) = delete;

		virtual void ExecuteInternal() override;

		image::feature::Generator mGenerator;
		std::optional<image::feature::GeneratorResult> mResult;
	};
}

#endif // IMAGE_JOB_GENERATE_H_