﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CBIREngine.Source.Model;

namespace CBIREngine.Source.ViewModel
{
	public class BaseDocumentViewModel : BaseViewModel
	{
		private readonly BaseDocumentModel mModel;

		public BaseDocumentViewModel(BaseDocumentModel model)
		{
			mModel = model;
		}

		#region DataBindings
		public BaseDocumentModel Model
		{
			get { return mModel; }
		}
		public String Name
		{
			get { return mModel.Name; }
		}
		public String FilePath
		{
			get { return mModel.FilePath; }
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Name":
				{
					OnPropertyChanged("Name");
					break;
				}
				case "FilePath":
				{
					OnPropertyChanged("FilePath");
					break;
				}
			}
		}
		#endregion
	}
}
