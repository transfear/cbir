﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.Locality
{
	public class ThreeHorizontalLocalityModel : BaseLocalityModel
	{
		private float mfTop    = 1.0f / 3.0f;
		private float mfCenter = 1.0f / 3.0f;
		private float mfBot    = 1.0f / 3.0f;

		public ThreeHorizontalLocalityModel() { }
		public ThreeHorizontalLocalityModel(ref CBIRNative.FeatureDescriptor featureDescriptor)
		{
			unsafe
			{
				mfTop    = featureDescriptor.pRegionWeights[0];
				mfCenter = featureDescriptor.pRegionWeights[1];
				mfBot    = featureDescriptor.pRegionWeights[2];
			}
		}


		public float Top
		{
			get { return mfTop; }
			set
			{
				mfTop = value;
				OnPropertyChanged();
			}
		}

		public float Center
		{
			get { return mfCenter; }
			set
			{
				mfCenter = value;
				OnPropertyChanged();
			}
		}

		public float Bot
		{
			get { return mfBot; }
			set
			{
				mfBot = value;
				OnPropertyChanged();
			}
		}

		public override CBIRNative.eLocalityDescription LocalityDescription
		{
			get { return CBIRNative.eLocalityDescription.kLocalityDescription_3Horizontal; }
		}

		public override int NumWeights
		{
			get { return 3; }
		}

		public override float GetWeightAt(int idx)
		{
			switch (idx)
			{
				case 0:
					return Top;
				case 1:
					return Center;
				case 2:
					return Bot;
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Invalid weight index: {0}", idx));
			return -1.0f;
		}
	}
}
