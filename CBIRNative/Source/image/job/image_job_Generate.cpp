#include "image_job_Generate.h"

namespace image::job
{
	Generate::~Generate()
	{}

	void Generate::ExecuteInternal()
	{
		mResult = std::move(mGenerator.Generate());
	}
}