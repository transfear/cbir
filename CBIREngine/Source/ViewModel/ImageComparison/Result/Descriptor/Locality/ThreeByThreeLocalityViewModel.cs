﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MDL      = CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Locality;
using RegionVM = CBIREngine.Source.ViewModel.ImageComparison.Result.Descriptor.Region;

namespace CBIREngine.Source.ViewModel.ImageComparison.Result.Descriptor.Locality
{
	public class ThreeByThreeLocalityViewModel : BaseResultDescriptorViewModel
	{
		private readonly MDL.ThreeByThreeLocalityModel mModel;

		private readonly RegionVM.BaseRegionViewModel mTopLeftVM;
		private readonly RegionVM.BaseRegionViewModel mTopCenterVM;
		private readonly RegionVM.BaseRegionViewModel mTopRightVM;
		private readonly RegionVM.BaseRegionViewModel mCenterLeftVM;
		private readonly RegionVM.BaseRegionViewModel mCenterVM;
		private readonly RegionVM.BaseRegionViewModel mCenterRightVM;
		private readonly RegionVM.BaseRegionViewModel mBotLeftVM;
		private readonly RegionVM.BaseRegionViewModel mBotCenterVM;
		private readonly RegionVM.BaseRegionViewModel mBotRightVM;

		public ThreeByThreeLocalityViewModel(MDL.ThreeByThreeLocalityModel model, ResultViewModel parent) : base(model, parent)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;

			mTopLeftVM     = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.TopLeft, parent);
			mTopCenterVM   = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.TopCenter, parent);
			mTopRightVM    = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.TopRight, parent);
			mCenterLeftVM  = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.CenterLeft, parent);
			mCenterVM      = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.Center, parent);
			mCenterRightVM = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.CenterRight, parent);
			mBotLeftVM     = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.BotLeft, parent);
			mBotCenterVM   = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.BotCenter, parent);
			mBotRightVM    = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.BotRight, parent);
		}

		public override RegionVM.BaseRegionViewModel GetRegionAt(int idx)
		{
			switch (idx)
			{
				case 0:
					return mTopLeftVM;
				case 1:
					return mTopCenterVM;
				case 2:
					return mTopRightVM;
				case 3:
					return mCenterLeftVM;
				case 4:
					return mCenterVM;
				case 5:
					return mCenterRightVM;
				case 6:
					return mBotLeftVM;
				case 7:
					return mBotCenterVM;
				case 8:
					return mBotRightVM;
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Invalid region index: {0}", idx));
			return null;
		}

		public RegionVM.BaseRegionViewModel TopLeft
		{
			get { return mTopLeftVM; }
		}
		public RegionVM.BaseRegionViewModel TopCenter
		{
			get { return mTopCenterVM; }
		}
		public RegionVM.BaseRegionViewModel TopRight
		{
			get { return mTopRightVM; }
		}
		public RegionVM.BaseRegionViewModel CenterLeft
		{
			get { return mCenterLeftVM; }
		}
		public RegionVM.BaseRegionViewModel Center
		{
			get { return mCenterVM; }
		}
		public RegionVM.BaseRegionViewModel CenterRight
		{
			get { return mCenterRightVM; }
		}
		public RegionVM.BaseRegionViewModel BotLeft
		{
			get { return mBotLeftVM; }
		}
		public RegionVM.BaseRegionViewModel BotCenter
		{
			get { return mBotCenterVM; }
		}
		public RegionVM.BaseRegionViewModel BotRight
		{
			get { return mBotRightVM; }
		}

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
		}
		#endregion
	}
}
