#include "image_job_Compare.h"
#include "container/container_UnmanagedVector.h"

namespace image::job
{
	template <typename ParamsContainer>
	Compare::Compare(const CompareOptions& opt, const char* srcImage, const ParamsContainer& paramsVec, Relay& relayThread, core::job::GroupLock& groupLock)
	: mCompareOpt(opt), mSrcImage(srcImage), mRelayThread(relayThread), mGroupLock(groupLock)
	{
		const u32 uiNumParams = static_cast<u32>(paramsVec.size());
		mComparators.reserve(uiNumParams);

		for (u32 uiCurParam = 0; uiCurParam < uiNumParams; ++uiCurParam)
		{
			const image::distance::ComparatorParams& params = paramsVec[uiCurParam];
			mComparators.emplace_back(params);
		}

		mGroupLock.JobCreated();
	}

	void Compare::ExecuteInternal()
	{
		// API and this function will need to be patched if this assert fires
		static_assert(sizeof(dist_elem) == sizeof(double));

		// Calc required space
		const u32 uiNumDescriptors = static_cast<u32>(mComparators.size());
		const u32 uiCmpResultSize  = sizeof(CompareResult);
		const u32 uiDescriptorSize = sizeof(DescriptorResult) * uiNumDescriptors;
		const u32 uiRequiredSpace  = std::accumulate(mComparators.begin(), mComparators.end(), uiCmpResultSize + uiDescriptorSize,
			[](u32 accum, const image::distance::Comparator& curComparator)
			{
				const u32 uiNumRegions = curComparator.GetParams().GetGenParams().GetDescriptor().GetRegionCount();
				const u32 uiRegionSize = uiNumRegions * sizeof(RegionResult);
				return accum + uiRegionSize;
			}
		);
		std::unique_ptr<u8[]> memBuf = std::make_unique<u8[]>(uiRequiredSpace);
		u8* pRaw = memBuf.get();
		CompareResult* pCmpResult = reinterpret_cast<CompareResult*>(pRaw);
		pRaw += uiCmpResultSize;
		DescriptorResult* pDescriptorResults = reinterpret_cast<DescriptorResult*>(pRaw);
		pRaw += uiDescriptorSize;
		
		dist_elem dResult = 0.0f;
		for (u32 uiCurDescriptor = 0; uiCurDescriptor < uiNumDescriptors; ++uiCurDescriptor)
		{
			const DescriptorParameters&  curDescriptor = mCompareOpt.descriptParams[uiCurDescriptor];
			image::distance::Comparator& curComparator = mComparators[uiCurDescriptor];
			DescriptorResult& curResult = pDescriptorResults[uiCurDescriptor];

			RegionResult* pRegionResults = reinterpret_cast<RegionResult*>(pRaw);
			const u32 uiNumRegions       = curComparator.GetParams().GetGenParams().GetDescriptor().GetRegionCount();
			const u32 uiRegionSize       = uiNumRegions * sizeof(RegionResult);
			pRaw += uiRegionSize;

			const dist_elem dCurWeight = curDescriptor.weight;
			const dist_elem dCurResult = curComparator.Compare(pRegionResults);
			dResult += dCurResult * dCurWeight;

			curResult.distance       = dCurResult;
			curResult.pRegionResults = pRegionResults;
			curResult.numRegions     = uiNumRegions;
			curResult.params         = curDescriptor;
		}

		// Relay to the notification thread
		pCmpResult->refPath            = mCompareOpt.srcImagePath;
		pCmpResult->imgPath            = mSrcImage;
		pCmpResult->pDescriptorResults = pDescriptorResults;
		pCmpResult->distance           = dResult;
		pCmpResult->numDescriptors     = uiNumDescriptors;
		mRelayThread.PushElement(memBuf);
	}

	Compare::~Compare()
	{
		mGroupLock.JobExecuted();
	}

	// Force template instantiation
	template Compare::Compare(const CompareOptions&, const char*, const container::UnmanagedVector<image::distance::ComparatorParams>&, Relay&, core::job::GroupLock&);
}