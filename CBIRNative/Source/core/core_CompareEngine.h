#ifndef CORE_COMPAREENGINE_H_
#define CORE_COMPAREENGINE_H_

#include "CBIRNative.h"
#include "utils/utils_basictypes.h"
#include "job/core_job_JobSystem.h"

namespace core
{
	class CompareEngine
	{
	public:
		CompareEngine(s32 numWorkers)
		: mJobSys(numWorkers <= 0 ? mNumCores : numWorkers)
		{
		}
		
		int CompareImage(const CompareOptions* opt);

	private:

		CompareEngine(const CompareEngine&) = delete;
		CompareEngine& operator=(const CompareEngine&) = delete;

		u32  mNumCores = std::thread::hardware_concurrency();
		u8 padding[4];

		job::JobSystem mJobSys;
	};
}

#endif	// CORE_COMPAREENGINE_H_