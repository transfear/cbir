#ifndef CORE_JOB_WORKERTHREAD_H_
#define CORE_JOB_WORKERTHREAD_H_

#include "utils/utils_stdlibwrapper.h"
#include "utils/utils_basictypes.h"

namespace core::job
{
	class JobSystem;

	class WorkerThread
	{
	public:
		WorkerThread();
		~WorkerThread();

		void Start(u32 workerId, JobSystem* pJobSys);
		void Stop();
		void Join();

		u32 GetId() const;

	private:

		WorkerThread(const WorkerThread&) = delete;
		WorkerThread& operator=(const WorkerThread&) = delete;

		void DoWork();

		std::thread mThread;
		u32 mWorkerId = std::numeric_limits<u32>::max();

		volatile bool mbThreadRunning = false;
		u8 padding[3];

		JobSystem* mpJobSys = nullptr;
	};

	typedef std::unique_ptr<WorkerThread[]> WorkerThreadArrayPtr;
}

#endif // CORE_JOB_WORKERTHREAD_H_