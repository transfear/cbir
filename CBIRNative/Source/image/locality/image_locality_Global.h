#ifndef IMAGE_LOCALITY_GLOBAL_H_
#define IMAGE_LOCALITY_GLOBAL_H_

#include "utils/utils_basictypes.h"

namespace image::locality
{
	class Global
	{
	public:
		enum
		{
			kNumRegions = 1
		};


		static constexpr u32 GetRegionIndex(u32, u32, u32, u32)
		//static u32 GetRegionIndex(u32 x, u32 y, u32 w, u32 h)
		{
			return 0;
		}
	};
}

#endif // IMAGE_LOCALITY_GLOBAL_H_