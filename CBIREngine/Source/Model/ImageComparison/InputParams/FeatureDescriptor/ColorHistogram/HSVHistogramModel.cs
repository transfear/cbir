﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram
{
	public class HSVHistogramModel : BaseColorHistogramModel
	{
		private byte mNumBucketsH = 8;
		private byte mNumBucketsS = 12;
		private byte mNumBucketsV = 3;

		public HSVHistogramModel() { }
		public HSVHistogramModel(ref CBIRNative.HSVHistogramParameters nativeParams)
		{
			mNumBucketsH = nativeParams.numBucketsH;
			mNumBucketsS = nativeParams.numBucketsS;
			mNumBucketsV = nativeParams.numBucketsV;
		}

		public byte NumBucketsH
		{
			get { return mNumBucketsH; }
			set
			{
				mNumBucketsH = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsS
		{
			get { return mNumBucketsS; }
			set
			{
				mNumBucketsS = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsV
		{
			get { return mNumBucketsV; }
			set
			{
				mNumBucketsV = value;
				OnPropertyChanged();
			}
		}

		public override CBIRNative.eHistogramColorSpace ColorSpace
		{
			get { return CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HSV; }
		}
	}
}
