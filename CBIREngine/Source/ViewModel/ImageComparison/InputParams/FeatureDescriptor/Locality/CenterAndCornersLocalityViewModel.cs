﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CMD = CBIREngine.Source.Command.ImageComparison.InputParams.FeatureDescriptor.Locality;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.Locality;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor.Locality
{
	public class CenterAndCornersLocalityViewModel : BaseLocalityViewModel
	{
		private readonly MDL.CenterAndCornersLocalityModel mModel;
		public CenterAndCornersLocalityViewModel(MDL.CenterAndCornersLocalityModel model, CMD.ChangeLocalityCommand changeLocalityCmd) : base(model, changeLocalityCmd)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
		}

		#region DataBindings
		public float TopLeft
		{
			get { return mModel.TopLeft; }
			set
			{
				mModel.TopLeft = value;
				OnPropertyChanged();
			}
		}

		public float TopRight
		{
			get { return mModel.TopRight; }
			set
			{
				mModel.TopRight = value;
				OnPropertyChanged();
			}
		}

		public float BotLeft
		{
			get { return mModel.BotLeft; }
			set
			{
				mModel.BotLeft = value;
				OnPropertyChanged();
			}
		}

		public float BotRight
		{
			get { return mModel.BotRight; }
			set
			{
				mModel.BotRight = value;
				OnPropertyChanged();
			}
		}

		public float Center
		{
			get { return mModel.Center; }
			set
			{
				mModel.Center = value;
				OnPropertyChanged();
			}
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "TopLeft":
				case "TopRight":
				case "BotLeft":
				case "BotRight":
				case "Center":
					{
						OnPropertyChanged(e.PropertyName);
						break;
					}
			}
		}
		#endregion
	}
}
