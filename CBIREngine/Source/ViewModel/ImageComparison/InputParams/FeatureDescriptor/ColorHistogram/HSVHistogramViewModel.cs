﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CMD = CBIREngine.Source.Command.ImageComparison.InputParams.FeatureDescriptor;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram
{
	public class HSVHistogramViewModel : BaseColorHistogramViewModel
	{
		private readonly MDL.HSVHistogramModel mModel;

		public HSVHistogramViewModel(MDL.HSVHistogramModel model, CMD.ChangeFeatureDescriptorCommand changeDescriptCmd) : base(model, changeDescriptCmd)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
		}

		#region DataBindings
		public byte NumBucketsH
		{
			get { return mModel.NumBucketsH; }
			set
			{
				mModel.NumBucketsH = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsS
		{
			get { return mModel.NumBucketsS; }
			set
			{
				mModel.NumBucketsS = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsV
		{
			get { return mModel.NumBucketsV; }
			set
			{
				mModel.NumBucketsV = value;
				OnPropertyChanged();
			}
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "NumBucketsH":
				case "NumBucketsS":
				case "NumBucketsV":
					{
						OnPropertyChanged(e.PropertyName);
						break;
					}
			}
		}
		#endregion
	}
}
