﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Region.ColorHistogram
{
	public class RGBHistogramRegionModel : BaseColorHistogramRegionModel
	{
		public RGBHistogramRegionModel(ref CBIRNative.RGBHistogramParameters nativeHisto, ref CBIRNative.RegionResult nativeRegion, float fWeight) : base(ref nativeRegion, fWeight) { }

		public override CBIRNative.eHistogramColorSpace ColorSpace
		{
			get { return CBIRNative.eHistogramColorSpace.kHistogramColorSpace_RGB; }
		}
	}
}
