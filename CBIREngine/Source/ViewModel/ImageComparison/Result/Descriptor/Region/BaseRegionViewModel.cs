﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using MDL = CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Region;

namespace CBIREngine.Source.ViewModel.ImageComparison.Result.Descriptor.Region
{
	public abstract class BaseRegionViewModel : BaseViewModel
	{
		private readonly MDL.BaseRegionModel mModel;
		private readonly ResultViewModel     mParentResult;

		private bool mIsSelected;

		public BaseRegionViewModel(MDL.BaseRegionModel model, ResultViewModel parentResult)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;

			mParentResult = parentResult;
			mParentResult.PropertyChanged += ParentResult_PropertyChanged;
		}

		public ResultViewModel ParentResult
		{
			get { return mParentResult; }
		}

		public double DistToRef
		{
			get { return mModel.DistToRef; }
		}

		public float Weight
		{
			get { return mModel.Weight; }
		}

		public double WeightedDistToRef
		{
			get { return mModel.DistToRef * mModel.Weight; }
		}

		public bool IsSelected
		{
			get { return mIsSelected; }
			set
			{
				mIsSelected = value;
				OnPropertyChanged();
			}
		}

		public static BaseRegionViewModel CreateFromModel(MDL.BaseRegionModel model, ResultViewModel parentResult)
		{
			CBIRNative.eDescriptionMethod enumValue = model.DescriptionMethod;
			switch (enumValue)
			{
				case CBIRNative.eDescriptionMethod.kDescriptionMethod_ColorHistogram:
				{
						MDL.ColorHistogram.BaseColorHistogramRegionModel derivedModel = model as MDL.ColorHistogram.BaseColorHistogramRegionModel;
						return ColorHistogram.BaseColorHistogramRegionViewModel.CreateFromModel(derivedModel, parentResult);
				}

				case CBIRNative.eDescriptionMethod.kDescriptionMethod_GLCM:
				{
					MDL.GLCM.GLCMRegionModel derivedModel = model as MDL.GLCM.GLCMRegionModel;
					return new GLCM.GLCMRegionViewModel(derivedModel, parentResult);
				}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported description method: {0}", enumValue.ToString()));
			return null;
		}

		#region Listeners
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "DistToRef":
					{
						OnPropertyChanged(e.PropertyName);
						break;
					}
			}
		}

		private void ParentResult_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "SelectedRegion":
				{
					IsSelected = mParentResult.SelectedRegion == this;
					break;
				}
			}
		}
		#endregion
	}
}
