#ifndef CORE_JOB_WAITABLEJOBTYPE_H_
#define CORE_JOB_WAITABLEJOBTYPE_H_

#include "utils/utils_stdlibwrapper.h"
#include "core_job_NotifyLastExecutedJob.h"

namespace core::job
{
	template <class T>
	class WaitableJobType : public NotifyExecutedJob<T>
	{
		friend class NotifyExecutedJob<T>;
	public:
		static void WaitForAllJobs()
		{
			std::unique_lock<std::mutex> lock(smMtx);
			while (smCount != 0)
				smCv.wait(lock);
		}

	private:
		static void JobCreated()
		{
			++smCount;
		}

		static void JobExecuted()
		{
			int newCount;
			{
				std::lock_guard<std::mutex> lock(smMtx);
				newCount = --smCount;
			}

			assert(newCount >= 0);
			if (newCount == 0)
				smCv.notify_all();
		}

		static std::mutex smMtx;
		static std::condition_variable smCv;
		static std::atomic<int> smCount;
	};


	template <class T> std::mutex              WaitableJobType<T>::smMtx;
	template <class T> std::condition_variable WaitableJobType<T>::smCv;
	template <class T> std::atomic<int>        WaitableJobType<T>::smCount;
}

#endif // CORE_JOB_WAITABLEJOBTYPE_H_