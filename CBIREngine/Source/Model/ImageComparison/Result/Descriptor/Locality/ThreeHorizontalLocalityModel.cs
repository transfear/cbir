﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using CBIREngine.Source.Utils;

namespace CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Locality
{
	public class ThreeHorizontalLocalityModel : BaseResultDescriptorModel
	{
		private readonly Region.BaseRegionModel mTop;
		private readonly Region.BaseRegionModel mCenter;
		private readonly Region.BaseRegionModel mBot;

		public ThreeHorizontalLocalityModel(ref CBIRNative.DescriptorResult nativeDescript) : base(ref nativeDescript)
		{
			System.Diagnostics.Debug.Assert(nativeDescript.inParams.feature.localityDescription == CBIRNative.eLocalityDescription.kLocalityDescription_3Horizontal);
			System.Diagnostics.Debug.Assert(nativeDescript.numRegions == 3);

			CBIRNative.RegionResult[] regionArray = MarshalUtils.GetArray<CBIRNative.RegionResult>(nativeDescript.pRegionResults, 3);
			unsafe
			{
				mTop    = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[0], nativeDescript.inParams.feature.pRegionWeights[0]);
				mCenter = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[1], nativeDescript.inParams.feature.pRegionWeights[1]);
				mBot    = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[2], nativeDescript.inParams.feature.pRegionWeights[2]);
			}
		}

		public override int NumRegions
		{
			get { return 3; }
		}

		public override CBIRNative.eLocalityDescription LocalityType
		{
			get { return CBIRNative.eLocalityDescription.kLocalityDescription_3Horizontal; }
		}

		public Region.BaseRegionModel Top
		{
			get { return mTop; }
		}

		public Region.BaseRegionModel Center
		{
			get { return mCenter; }
		}

		public Region.BaseRegionModel Bot
		{
			get { return mBot; }
		}

		public override Region.BaseRegionModel GetRegionAt(int idx)
		{
			switch (idx)
			{
				case 0:
					return Top;
				case 1:
					return Center;
				case 2:
					return Bot;
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Invalid weight index: {0}", idx));
			return null;
		}
	}
}
