﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

using CMD = CBIREngine.Source.Command.ImageComparison.Result;
using MDL = CBIREngine.Source.Model.ImageComparison.Result;
using CBIREngine.Source.Utils;

namespace CBIREngine.Source.ViewModel.ImageComparison.Result
{
	public class ResultViewModel : BaseViewModel
	{
		private readonly MDL.ResultModel mModel;
		private readonly CMD.SetSelectedRegionCommand mSetSelectedRegionCmd;

		private readonly ObservableRangeCollection<Descriptor.BaseResultDescriptorViewModel> mDescriptors = new ObservableRangeCollection<Descriptor.BaseResultDescriptorViewModel>();
		
		private Descriptor.BaseResultDescriptorViewModel mSelectedDescriptor;
		private Descriptor.Region.BaseRegionViewModel    mSelectedRegion;

		public ResultViewModel(MDL.ResultModel model)
		{
			mSetSelectedRegionCmd = new CMD.SetSelectedRegionCommand(this);

			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
			mModel.Descriptors.CollectionChanged += Model_DescriptorListChanged;

			// Initialize internal collection
			foreach (MDL.Descriptor.BaseResultDescriptorModel descript in mModel.Descriptors)
				mDescriptors.Add(Descriptor.BaseResultDescriptorViewModel.CreateFromModel(descript, this));

			SelectedDescriptor = mDescriptors.FirstOrDefault();
		}

		#region DataBindings
		public CMD.SetSelectedRegionCommand SelectRegionCommand
		{
			get { return mSetSelectedRegionCmd; }
		}

		public MDL.ResultModel Model
		{
			get { return mModel; }
		}

		public string ComparedImagePath
		{
			get { return mModel.ComparedImagePath; }
		}

		public string FileName
		{
			get { return Path.GetFileName(mModel.ComparedImagePath); }
		}

		public string ReferenceImagePath
		{
			get { return mModel.ReferenceImagePath; }
		}

		public string ReferenceFileName
		{
			get { return Path.GetFileName(mModel.ReferenceImagePath); }
		}

		public double DistToRef
		{
			get { return mModel.DistToRef; }
		}

		public BitmapImage ImageSrc
		{
			get
			{
				BitmapImage bi = new BitmapImage(new Uri(mModel.ComparedImagePath));
				bi.Freeze();
				return bi;
			}
		}

		public BitmapImage ReferenceImageSrc
		{
			get
			{
				BitmapImage bi = new BitmapImage(new Uri(mModel.ReferenceImagePath));
				bi.Freeze();
				return bi;
			}
		}

		public ObservableRangeCollection<Descriptor.BaseResultDescriptorViewModel> Descriptors
		{
			get { return mDescriptors; }
		}

		public Descriptor.BaseResultDescriptorViewModel SelectedDescriptor
		{
			get { return mSelectedDescriptor; }
			set
			{
				mSelectedDescriptor = value;
				OnPropertyChanged();

				SelectedRegion = mSelectedDescriptor.GetRegionAt(0);
			}
		}

		public Descriptor.Region.BaseRegionViewModel SelectedRegion
		{
			get { return mSelectedRegion; }
			set
			{
				mSelectedRegion = value;
				OnPropertyChanged();
			}
		}
		#endregion


		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "ComparedImagePath":
					{
						OnPropertyChanged(e.PropertyName);
						OnPropertyChanged("FileName");
						break;
					}
				case "DistToRef":
					{
						OnPropertyChanged(e.PropertyName);
						break;
					}
			}
		}

		private void Model_DescriptorListChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			switch (e.Action)
			{
				case NotifyCollectionChangedAction.Add:
					{
						foreach (MDL.Descriptor.BaseResultDescriptorModel newEntry in e.NewItems)
							mDescriptors.Add(Descriptor.BaseResultDescriptorViewModel.CreateFromModel(newEntry, this));
						break;
					}

				case NotifyCollectionChangedAction.Remove:
					{
						foreach (MDL.Descriptor.BaseResultDescriptorModel oldEntry in e.OldItems)
						{
							Descriptor.BaseResultDescriptorViewModel vm = mDescriptors.First(x => x.Model == oldEntry);
							mDescriptors.Remove(vm);
						}
						break;
					}

				case NotifyCollectionChangedAction.Reset:
					{
						mDescriptors.Clear();
						ObservableRangeCollection<MDL.Descriptor.BaseResultDescriptorModel> srcList = (ObservableRangeCollection<MDL.Descriptor.BaseResultDescriptorModel>)sender;
						foreach (MDL.Descriptor.BaseResultDescriptorModel newEntry in srcList)
						{
							Descriptor.BaseResultDescriptorViewModel vm = Descriptor.BaseResultDescriptorViewModel.CreateFromModel(newEntry, this);
							mDescriptors.Add(vm);
						}
						break;
					}
			}
		}
		#endregion
	}
}
