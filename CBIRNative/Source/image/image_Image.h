#ifndef IMAGE_IMAGE_H_
#define IMAGE_IMAGE_H_

#include "utils/utils_stdlibwrapper.h"
#include "utils/utils_basictypes.h"

namespace image
{
	class Image
	{
		public: 
			Image() { }
			Image(Image&& other) : mRawData(std::move(mRawData)), mWidth(other.mWidth), mHeight(other.mHeight), mNumComponents(other.mNumComponents) { }
			Image& operator=(Image&& other)
			{
				mRawData       = std::move(other.mRawData);
				mWidth         = other.mWidth;
				mHeight        = other.mHeight;
				mNumComponents = other.mNumComponents;
				return *this;
			}

			void Reset(u8* pRawData, u32 width, u32 height, u8 numComponents);

			u32 GetWidth() const;
			u32 GetHeight() const;
			u8  GetNumComponents() const;

			const u8* GetRawData() const;

		private:
			Image(const Image&) = delete;
			Image& operator=(const Image&) = delete;

			std::unique_ptr<u8[]> mRawData;

			u32 mWidth = 0;
			u32 mHeight = 0;

			u8 mNumComponents = 0;
			u8 padding[7];
	};
}

#endif // IMAGE_IMAGE_H_