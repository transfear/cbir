﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using API;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams;
using VM  = CBIREngine.Source.ViewModel.ImageComparison.InputParams;

namespace CBIREngine.Source.Command.ImageComparison.InputParams.DistanceDescriptor
{
	public class ChangeDistanceDescriptorCommand : ICommand
	{
		private readonly VM.DescriptorParamsViewModel mVM;
		private readonly MDL.DescriptorParamsModel    mModel;
		public event EventHandler CanExecuteChanged;

		public ChangeDistanceDescriptorCommand(VM.DescriptorParamsViewModel vm)
		{
			mVM = vm;
			mModel = mVM.Model;
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			// Create a new distance descriptor based on the wanted parameter
			CBIRNative.eDistanceMethod enumValue = (CBIRNative.eDistanceMethod)parameter;
			MDL.DistanceDescriptor.BaseDistanceDescriptorModel newModel = MDL.DistanceDescriptor.BaseDistanceDescriptorModel.CreateFromEnumValue(enumValue);

			mModel.DistanceDescriptor = newModel;
		}

		public void RaiseCanExecuteChanged()
		{
			if (CanExecuteChanged != null)
			{
				CanExecuteChanged(this, EventArgs.Empty);
			}
		}
	}
}
