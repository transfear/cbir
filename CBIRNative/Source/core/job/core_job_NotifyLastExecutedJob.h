#ifndef CORE_JOB_NOTIFYLASTEXECUTEDJOB_H_
#define CORE_JOB_NOTIFYLASTEXECUTEDJOB_H_

#include "core_job_Job.h"

namespace core::job
{
	template <typename T>
	class NotifyExecutedJob : public Job
	{
	public:
		NotifyExecutedJob()
		{
			T::JobCreated();
		}

		virtual void Execute() override
		{
			ExecuteInternal();
			T::JobExecuted();
		}
	};
}

#endif // CORE_JOB_NOTIFYLASTEXECUTEDJOB_H_