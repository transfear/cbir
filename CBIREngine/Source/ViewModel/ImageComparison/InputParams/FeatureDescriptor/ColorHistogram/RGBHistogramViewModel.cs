﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CMD = CBIREngine.Source.Command.ImageComparison.InputParams.FeatureDescriptor;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram
{
	public class RGBHistogramViewModel : BaseColorHistogramViewModel
	{
		private readonly MDL.RGBHistogramModel mModel;

		public RGBHistogramViewModel(MDL.RGBHistogramModel model, CMD.ChangeFeatureDescriptorCommand changeDescriptCmd) : base(model, changeDescriptCmd)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
		}

		#region DataBindings
		public byte NumBucketsR
		{
			get { return mModel.NumBucketsR; }
			set
			{
				mModel.NumBucketsR = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsG
		{
			get { return mModel.NumBucketsG; }
			set
			{
				mModel.NumBucketsG = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsB
		{
			get { return mModel.NumBucketsB; }
			set
			{
				mModel.NumBucketsB = value;
				OnPropertyChanged();
			}
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "NumBucketsR":
				case "NumBucketsG":
				case "NumBucketsB":
					{
						OnPropertyChanged(e.PropertyName);
						break;
					}
			}
		}
		#endregion
	}
}
