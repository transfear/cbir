﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CMD = CBIREngine.Source.Command.ImageComparison.InputParams.FeatureDescriptor;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram
{
	public class HMMDHistogramViewModel : BaseColorHistogramViewModel
	{
		private readonly MDL.HMMDHistogramModel mModel;

		public HMMDHistogramViewModel(MDL.HMMDHistogramModel model, CMD.ChangeFeatureDescriptorCommand changeDescriptCmd) : base(model, changeDescriptCmd)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
		}

		#region DataBindings
		public byte NumBucketsH
		{
			get { return mModel.NumBucketsH; }
			set
			{
				mModel.NumBucketsH = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsMin
		{
			get { return mModel.NumBucketsMin; }
			set
			{
				mModel.NumBucketsMin = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsMax
		{
			get { return mModel.NumBucketsMax; }
			set
			{
				mModel.NumBucketsMax = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsD
		{
			get { return mModel.NumBucketsD; }
			set
			{
				mModel.NumBucketsD = value;
				OnPropertyChanged();
			}
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "NumBucketsH":
				case "NumBucketsMin":
				case "NumBucketsMax":
				case "NumBucketsD":
					{
						OnPropertyChanged(e.PropertyName);
						break;
					}
			}
		}
		#endregion
	}
}
