﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace API
{
	public static class CBIRNative
	{
		public enum eDescriptionMethod : byte
		{
			kDescriptionMethod_ColorHistogram,
			kDescriptionMethod_GLCM
		};

		public enum eHistogramColorSpace : byte
		{
			kHistogramColorSpace_RGB,
			kHistogramColorSpace_HSV,
			kHistogramColorSpace_HMMD
		};

		public enum eLocalityDescription : byte
		{
			kLocalityDescription_Global,            // 1 region
			kLocalityDescription_CenterAndCorners,  // 5 regions
			kLocalityDescription_3Horizontal,       // 3 regions
			kLocalityDescription_3x3                // 9 regions
		};

		public enum eDistanceMethod : byte
		{
			kDistanceMethod_Euclidean,
			kDistanceMethod_ChiSquared,
			kDistanceMethod_EarthMover
		};

		[StructLayout(LayoutKind.Sequential, Pack=8)]
		public unsafe struct HSVHistogramParameters
		{
			public byte numBucketsH;
			public byte numBucketsS;
			public byte numBucketsV;

			public fixed byte padding[5];
		}

		[StructLayout(LayoutKind.Sequential, Pack = 8)]
		public unsafe struct RGBHistogramParameters
		{
			public byte numBucketsR;
			public byte numBucketsG;
			public byte numBucketsB;

			public fixed byte padding[5];
		}

		[StructLayout(LayoutKind.Sequential, Pack = 8)]
		public unsafe struct HMMDHistogramParameters
		{
			public byte numBucketsH;
			public byte numBucketsMin;
			public byte numBucketsMax;
			public byte numBucketsD;

			public fixed byte padding[4];
		}

		// Union emulation
		[StructLayout(LayoutKind.Explicit)]
		public struct ColorHistogramUnion
		{
			[FieldOffset(0)]
			public HSVHistogramParameters hsvParams;
			[FieldOffset(0)]
			public RGBHistogramParameters rgbParams;
			[FieldOffset(0)]
			public HMMDHistogramParameters hmmdParams;
		}

		[StructLayout(LayoutKind.Sequential, Pack = 8)]
		public unsafe struct ColorHistogramParameters
		{
			public eHistogramColorSpace colorSpace;
			public fixed byte padding[7];

			public ColorHistogramUnion union;
		}

		// Union emulation
		[StructLayout(LayoutKind.Explicit)]
		public struct FeatureDescriptorMethod
		{
			[FieldOffset(0)]
			public ColorHistogramParameters colorHist;
		}

		[StructLayout(LayoutKind.Sequential)]
		public unsafe struct FeatureDescriptor
		{
			public eDescriptionMethod   descriptionMethod;      // Must be a member of eDescriptionMethod
			public eLocalityDescription localityDescription;    // Must be a member of eLocalityDescription

			public float* pRegionWeights;      // Weights. Array size expected to be equal to number of regions in localityDescription. Sum assumed to be > 0. Each weight assumed to be >= 0.

			public FeatureDescriptorMethod descriptor;
		}

		[StructLayout(LayoutKind.Sequential, Pack = 8)]
		public unsafe struct DistanceDescriptor
		{
			public eDistanceMethod distanceMethod;         // Must be a member of eDistanceMethod
			public fixed byte padding[7];

			public int dummy;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct DescriptorParameters
		{
			public FeatureDescriptor  feature;
			public DistanceDescriptor distance;
			public float              weight;    // weight of this descriptor
		};

		[StructLayout(LayoutKind.Sequential)]
		public struct RegionResult
		{
			public double distance;

			// TODO: Histogram and other data
		}

		[StructLayout(LayoutKind.Sequential)]
		public unsafe struct DescriptorResult
		{
			public DescriptorParameters inParams;
			
			public uint numRegions;               // region count
			public fixed byte padding[4];

			public IntPtr pRegionResults;         // 1 result per region (RegionResult*)
			public double distance;               // distance for this descriptor, weighted by region
		};

		[StructLayout(LayoutKind.Sequential)]
		public unsafe struct CompareResult
		{
			[MarshalAs(UnmanagedType.LPStr)]
			public string refPath;             // Path of the reference image
			[MarshalAs(UnmanagedType.LPStr)]
			public string imgPath;             // Path of the compared image
			public IntPtr pDescriptorResults;  // 1 result per descriptor (DescriptorResult*)
			public double distance;            // weighted distance of all descriptors
			public uint   numDescriptors;      // descriptor count

			public fixed byte padding[4];
		};

		// ppResults is expected to be an array of IntPtr, each entry being in fact a pointer to a CompareResult
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		public delegate bool ProgressCallback(ulong userData, IntPtr ppResults, uint numResults);

		[StructLayout(LayoutKind.Sequential)]
		public unsafe struct CompareOptions
		{
			public DescriptorParameters* descriptParams;

			public ProgressCallback cb;

			public string srcImagePath;
			public string rootImageFolder;
			public string cacheFolder;

			public ulong userData;

			public uint numDescriptors; 

			[MarshalAs(UnmanagedType.U1)]
			public bool checkSubFolders;
			[MarshalAs(UnmanagedType.U1)]
			public bool compressCache;

			public fixed byte padding[2];
		}

		[DllImport("CBIRNative.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int CompareImage(ref CompareOptions opt, int numWorkers);
	}
}
