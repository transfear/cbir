﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using API;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor;
using VM  = CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor;

namespace CBIREngine.Source.Command.ImageComparison.InputParams.FeatureDescriptor.Locality
{
	public class ChangeLocalityCommand : ICommand
	{
		private readonly VM.BaseFeatureDescriptorViewModel mVM;
		private readonly MDL.BaseFeatureDescriptorModel    mModel;
		public event EventHandler CanExecuteChanged;

		public ChangeLocalityCommand(VM.BaseFeatureDescriptorViewModel vm)
		{
			mVM = vm;
			mModel = mVM.Model;
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			CBIRNative.eLocalityDescription locality = (CBIRNative.eLocalityDescription)parameter;

			MDL.Locality.BaseLocalityModel newModel = MDL.Locality.BaseLocalityModel.CreateFromEnum(locality);
			mModel.Locality = newModel;
		}

		public void RaiseCanExecuteChanged()
		{
			if (CanExecuteChanged != null)
			{
				CanExecuteChanged(this, EventArgs.Empty);
			}
		}
	}
}
