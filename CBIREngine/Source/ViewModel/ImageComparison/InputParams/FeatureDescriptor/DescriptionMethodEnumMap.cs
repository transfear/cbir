﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor
{
	public class DescriptionMethodEnumMap
	{
		public CBIRNative.eDescriptionMethod EnumValue { get; set; }
		public string StringValue { get; set; }

		static public List<DescriptionMethodEnumMap> EnumList { get; set; } = new List<DescriptionMethodEnumMap>()
		{
			new DescriptionMethodEnumMap() { EnumValue = CBIRNative.eDescriptionMethod.kDescriptionMethod_ColorHistogram, StringValue = "Color histogram" },
			new DescriptionMethodEnumMap() { EnumValue = CBIRNative.eDescriptionMethod.kDescriptionMethod_GLCM,           StringValue = "GLCM" }
		};
	}
}
