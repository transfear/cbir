#ifndef IMAGE_LOCALITY_THREEBYTHREE_H_
#define IMAGE_LOCALITY_THREEBYTHREE_H_

#include "utils/utils_basictypes.h"
#include "utils/utils_stdlibwrapper.h"

namespace image::locality
{
	class ThreeByThree
	{
	public:
		enum
		{
			kNumRegions = 9
		};

		static constexpr u32 GetRegionIndex(u32 x, u32 y, u32 w, u32 h)
		{
			const u32 firstBoundX = w / 3;
			const u32 secndBoundX = 2 * firstBoundX;

			const u32 firstBoundY = h / 3;
			const u32 secndBoundY = 2 * firstBoundY;

			if (y < firstBoundY)
			{
				// 0, 1 or 2
				if (x < firstBoundX)
					return 0u;
				else
					return x < secndBoundX ? 1u : 2u;

			}
			else if (y < secndBoundY)
			{
				// 3, 4 or 5
				if (x < firstBoundX)
					return 3u;
				else
					return x < secndBoundX ? 4u : 5u;
			}

			// 6, 7 or 8
			if (x < firstBoundX)
				return 6u;
					
			return x < secndBoundX ? 7u : 8u;
		}
	};
}

#endif // IMAGE_LOCALITY_THREEBYTHREE_H_