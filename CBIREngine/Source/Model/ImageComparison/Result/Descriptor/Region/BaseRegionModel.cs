﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Region
{
	public abstract class BaseRegionModel : BaseModel
	{
		private readonly double mDistToRef;
		private readonly float  mfWeight;

		public BaseRegionModel(ref CBIRNative.RegionResult nativeRegion, float fWeight) 
		{
			mDistToRef = nativeRegion.distance;
			mfWeight   = fWeight;
		}

		public double DistToRef
		{
			get { return mDistToRef; }
		}

		public float Weight
		{
			get { return mfWeight; }
		}

		public abstract CBIRNative.eDescriptionMethod DescriptionMethod { get; }

		#region HelperFunctions
		public static BaseRegionModel CreateFromParams(ref CBIRNative.DescriptorResult nativeDescript, ref CBIRNative.RegionResult nativeRegion, float fWeight)
		{
			CBIRNative.eDescriptionMethod enumValue = nativeDescript.inParams.feature.descriptionMethod;
			switch (enumValue)
			{
				case CBIRNative.eDescriptionMethod.kDescriptionMethod_ColorHistogram:
				{
					return ColorHistogram.BaseColorHistogramRegionModel.CreateFromParams(ref nativeDescript.inParams.feature.descriptor.colorHist, ref nativeRegion, fWeight);
				}
				case CBIRNative.eDescriptionMethod.kDescriptionMethod_GLCM:
				{
					System.Diagnostics.Debug.Assert(false, "todo");
					return new GLCM.GLCMRegionModel(ref nativeRegion, fWeight);
				}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported descriptor method: {0}", enumValue.ToString()));
			return null;
		}
		#endregion
	}
}
