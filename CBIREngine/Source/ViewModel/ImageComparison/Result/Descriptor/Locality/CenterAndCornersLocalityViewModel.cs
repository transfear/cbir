﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MDL      = CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Locality;
using RegionVM = CBIREngine.Source.ViewModel.ImageComparison.Result.Descriptor.Region;

namespace CBIREngine.Source.ViewModel.ImageComparison.Result.Descriptor.Locality
{
	public class CenterAndCornersLocalityViewModel : BaseResultDescriptorViewModel
	{
		private readonly MDL.CenterAndCornersLocalityModel mModel;

		private readonly RegionVM.BaseRegionViewModel mTopLeftVM;
		private readonly RegionVM.BaseRegionViewModel mTopRightVM;
		private readonly RegionVM.BaseRegionViewModel mBotLeftVM;
		private readonly RegionVM.BaseRegionViewModel mBotRightVM;
		private readonly RegionVM.BaseRegionViewModel mCenterVM;

		public CenterAndCornersLocalityViewModel(MDL.CenterAndCornersLocalityModel model, ResultViewModel parent) : base(model, parent)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;

			mTopLeftVM  = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.TopLeft, parent);
			mTopRightVM = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.TopRight, parent);
			mBotLeftVM  = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.BotLeft, parent);
			mBotRightVM = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.BotRight, parent);
			mCenterVM   = RegionVM.BaseRegionViewModel.CreateFromModel(mModel.Center, parent);
		}

		public override RegionVM.BaseRegionViewModel GetRegionAt(int idx)
		{
			switch (idx)
			{ 
				case 0:
					return mTopLeftVM;
				case 1:
					return mTopRightVM;
				case 2:
					return mBotLeftVM;
				case 3:
					return mBotRightVM;
				case 4:
					return mCenterVM;
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Invalid region index: {0}", idx));
			return null;
		}

		public RegionVM.BaseRegionViewModel TopLeft
		{
			get { return mTopLeftVM; }
		}
		public RegionVM.BaseRegionViewModel TopRight
		{
			get { return mTopRightVM; }
		}
		public RegionVM.BaseRegionViewModel BotLeft
		{
			get { return mBotLeftVM; }
		}
		public RegionVM.BaseRegionViewModel BotRight
		{
			get { return mBotRightVM; }
		}
		public RegionVM.BaseRegionViewModel Center
		{
			get { return mCenterVM; }
		}

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
		}
		#endregion
	}
}
