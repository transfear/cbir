﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using MDL = CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Region.ColorHistogram;

namespace CBIREngine.Source.ViewModel.ImageComparison.Result.Descriptor.Region.ColorHistogram
{
	public abstract class BaseColorHistogramRegionViewModel : BaseRegionViewModel
	{
		private readonly MDL.BaseColorHistogramRegionModel mModel;

		public BaseColorHistogramRegionViewModel(MDL.BaseColorHistogramRegionModel model, ResultViewModel parentResult) : base(model, parentResult)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
		}

		public static BaseColorHistogramRegionViewModel CreateFromModel(MDL.BaseColorHistogramRegionModel model, ResultViewModel parentResult)
		{
			CBIRNative.eHistogramColorSpace enumValue = model.ColorSpace;
			switch (enumValue)
			{
				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HMMD:
				{
					MDL.HMMDHistogramRegionModel derivedModel = model as MDL.HMMDHistogramRegionModel;
					return new HMMDHistogramRegionViewModel(derivedModel, parentResult);
				}

				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HSV:
				{
					MDL.HSVHistogramRegionModel derivedModel = model as MDL.HSVHistogramRegionModel;
					return new HSVHistogramRegionViewModel(derivedModel, parentResult);
				}

				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_RGB:
				{
					MDL.RGBHistogramRegionModel derivedModel = model as MDL.RGBHistogramRegionModel;
					return new RGBHistogramRegionViewModel(derivedModel, parentResult);
				}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported histogram colorspace: {0}", enumValue.ToString()));
			return null;
		}

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				default:
				{
					//TODO
					break;
				}
			}
		}
		#endregion
	}
}
