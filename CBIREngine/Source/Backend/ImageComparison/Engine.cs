﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using API;
using MDL = CBIREngine.Source.Model.ImageComparison;
using CBIREngine.Source.Utils;

namespace CBIREngine.Source.Backend.ImageComparison
{
	public class Engine
	{
		private static long smNextKey = 0;
		private static ConcurrentDictionary<long, Engine> smDictionary = new ConcurrentDictionary<long, Engine>();

		private readonly MDL.ImageComparisonModel mModel;

		private bool          mShouldStop  = false;
		private object        mRunningLock = new object();
		private readonly long mKey;

		public static Engine CreateEngine(MDL.ImageComparisonModel model)
		{
			long key = Interlocked.Increment(ref smNextKey);

			Engine newEngine = new Engine(model, key);
			if (!smDictionary.TryAdd(key, newEngine))
				System.Diagnostics.Debug.Assert(false);

			return newEngine;
		}

		public static void ReleaseEngine(Engine engine)
		{
			// Assuming engine is already stopped;
			System.Diagnostics.Debug.Assert(!engine.mShouldStop);

			Engine outEngine;
			if (!smDictionary.TryRemove(engine.Key, out outEngine))
				System.Diagnostics.Debug.Assert(false);

			System.Diagnostics.Debug.Assert(outEngine.Key == engine.Key);
		}

		private Engine(MDL.ImageComparisonModel model, long key)
		{
			mKey = key;
			mModel = model;
		}

		public long Key
		{
			get { return mKey; }
		}

		public async void Start()
		{
			mShouldStop = false;
			mModel.Results.Clear();
			await Task.Run(() => StartTask());
		}

		public void Stop()
		{
			// Wait for the running lock to finish
			mShouldStop = true;
			lock (mRunningLock) { }
		}

		private void StartTask()
		{
			lock (mRunningLock)
			{
				unsafe
				{
					CBIRNative.CompareOptions nativeOptions = AllocCompareOptions(mModel);

					int result = CBIRNative.CompareImage(ref nativeOptions, mModel.InputParams.NumThreads);
					System.Diagnostics.Debug.Assert(result >= 0);

					FreeCompareOptions(nativeOptions);
				}

				// Task finished
				mShouldStop = false;
				Engine.ReleaseEngine(mModel.Engine);
				mModel.Engine = null;
			}
		}

		private unsafe CBIRNative.CompareOptions AllocCompareOptions(MDL.ImageComparisonModel model)
		{
			MDL.InputParams.InputParamsModel inputParams = model.InputParams;

			int numDescriptors = inputParams.Descriptors.Count;

			// Main struct
			CBIRNative.CompareOptions nativeOptions = new CBIRNative.CompareOptions();

			IntPtr ptrDescriptorParams  = Marshal.AllocHGlobal(sizeof(CBIRNative.DescriptorParameters) * numDescriptors);

			nativeOptions.descriptParams  = (CBIRNative.DescriptorParameters*)ptrDescriptorParams.ToPointer();
			nativeOptions.cb              = StaticNativeProgressCB;

			nativeOptions.srcImagePath    = inputParams.SourceImage;
			nativeOptions.rootImageFolder = inputParams.ImageLib;
			nativeOptions.cacheFolder     = inputParams.CacheFolder;

			unchecked { nativeOptions.userData = (ulong)this.Key; }

			nativeOptions.numDescriptors = (uint)numDescriptors;

			nativeOptions.checkSubFolders = inputParams.CheckSubfolders;
			nativeOptions.compressCache   = inputParams.CompressCache;

			// Loop on each descriptor
			for (int curDescriptIdx = 0; curDescriptIdx < numDescriptors; ++curDescriptIdx)
			{
				MDL.InputParams.DescriptorParamsModel descParams = inputParams.Descriptors.ElementAt(curDescriptIdx);
				MDL.InputParams.FeatureDescriptor.BaseFeatureDescriptorModel   featDesc = descParams.FeatureDescriptor;
				MDL.InputParams.DistanceDescriptor.BaseDistanceDescriptorModel distDesc = descParams.DistanceDescriptor;

				// Init feature descriptor
				CBIRNative.DescriptorParameters* nativeDesc = &nativeOptions.descriptParams[curDescriptIdx];
				nativeDesc->feature.descriptionMethod   = featDesc.DescriptionMethod;
				nativeDesc->feature.localityDescription = featDesc.Locality.LocalityDescription;

				int numWeights = featDesc.Locality.NumWeights;
				IntPtr ptrLocalityWeights = Marshal.AllocHGlobal(sizeof(float) * numWeights);
				nativeDesc->feature.pRegionWeights = (float*)ptrLocalityWeights.ToPointer();

				// Init region weights
				for (int curRegionIdx = 0; curRegionIdx < numWeights; ++curRegionIdx)
					nativeDesc->feature.pRegionWeights[curRegionIdx] = featDesc.Locality.GetWeightAt(curRegionIdx);

				// Descriptor mode
				switch (featDesc.DescriptionMethod)
				{
					case CBIRNative.eDescriptionMethod.kDescriptionMethod_ColorHistogram:
						{
							MDL.InputParams.FeatureDescriptor.ColorHistogram.BaseColorHistogramModel colorHistDesc = featDesc as MDL.InputParams.FeatureDescriptor.ColorHistogram.BaseColorHistogramModel;
							nativeDesc->feature.descriptor.colorHist.colorSpace = colorHistDesc.ColorSpace;
							
							switch (colorHistDesc.ColorSpace)
							{
								case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HSV:
									{
										MDL.InputParams.FeatureDescriptor.ColorHistogram.HSVHistogramModel hsvDesc = colorHistDesc as MDL.InputParams.FeatureDescriptor.ColorHistogram.HSVHistogramModel;
										nativeDesc->feature.descriptor.colorHist.union.hsvParams.numBucketsH = hsvDesc.NumBucketsH;
										nativeDesc->feature.descriptor.colorHist.union.hsvParams.numBucketsS = hsvDesc.NumBucketsS;
										nativeDesc->feature.descriptor.colorHist.union.hsvParams.numBucketsV = hsvDesc.NumBucketsV;
										break;
									}

								case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_RGB:
									{
										MDL.InputParams.FeatureDescriptor.ColorHistogram.RGBHistogramModel rgbDesc = colorHistDesc as MDL.InputParams.FeatureDescriptor.ColorHistogram.RGBHistogramModel;
										nativeDesc->feature.descriptor.colorHist.union.rgbParams.numBucketsR = rgbDesc.NumBucketsR;
										nativeDesc->feature.descriptor.colorHist.union.rgbParams.numBucketsG = rgbDesc.NumBucketsG;
										nativeDesc->feature.descriptor.colorHist.union.rgbParams.numBucketsB = rgbDesc.NumBucketsB;
										break;
									}

								case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HMMD:
									{
										MDL.InputParams.FeatureDescriptor.ColorHistogram.HMMDHistogramModel hmmdDesc = colorHistDesc as MDL.InputParams.FeatureDescriptor.ColorHistogram.HMMDHistogramModel;
										nativeDesc->feature.descriptor.colorHist.union.hmmdParams.numBucketsH   = hmmdDesc.NumBucketsH;
										nativeDesc->feature.descriptor.colorHist.union.hmmdParams.numBucketsMin = hmmdDesc.NumBucketsMin;
										nativeDesc->feature.descriptor.colorHist.union.hmmdParams.numBucketsMax = hmmdDesc.NumBucketsMax;
										nativeDesc->feature.descriptor.colorHist.union.hmmdParams.numBucketsD   = hmmdDesc.NumBucketsD;
										break;
									}

								default:
									{
										System.Diagnostics.Debug.Assert(false, String.Format("Unsupported histogram colorspace: {0}", colorHistDesc.ColorSpace.ToString()));
										break;
									}
							}

							break;
						}
					default:
						{
							// TODO: Only color histogram is currently supported!
							System.Diagnostics.Debug.Assert(false, String.Format("Unsupported descriptor format: {0}", featDesc.DescriptionMethod.ToString()));
							break;
						}
				}

				// Init distance descriptor
				nativeDesc->distance.distanceMethod = distDesc.DistanceMethod;

				// Init descriptor weight
				nativeDesc->weight = descParams.Weight;
			}

			return nativeOptions;
		}

		private unsafe void FreeCompareOptions(CBIRNative.CompareOptions nativeOptions)
		{
			// Free each descriptor dynamically allocated memory
			uint numDescriptors = nativeOptions.numDescriptors;
			for (int curDescriptIdx = 0; curDescriptIdx < numDescriptors; ++curDescriptIdx)
			{
				IntPtr ptrRegionWeights = new IntPtr(nativeOptions.descriptParams[curDescriptIdx].feature.pRegionWeights);
				Marshal.FreeHGlobal(ptrRegionWeights);
			}

			// Free allocated memory for the descriptors
			IntPtr ptrDescriptors = new IntPtr(nativeOptions.descriptParams);
			Marshal.FreeHGlobal(ptrDescriptors);
		}

		private bool NativeProgressCB(IntPtr ppResults, uint numResults)
		{
			List<MDL.Result.ResultModel> localResultList = new List<MDL.Result.ResultModel>();

			IntPtr[] resultArray = MarshalUtils.GetArray<IntPtr>(ppResults, numResults);
			foreach (IntPtr pResult in resultArray)
			{
				CBIRNative.CompareResult result = (CBIRNative.CompareResult)Marshal.PtrToStructure(pResult, typeof(CBIRNative.CompareResult));
				
				MDL.Result.ResultModel newModel = new MDL.Result.ResultModel();
				newModel.ReferenceImagePath = result.refPath;
				newModel.ComparedImagePath  = result.imgPath;
				newModel.DistToRef          = result.distance;

				// Loop on each descriptor
				CBIRNative.DescriptorResult[] descriptorArray = MarshalUtils.GetArray<CBIRNative.DescriptorResult>(result.pDescriptorResults, result.numDescriptors);
				for (int descriptIdx = 0; descriptIdx < result.numDescriptors; ++descriptIdx)
				{
					MDL.Result.Descriptor.BaseResultDescriptorModel newDescriptor = MDL.Result.Descriptor.BaseResultDescriptorModel.CreateFromParams(ref descriptorArray[descriptIdx]);					
					newModel.Descriptors.Add(newDescriptor);
				}
				localResultList.Add(newModel);
			}

			// Add the results to our model
			App.Current.Dispatcher.BeginInvoke(new Action(() =>
			{
				mModel.Results.AddRange(localResultList);
			}));

			return !mShouldStop;
		}

		private static bool StaticNativeProgressCB(ulong userData, IntPtr ppResults, uint numResults)
		{
			long key;
			unchecked { key = (long)userData; }

			Engine engine = Engine.smDictionary[key];
			return engine.NativeProgressCB(ppResults, numResults);
		}
	}
}
