#ifndef CORE_JOB_JOBSYSTEM_H_
#define CORE_JOB_JOBSYSTEM_H_

#include "utils/utils_stdlibwrapper.h"
#include "utils/utils_basictypes.h"
#include "core_job_Job.h"
#include "core_job_WorkerThread.h"
#include "core_job_JobQueue.h"

namespace core::job
{
	class JobSystem
	{
	public:
		JobSystem(u32 numWorkers);
		~JobSystem();

		u32 GetWorkerCount() const;

		Job* PopJob();
		void PushJob(Job* pJob);

		void CancelAllJobs();

	private:

		JobSystem(const JobSystem&) = delete;
		JobSystem& operator=(const JobSystem&) = delete;

		JobQueue mQueue;
		std::mutex mQueueMutex;
		std::condition_variable mQueueCV;

		WorkerThreadArrayPtr mpWorkers;
		u32 mNumWorkers;
		u8 padding[4];
	};

	class JobSysCancelToken
	{
	public:
		JobSysCancelToken(JobSystem& jobSys) : mJobSys(jobSys) {}
		void operator()() { mJobSys.CancelAllJobs(); }
	private:
		JobSysCancelToken(const JobSysCancelToken&) = delete;
		JobSysCancelToken& operator=(const JobSysCancelToken&) = delete;
		JobSysCancelToken& operator=(JobSysCancelToken&&) = delete;
		JobSystem& mJobSys;
	};
}

#endif // CORE_JOB_JOBSYSTEM_H_