﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram
{
	public class RGBHistogramModel : BaseColorHistogramModel
	{
		private byte mNumBucketsR = 8;
		private byte mNumBucketsG = 12;
		private byte mNumBucketsB = 3;

		public RGBHistogramModel() { }
		public RGBHistogramModel(ref CBIRNative.RGBHistogramParameters nativeParams)
		{
			mNumBucketsR = nativeParams.numBucketsR;
			mNumBucketsG = nativeParams.numBucketsG;
			mNumBucketsB = nativeParams.numBucketsB;
		}

		public byte NumBucketsR
		{
			get { return mNumBucketsR; }
			set
			{
				mNumBucketsR = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsG
		{
			get { return mNumBucketsG; }
			set
			{
				mNumBucketsG = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsB
		{
			get { return mNumBucketsB; }
			set
			{
				mNumBucketsB = value;
				OnPropertyChanged();
			}
		}

		public override CBIRNative.eHistogramColorSpace ColorSpace
		{
			get { return CBIRNative.eHistogramColorSpace.kHistogramColorSpace_RGB; }
		}
	}
}
