#ifndef IMAGE_DISTANCE_COMPARATOR_H_
#define IMAGE_DISTANCE_COMPARATOR_H_

#include "image_distance_ComparatorParams.h"
#include "image/feature/image_feature_Generator.h"
#include "utils/utils_stdlibwrapper.h"

namespace image::distance
{
	class Comparator
	{
	public:
		
		Comparator(const ComparatorParams& params) : mParams(params), mGenerator(params.GetGenParams()) { }
		
		Comparator(Comparator&& other) : mParams(std::move(other.mParams)), mGenerator(std::move(other.mGenerator)) { }
		Comparator& operator=(Comparator&& other)
		{
			mParams = std::move(other.mParams);
			mGenerator = std::move(other.mGenerator);
			return *this;
		}


		const ComparatorParams& GetParams() const
		{
			return mParams;
		}

		dist_elem Compare(RegionResult* pRegionResults);

	private:

		Comparator(const Comparator& other) = delete;
		Comparator& operator=(const Comparator&) = delete;

		ComparatorParams mParams;
		image::feature::Generator mGenerator;
	};

	typedef std::vector<Comparator> ComparatorVector;
}

#endif // IMAGE_DISTANCE_COMPARATOR_H_