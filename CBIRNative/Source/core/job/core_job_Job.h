#ifndef CORE_JOB_JOB_H_
#define CORE_JOB_JOB_H_

#include "utils/utils_stdlibwrapper.h"
#include "container/container_SLinkNode.h"

namespace core::job
{
	class Job : public container::SLinkNode<Job>
	{
	public:
		Job();
		virtual ~Job();
		virtual void Execute();

	protected:
		virtual void ExecuteInternal() = 0;

	};

	typedef std::unique_ptr<Job> JobPtr;
}

#endif // CORE_JOB_JOB_H_