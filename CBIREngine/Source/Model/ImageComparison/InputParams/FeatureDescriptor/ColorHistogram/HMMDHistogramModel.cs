﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram
{
	public class HMMDHistogramModel : BaseColorHistogramModel
	{
		private byte mNumBucketsH   = 8;
		private byte mNumBucketsMin = 4;
		private byte mNumBucketsMax = 4;
		private byte mNumBucketsD   = 4;

		public HMMDHistogramModel() { }
		public HMMDHistogramModel(ref CBIRNative.HMMDHistogramParameters nativeParams)
		{
			mNumBucketsH   = nativeParams.numBucketsH;
			mNumBucketsMin = nativeParams.numBucketsMin;
			mNumBucketsMax = nativeParams.numBucketsMax;
			mNumBucketsD   = nativeParams.numBucketsD;
		}

		public byte NumBucketsH
		{
			get { return mNumBucketsH; }
			set
			{
				mNumBucketsH = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsMin
		{
			get { return mNumBucketsMin; }
			set
			{
				mNumBucketsMin = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsMax
		{
			get { return mNumBucketsMax; }
			set
			{
				mNumBucketsMax = value;
				OnPropertyChanged();
			}
		}

		public byte NumBucketsD
		{
			get { return mNumBucketsD; }
			set
			{
				mNumBucketsD = value;
				OnPropertyChanged();
			}
		}

		public override CBIRNative.eHistogramColorSpace ColorSpace
		{
			get { return CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HMMD; }
		}
	}
}
