#ifndef IMAGE_DESCRIPTOR_COLORHISTOGRAMAPPLIER_H_
#define IMAGE_DESCRIPTOR_COLORHISTOGRAMAPPLIER_H_

#include "image_feature_descriptor_applier_BaseApplier.h"
#include "image/image_Image.h"
#include "image/feature/image_feature_Feature.h"

namespace image::feature::descriptor::applier
{
	template <class LocalityClass, class ColorSpace>
	class ColorHistogramApplier : public BaseApplier
	{
	public:
		ColorHistogramApplier(typename const ColorSpace::InputOptions& params);

		virtual void Apply(const image::Image& inputImage, feature::Feature& outputFeature) const override;
		virtual u32 GetRegionCount() const override;

	private:

		ColorHistogramApplier(const ColorHistogramApplier& other) = delete;
		ColorHistogramApplier& operator=(const ColorHistogramApplier&) = delete;

		typename const ColorSpace::InputOptions& mParams;
		u32 muiNumBucketsPerHistogram;
		u8 padding[4];
	};
}

#endif // IMAGE_DESCRIPTOR_COLORHISTOGRAMAPPLIER_H_