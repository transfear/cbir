﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.InputParams.DistanceDescriptor
{
	public class ChiSquaredDistanceModel : BaseDistanceDescriptorModel
	{
		public override CBIRNative.eDistanceMethod DistanceMethod
		{
			get { return CBIRNative.eDistanceMethod.kDistanceMethod_ChiSquared; }
		}
	}
}
