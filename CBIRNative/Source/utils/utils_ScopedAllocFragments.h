#pragma once

#include "utils_stdlibwrapper.h"

namespace utils
{
	template <u32 TFragCount>
	class scoped_alloc_fragments
	{
		public:
			template <typename... Args>
			scoped_alloc_fragments(Args&&... args)
			{
				static_assert(TFragCount == sizeof...(args));

				// Make one alloc for all suballocs
				const auto uiTotalAllocsSize = sum(std::forward<Args>(args)...);
				mpAlloc = std::make_unique<u8[]>(uiTotalAllocsSize);

				// Setup pointers
				setup(0, 0, std::forward<Args>(args)...);
			}

			template <u32 Tindex> u8* get() const
			{
				static_assert(Tindex < TFragCount);
				return mSubAllocs[Tindex];
			}

		private:

			scoped_alloc_fragments(const scoped_alloc_fragments&) = delete;

			template<typename T>
			static constexpr T sum(T&& first)
			{
				return first;
			}

			template<typename T, typename... Args>
			static constexpr T sum(T&& first, Args&&... args)
			{
				return first + sum(std::forward<Args>(args)...);
			}

			template<typename T, typename... Args>
			void setup(u32 index, std::size_t offset, [[maybe_unused]] T&& first)
			{
				mSubAllocs[index] = mpAlloc.get() + offset;
			}

			template<typename T, typename... Args>
			void setup(u32 index, std::size_t offset, T&& first, Args&&... args)
			{
				mSubAllocs[index] = mpAlloc.get() + offset;
				setup(index + 1, offset + first, std::forward<Args>(args)...);
			}

			std::unique_ptr<u8[]> mpAlloc = nullptr;
			std::array<u8*, TFragCount> mSubAllocs;
	};

	template <typename... Args>scoped_alloc_fragments(Args&&... args) -> scoped_alloc_fragments<sizeof...(Args)>;
}