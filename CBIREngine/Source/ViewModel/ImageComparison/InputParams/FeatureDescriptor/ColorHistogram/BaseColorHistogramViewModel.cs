﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using CMD = CBIREngine.Source.Command.ImageComparison.InputParams.FeatureDescriptor;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram
{
	public abstract class BaseColorHistogramViewModel : BaseFeatureDescriptorViewModel
	{
		private readonly MDL.BaseColorHistogramModel mModel;
		private readonly CMD.ChangeFeatureDescriptorCommand mChangeDescriptCommand;    // Owned by parent DescriptorParamsViewModel

		public BaseColorHistogramViewModel(MDL.BaseColorHistogramModel model, CMD.ChangeFeatureDescriptorCommand changeDescriptCmd) : base(model, changeDescriptCmd)
		{
			mChangeDescriptCommand = changeDescriptCmd;

			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
		}

		#region DataBindings
		public CBIRNative.eHistogramColorSpace ColorSpace
		{
			get { return mModel.ColorSpace; }
			set
			{
				if (value != mModel.ColorSpace)
					mChangeDescriptCommand.Execute(new List<object>() { CBIRNative.eDescriptionMethod.kDescriptionMethod_ColorHistogram, value });
			}
		}
		#endregion

		#region HelperFunctions
		public static BaseColorHistogramViewModel CreateFromModel(MDL.BaseColorHistogramModel model, CMD.ChangeFeatureDescriptorCommand changeDescriptCommand)
		{
			switch (model.ColorSpace)
			{
				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HSV:
					{
						return new HSVHistogramViewModel(model as MDL.HSVHistogramModel, changeDescriptCommand);
					}

				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_RGB:
					{
						return new RGBHistogramViewModel(model as MDL.RGBHistogramModel, changeDescriptCommand);
					}

				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HMMD:
					{
						return new HMMDHistogramViewModel(model as MDL.HMMDHistogramModel, changeDescriptCommand);
					}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported color space: {0}", model.ColorSpace.ToString()));
			return null;
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
		}
		#endregion
	}
}
