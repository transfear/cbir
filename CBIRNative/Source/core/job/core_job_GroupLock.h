#ifndef CORE_JOB_GROUPLOCK_H_
#define CORE_JOB_GROUPLOCK_H_

#include "utils/utils_stdlibwrapper.h"

namespace core::job
{
	class GroupLock
	{
	public:
		GroupLock() : mCount(0) { }

		void WaitForAllJobs()
		{
			std::unique_lock<std::mutex> lock(mMtx);
			while (mCount != 0)
				mCv.wait(lock);
		}

		void JobCreated()
		{
			++mCount;
		}

		void JobExecuted()
		{
			int newCount;
			{
				std::lock_guard<std::mutex> lock(mMtx);
				newCount = --mCount;
			}

			assert(newCount >= 0);
			if (newCount == 0)
				mCv.notify_all();
		}

	private:
		GroupLock(const GroupLock&)            = delete;
		GroupLock(GroupLock&&)                 = delete;
		GroupLock& operator=(const GroupLock&) = delete;
		GroupLock& operator=(GroupLock&&)      = delete;

		std::mutex              mMtx;
		std::condition_variable mCv;
		std::atomic<int>        mCount;
	};
}

#endif // CORE_JOB_GROUPLOCK_H_