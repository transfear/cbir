﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

using API;
using VM = CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram;

namespace CBIREngine.Source.View.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram
{
	class ColorHistogramDataTemplateSelector : DataTemplateSelector
	{
		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			FrameworkElement fe = container as FrameworkElement;
			VM.BaseColorHistogramViewModel vm = item as VM.BaseColorHistogramViewModel;

			if (vm != null)
			{
				switch (vm.ColorSpace)
				{
					case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HSV:
						{
							return fe.FindResource("HSVHistogramDataTemplate") as DataTemplate;
						}

					case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_RGB:
						{
							return fe.FindResource("RGBHistogramDataTemplate") as DataTemplate;
						}

					case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HMMD:
						{
							return fe.FindResource("HMMDHistogramDataTemplate") as DataTemplate;
						}
				}
			}

			return base.SelectTemplate(item, container);
		}
	}
}
