#include "image_distance_applier_ChiSquared.h"

namespace image::distance::applier
{
	dist_elem ChiSquared::Apply(u32 uiRegionIdx, const image::feature::Feature& first, const image::feature::Feature& second) const
	{
		const u32 uiFeatureCount = first.GetFeatureCount();
		const u32 uiRegionCount = first.GetRegionCount();
		const u32 uiElemCount = uiFeatureCount / uiRegionCount;
		const u32 uiElemStart = uiRegionCount * uiRegionIdx;

		const feat_elem* pA = first.GetData()  + uiElemStart;
		const feat_elem* pB = second.GetData() + uiElemStart;

		dist_elem dSum = 0.0;
		for (u32 i = 0; i < uiElemCount; ++i)
		{
			const dist_elem a = pA[i];
			const dist_elem b = pB[i];

			const dist_elem dDenom = a + b;
			if (dDenom > 0.0)
			{
				const dist_elem dDiff    = a - b;
				const dist_elem dDiffSqr = dDiff * dDiff;

				const dist_elem  dFrac = dDiffSqr / dDenom;
				dSum += dFrac;
			}
		}

		return dSum;
	}

}

