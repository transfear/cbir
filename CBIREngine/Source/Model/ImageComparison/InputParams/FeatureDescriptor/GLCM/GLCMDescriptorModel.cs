﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.GLCM
{
	public class GLCMDescriptorModel : BaseFeatureDescriptorModel
	{
		public override CBIRNative.eDescriptionMethod DescriptionMethod
		{
			get { return CBIRNative.eDescriptionMethod.kDescriptionMethod_GLCM; }
		}
	}
}
