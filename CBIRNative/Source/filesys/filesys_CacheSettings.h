#ifndef FILESYS_CACHESETTINGS_H_
#define FILESYS_CACHESETTINGS_H_

#include "utils/utils_stdlibwrapper.h"
#include "utils/utils_String.h"

namespace filesys
{
	class CacheSettings
	{
	public:
		CacheSettings(utils::StringPtr&& cachePath, const char* rootSearchPath, bool bCompressData) :
			mRootSearchPath(rootSearchPath), mCachePath(std::move(cachePath)), mbCompressData(bCompressData) { }

		CacheSettings(CacheSettings&& other) :
			mRootSearchPath(other.mRootSearchPath), mCachePath(std::move(other.mCachePath)), mbCompressData(other.mbCompressData) { }

		CacheSettings& operator=(CacheSettings&& other)
		{
			mRootSearchPath = other.mRootSearchPath;
			mCachePath      = std::move(other.mCachePath);
			mbCompressData  = other.mbCompressData;

			return *this;
		}

		const char* GetCachePath() const
		{
			return mCachePath->GetRaw();
		}

		const char* GetRootSearchPath() const
		{
			return mRootSearchPath;
		}

		bool GetCompressData() const
		{
			return mbCompressData;
		}

	private:

		CacheSettings(const CacheSettings&) = delete;
		CacheSettings& operator=(const CacheSettings&) = delete;

		const char* mRootSearchPath;
		utils::StringPtr mCachePath;
		bool mbCompressData;

		char padding[7];
	};

	typedef std::vector<CacheSettings> CacheSettingsVector;
}

#endif // FILESYS_CACHESETTINGS_H_