#ifndef CBIRNATIVE_H
#define CBIRNATIVE_H

#pragma warning(push)
#pragma warning(4: 4820)	// For strict data alignment

// Public API goes here
extern "C"
{
	enum eDescriptionMethod : unsigned char
	{
		kDescriptionMethod_ColorHistogram,
		kDescriptionMethod_GLCM
	};

	enum eHistogramColorSpace : unsigned char
	{
		kHistogramColorSpace_RGB,			// Red, Green, Blue
		kHistogramColorSpace_HSV,			// Hue, Saturation, Value
		kHistogramColorSpace_HMMD			// Hue, Min, Max, Distance
	};

	enum eLocalityDescription : unsigned char
	{
		kLocalityDescription_Global,			// 1 region
		kLocalityDescription_CenterAndCorners,	// 5 regions
		kLocalityDescription_3Horizontal,		// 3 regions
		kLocalityDescription_3x3				// 9 regions
	};

	enum eDistanceMethod : unsigned char
	{
		kDistanceMethod_Euclidean,
		kDistanceMethod_ChiSquared,
		kDistanceMethod_EarthMover
	};

	struct HSVHistogramParameters
	{
		unsigned char numBucketsH;		// Number of "Hue" buckets
		unsigned char numBucketsS;		// Number of "Saturation" buckets
		unsigned char numBucketsV;		// Number of "Value" buckets
		char padding[5];
	};

	struct RGBHistogramParameters
	{
		unsigned char numBucketsR;
		unsigned char numBucketsG;
		unsigned char numBucketsB;
		char padding[5];
	};

	struct HMMDHistogramParameters
	{
		unsigned char numBucketsH;
		unsigned char numBucketsMin;
		unsigned char numBucketsMax;
		unsigned char numBucketsD;
		char padding[4];
	};

	struct ColorHistogramParameters
	{
		eHistogramColorSpace colorSpace;
		char padding[7];

		union
		{
			HSVHistogramParameters  hsvParams;
			RGBHistogramParameters  rgbParams;
			HMMDHistogramParameters hmmdParams;
		};
	};

	struct FeatureDescriptor
	{
		eDescriptionMethod   descriptionMethod;
		eLocalityDescription localityDescription;
		char padding[6];

		const float* pRegionWeights;		// Weights. Array size expected to be equal to number of regions in localityDescription. Sum assumed to be > 0. Each weight assumed to be >= 0.

		union
		{
			ColorHistogramParameters colorHist;
		};
	};

	struct DistanceDescriptor
	{
		eDistanceMethod distanceMethod;
		char padding[7];
		
		union
		{
			// data
			int dummy;
		};
	};

	struct DescriptorParameters
	{
		FeatureDescriptor  feature;
		DistanceDescriptor distance;
		float              weight;		// weight of the descriptor
	};

	struct RegionResult
	{
		double distance;

		// TODO: Histogram and other data
	};

	struct DescriptorResult
	{
		DescriptorParameters params;

		unsigned int         numRegions;
		char                 padding[4];

		const RegionResult*  pRegionResults;	// 1 result per region
		double               distance;			// weighted descriptor result
	};

	struct CompareResult
	{
		const char*             refPath;			// Path of the reference image
		const char*             imgPath;			// Path of the compared image
		const DescriptorResult* pDescriptorResults;	// 1 result per descriptor
		double                  distance;			// weighted result of all descriptors, and all their regions
		unsigned int            numDescriptors;
		char                    padding[4];
	};

	// Callback: return true to continue recieving results, false to abort
	typedef bool(*ProgressCallback)(unsigned long long userData, CompareResult const* const* results, unsigned int numResults);

	struct CompareOptions
	{
		const DescriptorParameters* descriptParams;		// Image descriptor parameters

		ProgressCallback cb;			// Callback to notify user of the progress

		const char* srcImagePath;		// Path to the source image to be compared
		const char* rootImageFolder;	// Path to root folder containing images to analyze
		const char* cacheFolder;		// Path to where the cached results will be stored

		unsigned long long userData;	// Userdata sent down the progression callback

		unsigned int numDescriptors;	// Number of descriptors in descriptParams;
		bool         checkSubFolders;	// Whether or not to analyze in subfolders
		bool         compressCache;		// Whether or not to compress (deflate) cached data
		char padding[2];
	};


	__declspec(dllexport) int CompareImage(const CompareOptions* opt, int numWorkers);	// Returns 0 if successful, < 0 if there are errors
};

#pragma warning(pop)

#endif	// CBIRNATIVE_H