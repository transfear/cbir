#ifndef EMD_HAT_H_
#define EMD_HAT_H_

#pragma warning(push, 3)
#pragma warning(disable: 4244)
#pragma warning(disable: 4267)
#pragma warning(disable: 4623)

namespace EmdHat
{
	#include "emd_hat.hpp"
}

#pragma warning(pop)

#endif // EMD_HAT_H_
