#ifndef MATH_HISTO_MULTIDIMHISTOGRAM_H_
#define MATH_HISTO_MULTIDIMHISTOGRAM_H_

#include "utils/utils_basictypes.h"

namespace math::histo
{
	template <u32 numDims, u32 curDim, class H> class MultiDimHistogramT
	{
	typedef MultiDimHistogramT<numDims - 1, curDim + 1, H> InferiorDimHisto;

	public:
		MultiDimHistogramT() { }

		void Init(const u32* dimSizes, H* pBuffer)
		{
			// Calculate stride
			u32 uiBufStride = 1;
			const u32 dimIdxStop = curDim + numDims;
			for (u32 dimIdx = curDim + 1; dimIdx < dimIdxStop; ++dimIdx)
				uiBufStride *= dimSizes[dimIdx];

			const u32 bucketsForDim = dimSizes[curDim];
			mHistograms = std::make_unique<InferiorDimHisto[]>(bucketsForDim);
			muiBucketsForDim = bucketsForDim;

			H* pInferiorBuffer = pBuffer;
			InferiorDimHisto* pInferiorHisto = mHistograms.get();
			for (u32 curBucket = 0; curBucket < bucketsForDim; ++curBucket, ++pInferiorHisto, pInferiorBuffer += uiBufStride)
			{
				pInferiorHisto->Init(dimSizes, pInferiorBuffer);
			}
		}

		void GetDimSizes(u32* pOutDims)
		{
			pOutDims[curDim] = muiBucketsForDim;
			mHistograms[0].GetDimSizes(pOutDims);
		}

		void AddEntry(const f32 v[numDims])
		{
			const f32 fCurDimValue = v[curDim];
					
			u32 uiBucketIdx = static_cast<u32>(fCurDimValue * static_cast<f32>(muiBucketsForDim));
			uiBucketIdx = std::min(uiBucketIdx, muiBucketsForDim - 1);
				
			InferiorDimHisto& inferiorHisto = mHistograms.get()[uiBucketIdx];
			inferiorHisto.AddEntry(v);
		}
			
	private:
		MultiDimHistogramT(const MultiDimHistogramT&) = delete;
		MultiDimHistogramT& operator=(const MultiDimHistogramT&) = delete;

		std::unique_ptr<InferiorDimHisto[]> mHistograms;
		u32 muiBucketsForDim;
		u8  mPadding[4];
	};

	template <u32 curDim, class H> class MultiDimHistogramT<1, curDim, H>
	{
	public:
		MultiDimHistogramT() { }

		void Init(const u32* dimSizes, H* pBuffer)
		{
			muiBucketsForDim = dimSizes[curDim];
			mHistogramData   = pBuffer;
		}

		void GetDimSizes(u32* pOutDims)
		{
			pOutDims[curDim] = muiBucketsForDim;
		}

		void AddEntry(const f32 v[1])
		{
			const f32 fCurDimValue = v[curDim];

			u32 uiBucketIdx = static_cast<u32>(fCurDimValue * static_cast<f32>(muiBucketsForDim));
			uiBucketIdx = std::min(uiBucketIdx, muiBucketsForDim - 1);

			++mHistogramData[uiBucketIdx];
		}

	private:
		MultiDimHistogramT(const MultiDimHistogramT&) = delete;
		MultiDimHistogramT& operator=(const MultiDimHistogramT&) = delete;

		H*  mHistogramData;
		u32 muiBucketsForDim;
		u8  mPadding[4];

	};
}

#endif // MATH_HISTO_MULTIDIMHISTOGRAM_H_