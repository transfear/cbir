#ifndef CONCURRENT_SCOPEDLOCK_H_
#define CONCURRENT_SCOPEDLOCK_H_

namespace concurrent
{
	template <class T>
	class ScopedLock
	{
	public:
		ScopedLock(T& toLock) : mLock(toLock)
		{
			mLock.Acquire();
		}

		~ScopedLock()
		{
			mLock.Release();
		}

	private:
		ScopedLock(const ScopedLock<T>&) = delete;
		ScopedLock<T>& operator=(const ScopedLock<T>&) = delete;

		T& mLock;
	};
}

#endif // CONCURRENT_SCOPEDLOCK_H_