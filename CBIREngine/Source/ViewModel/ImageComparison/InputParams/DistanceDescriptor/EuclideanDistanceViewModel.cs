﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CMD = CBIREngine.Source.Command.ImageComparison.InputParams.DistanceDescriptor;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams.DistanceDescriptor;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.DistanceDescriptor
{
	public class EuclideanDistanceViewModel : BaseDistanceDescriptorViewModel
	{
		public EuclideanDistanceViewModel(MDL.EuclideanDistanceModel model, CMD.ChangeDistanceDescriptorCommand changeDescriptCmd) : base(model, changeDescriptCmd)
		{
			model.PropertyChanged += Model_PropertyChanged;
		}

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
		}
		#endregion
	}
}
