#ifndef IMAGE_JPEGDECOMPRESSOR_H_
#define IMAGE_JPEGDECOMPRESSOR_H_

#include "utils/utils_stdlibwrapper.h"
#include "utils/utils_basictypes.h"
#include "image_Image.h"

namespace image
{
	class JpegDecompressor
	{
	public:
		JpegDecompressor() { }
		JpegDecompressor(JpegDecompressor&& other) : mImage(std::move(other.mImage)) { }
		JpegDecompressor& operator=(JpegDecompressor&& other)
		{
			mImage = std::move(other.mImage);
			return *this;
		}


		void Decompress(FILE* fp);

		const Image& GetDecompressedImage() const;

	private:

		JpegDecompressor(const JpegDecompressor&) = delete;
		JpegDecompressor& operator=(const JpegDecompressor&) = delete;

		Image mImage;
	};
}

#endif // IMAGE_JPEGDECOMPRESSOR_H_