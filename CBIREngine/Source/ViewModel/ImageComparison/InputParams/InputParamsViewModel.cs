﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CMD = CBIREngine.Source.Command;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams;
using CBIREngine.Source.Utils;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams
{
	public class InputParamsViewModel : BaseViewModel
	{
		private readonly MDL.InputParamsModel mModel;

		private readonly ObservableRangeCollection<DescriptorParamsViewModel> mDescriptors = new ObservableRangeCollection<DescriptorParamsViewModel>();

		private readonly CMD.BrowseFileCommand   mBrowseSourceImageCommand;
		private readonly CMD.BrowseFolderCommand mBrowseImageLibCommand;
		private readonly CMD.BrowseFolderCommand mBrowseCacheFolderCommand;
		private readonly CMD.ImageComparison.InputParams.NewDescriptorParamsCommand mNewDescriptorParamsCommand;
		private readonly CMD.ImageComparison.InputParams.RemoveDescriptorParamsCommand mRemoveDescriptorParamsCommand;

		public InputParamsViewModel(MDL.InputParamsModel model)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
			mModel.Descriptors.CollectionChanged += Model_DescriptorListChanged;
			
			mBrowseSourceImageCommand = new CMD.BrowseFileCommand(
				(string filePath) => { this.SourceImage = filePath; },
				() => { return Path.GetDirectoryName(this.SourceImage); },
				"Select source image",
				new List<string>(){ "jpg", "jpeg" }	// TODO: Fetch supported extensions from the native dll
			);

			mBrowseImageLibCommand = new CMD.BrowseFolderCommand(
				(string folderPath) => { this.ImageLib = folderPath; },
				() => { return this.ImageLib; },
				"Select the folder containing the images to compare."
			);

			mBrowseCacheFolderCommand = new CMD.BrowseFolderCommand(
				(string folderPath) => { this.CacheFolder = folderPath; },
				() => { return this.CacheFolder; },
				"Select the folder where to cache the intermediate results of the image comparison."
			);

			mNewDescriptorParamsCommand    = new Command.ImageComparison.InputParams.NewDescriptorParamsCommand(this);
			mRemoveDescriptorParamsCommand = new Command.ImageComparison.InputParams.RemoveDescriptorParamsCommand(this);

			// Initialize internal collection
			foreach (MDL.DescriptorParamsModel descript in mModel.Descriptors)
				mDescriptors.Add(new DescriptorParamsViewModel(descript, mRemoveDescriptorParamsCommand));
		}

		public MDL.InputParamsModel Model
		{
			get { return mModel; }
		}

		#region DataBindings
		public String SourceImage
		{
			get { return mModel.SourceImage; }
			set
			{
				mModel.SourceImage = value;
				OnPropertyChanged();
			}
		}
		
		public String ImageLib
		{
			get { return mModel.ImageLib; }
			set
			{
				mModel.ImageLib = value;
				OnPropertyChanged();
			}
		}

		public String CacheFolder
		{
			get { return mModel.CacheFolder; }
			set
			{
				mModel.CacheFolder = value;
				OnPropertyChanged();
			}
		}

		public int NumThreads
		{
			get { return mModel.NumThreads; }
			set
			{
				mModel.NumThreads = value;
				OnPropertyChanged();
			}
		}

		public bool CheckSubfolders
		{
			get { return mModel.CheckSubfolders; }
			set
			{
				mModel.CheckSubfolders = value;
				OnPropertyChanged();
			}
		}

		public bool CompressCache
		{
			get { return mModel.CompressCache; }
			set
			{
				mModel.CompressCache = value;
				OnPropertyChanged();
			}
		}

		public ObservableRangeCollection<DescriptorParamsViewModel> Descriptors
		{
			get { return mDescriptors; }
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "SourceImage":
				case "ImageLib":
				case "CacheFolder":
				case "NumThreads":
				case "CheckSubfolders":
				case "CompressCache":
				{
					OnPropertyChanged(e.PropertyName);
					break;
				}
			}
		}

		private void Model_DescriptorListChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			switch (e.Action)
			{
				case NotifyCollectionChangedAction.Add:
					{
						foreach (MDL.DescriptorParamsModel newEntry in e.NewItems)
							mDescriptors.Add(new DescriptorParamsViewModel(newEntry, mRemoveDescriptorParamsCommand));
						break;
					}

				case NotifyCollectionChangedAction.Remove:
					{
						foreach (MDL.DescriptorParamsModel oldEntry in e.OldItems)
						{
							DescriptorParamsViewModel vm = mDescriptors.First(x => x.Model == oldEntry);
							mDescriptors.Remove(vm);
						}
						break;
					}

				case NotifyCollectionChangedAction.Reset:
					{
						mDescriptors.Clear();
						ObservableRangeCollection<MDL.DescriptorParamsModel> srcList = (ObservableRangeCollection<MDL.DescriptorParamsModel>)sender;
						foreach (MDL.DescriptorParamsModel newEntry in srcList)
						{
							DescriptorParamsViewModel vm = new DescriptorParamsViewModel(newEntry, mRemoveDescriptorParamsCommand);
							mDescriptors.Add(vm);
						}
						break;
					}
			}
		}
		#endregion

		#region Commands
		public CMD.BrowseFileCommand BrowseSourceImageCommand
		{
			get { return mBrowseSourceImageCommand; }
		}
		public CMD.BrowseFolderCommand BrowseImageLibCommand
		{
			get { return mBrowseImageLibCommand; }
		}
		public CMD.BrowseFolderCommand BrowseCacheFolderCommand
		{
			get { return mBrowseCacheFolderCommand; }
		}
		public CMD.ImageComparison.InputParams.NewDescriptorParamsCommand NewDescriptorParamsCommand
		{
			get { return mNewDescriptorParamsCommand; }
		}
		#endregion
	}
}
