﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using CMD = CBIREngine.Source.Command.ImageComparison.InputParams;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams
{
    public class DescriptorParamsViewModel : BaseViewModel
    {
		private readonly MDL.DescriptorParamsModel mModel;
		private readonly CMD.RemoveDescriptorParamsCommand mRemoveDescriptorParamsCommand;  // Owned by parent InputParamsViewModel
		private readonly CMD.DistanceDescriptor.ChangeDistanceDescriptorCommand mChangeDistDescriptCommand;
		private readonly CMD.FeatureDescriptor.ChangeFeatureDescriptorCommand mChangeFeatDescriptCommand;

		private DistanceDescriptor.BaseDistanceDescriptorViewModel mDistanceDescriptorVM = null;
		private FeatureDescriptor.BaseFeatureDescriptorViewModel   mFeatureDescriptorVM  = null;

		public DescriptorParamsViewModel(MDL.DescriptorParamsModel model, CMD.RemoveDescriptorParamsCommand deleteCmd)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;

			mModel.FeatureDescriptor.PropertyChanged += FeatureDescriptor_PropertyChanged;

			mRemoveDescriptorParamsCommand = deleteCmd;
			mChangeDistDescriptCommand = new CMD.DistanceDescriptor.ChangeDistanceDescriptorCommand(this);
			mChangeFeatDescriptCommand = new CMD.FeatureDescriptor.ChangeFeatureDescriptorCommand(this);

			RecreateDistanceDescriptor();
			RecreateFeatureDescriptor();
		}

		#region DataBindings
		public CMD.RemoveDescriptorParamsCommand DeleteCmd
		{
			get { return mRemoveDescriptorParamsCommand; }
		}

		public MDL.DescriptorParamsModel Model
		{
			get { return mModel; }
		}

		public DistanceDescriptor.BaseDistanceDescriptorViewModel Distance
		{
			get { return mDistanceDescriptorVM; }
			set
			{
				mDistanceDescriptorVM = value;
				OnPropertyChanged();
				OnPropertyChanged("DescriptorName");
			}
		}

		public FeatureDescriptor.BaseFeatureDescriptorViewModel Feature
		{
			get { return mFeatureDescriptorVM; }
			set
			{
				mFeatureDescriptorVM = value;
				OnPropertyChanged();
				OnPropertyChanged("DescriptorName");
			}
		}

		public float Weight
		{
			get { return mModel.Weight; }
			set
			{
				mModel.Weight = value;
				OnPropertyChanged();
			}
		}

		public string DescriptorName
		{
			get
			{
				string strDistance = DistanceDescriptor.DistanceEnumMap.EnumList.First(x => x.EnumValue == mDistanceDescriptorVM.Model.DistanceMethod).StringValue;
				string strMethod   = FeatureDescriptor.DescriptionMethodEnumMap.EnumList.First(x => x.EnumValue == mFeatureDescriptorVM.Model.DescriptionMethod).StringValue;
				string strLocality = FeatureDescriptor.Locality.LocalityEnumMap.EnumList.First(x => x.EnumValue == mFeatureDescriptorVM.Model.Locality.LocalityDescription).StringValue;

				return String.Format("{0}/{1}/{2}", strDistance, strMethod, strLocality);
			}
		}
		#endregion

		#region HelperFunctions
		private void RecreateFeatureDescriptor()
		{
			Feature = FeatureDescriptor.BaseFeatureDescriptorViewModel.CreateFromModel(mModel.FeatureDescriptor, mChangeFeatDescriptCommand);
		}

		private void RecreateDistanceDescriptor()
		{
			Distance = DistanceDescriptor.BaseDistanceDescriptorViewModel.CreateFromModel(mModel.DistanceDescriptor, mChangeDistDescriptCommand);
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Weight":
					{
						OnPropertyChanged("Weight");
						break;
					}
				case "FeatureDescriptor":
					{
						RecreateFeatureDescriptor();
						break;
					}
				case "DistanceDescriptor":
					{
						RecreateDistanceDescriptor();
						break;
					}

			}
		}

		private void FeatureDescriptor_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Locality":
					{
						OnPropertyChanged("DescriptorName");
						break;
					}
			}
		}
		#endregion

	}
}
