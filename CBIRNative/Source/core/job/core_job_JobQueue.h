#ifndef CORE_JOB_JOBQUEUE_H_
#define CORE_JOB_JOBQUEUE_H_

#include "utils/utils_basictypes.h"
#include "core_job_Job.h"
#include "concurrent/concurrent_SpinLock.h"

namespace core::job
{
	class JobQueue
	{
	public:
		JobQueue();
		~JobQueue();

		void Enqueue(Job* pJob);
		Job* Dequeue();

	private:
		JobQueue(const JobQueue&) = delete;
		JobQueue& operator=(const JobQueue&) = delete;

		Job* mpFirst = nullptr;
		Job* mpLast = nullptr;
	};
}

#endif // CORE_JOB_JOBQUEUE_H_