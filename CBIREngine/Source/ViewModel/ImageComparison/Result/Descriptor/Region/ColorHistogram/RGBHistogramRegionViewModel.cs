﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MDL = CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Region.ColorHistogram;

namespace CBIREngine.Source.ViewModel.ImageComparison.Result.Descriptor.Region.ColorHistogram
{
	public class RGBHistogramRegionViewModel : BaseColorHistogramRegionViewModel
	{
		private readonly MDL.RGBHistogramRegionModel mModel;

		public RGBHistogramRegionViewModel(MDL.RGBHistogramRegionModel model, ResultViewModel parentResult) : base(model, parentResult)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
		}

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				default:
				{
					//TODO
					break;
				}
			}
		}
		#endregion
	}
}
