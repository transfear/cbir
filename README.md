Interesting references:

http://www.pyimagesearch.com/2014/12/01/complete-guide-building-image-search-engine-python-opencv/
https://github.com/aminert/CBIR
http://www.ci.gxnu.edu.cn/cbir/code/MTH%20code.txt

Nanoflann:
https://github.com/jlblancoc/nanoflann

Base encode:
https://en.wikipedia.org/wiki/Base36

k-mean:
http://www.goldsborough.me/c++/python/cuda/2017/09/10/20-32-46-exploring_k-means_in_python,_c++_and_cuda/

C++17/concurrency stuff:
http://www.bfilipek.com/2017/09/c17-in-detail-summary-bonus.html
http://www.modernescpp.com/index.php/parallel-algorithm-of-the-standard-template-library
https://paoloseverini.wordpress.com/2014/04/07/concurrency-in-c11/

emd:
https://github.com/aaalgo/donkey/blob/master/src/donkey-emd.h
https://github.com/aaalgo/donkey/tree/master/3rd/FastEMD
https://github.com/yzbx/DistLearnKit <-- FastEMD-3.1.zip

=============================================================================================================
  TODO
---------

- Done: Setup GitHub
- Done: Basic Project Setup
- Done: Image Pool
- Done: Build libjpeg
- Done: Basic C#/Native wrapper
- Done: Basic init flow
- Done: Threads pool, job system
- Done: Image loading
- Done: HSV Histogram
- Done: Locality functions
- Done: Local cache / Save features to disk
- Done: Save debug data to disk
- Done: Load feature & debug data from disk
- Done: Compression & decompression of the cache
- Done: Euclidean + ChiSquared distance functions
- Done: Support multiple descriptors with weights
- Done: Weights per region
- Done: FastEMD
- Done: C++ -> C# notification (RelayThread)
- Done: image::descriptor cleanup (move to image::feature::descriptor?)
- Done: histogram: colorspace improvements (RGB, HMMD)
- Done: UI -> Basic architecure, MVVM classes, can add tabs
- Done: UI -> Closeable tabs with [X] button
- Done: UI -> Numeric-only textbox
- Done: UI -> File/Folder browser
- Done: UI -> CompareImageParams
- Done: Support userdata from CompareImage to callback function
- Done: Cancellation support (notification from feedback?)
- Done: UI -> CompareImageResults (similar images preview + distances)
- Done: UI -> Doubleclick to open the image in Explorer
- Done: Update to VS2019 + warning cleanup
- Done: UI -> Descriptor and region selection
- Do not copy reference image data for each compared image
- C++ -> pass histogram data to caller
- C++ -> C# histogram data
- UI -> charts (see DataVisualization.Charting)
- C++ -> generate quantized image
- UI -> quantized image
- Profiling data (open files, generating descriptors, comparing distances, adding to ui)
- UI -> Don't show descriptor selector when only 1 descriptor, don't show region selector when only 1 region
- Make C++ paths Unicode-compliant instead of char*
- histogram: Color Structured Descriptor, etc...
- Rubner's EMD


Future:
- GLCM
- K-means
- Textons
- Clustering

=============================================================================================================
Disabled warnings:
4514: unreferenced inline function has been removed
4577: 'noexcept' used with no exception handling mode specified; termination on exception is not guaranteed. Specify /EHsc
4626: 'core::job::JobSystem::PopJob::<lambda_1>': assignment operator was implicitly defined as deleted
4710: 'function name': function not inlined
4711: 'function name': selected for automatic inline expansion
4820: 'DescriptorParameters': '4' bytes padding added after data member 'DescriptorParameters::distance'
5045: Compiler will insert Spectre mitigation for memory load if /Qspectre switch specified