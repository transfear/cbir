#ifndef IMAGE_DISTANCE_DESCRIPTOR_H_
#define IMAGE_DISTANCE_DESCRIPTOR_H_

#include "CBIRNative.h"
#include "image/feature/image_feature_Feature.h"
#include "image/distance/applier/image_distance_applier_BaseApplier.h"

namespace image::distance
{
	class Descriptor
	{
	public:

		Descriptor(const DistanceDescriptor& descriptOpt, const f32* pfRegionWeights, u32 uiNumRegions);
		Descriptor(Descriptor&& other) : mBaseApplier(std::move(other.mBaseApplier)), mpfRegionWeights(other.mpfRegionWeights), muiNumRegions(other.muiNumRegions) { }
		Descriptor& operator=(Descriptor&& other)
		{
			mBaseApplier     = std::move(other.mBaseApplier);
			mpfRegionWeights = other.mpfRegionWeights;
			muiNumRegions    = other.muiNumRegions;
			return *this;
		}

		dist_elem Apply(const image::feature::Feature& first, const image::feature::Feature& second, RegionResult* pRegionResults) const;

	private:

		Descriptor(const Descriptor&) = delete;
		Descriptor& operator=(const Descriptor&) = delete;

		std::unique_ptr<image::distance::applier::BaseApplier> mBaseApplier;
		const f32* mpfRegionWeights;
		u32        muiNumRegions;
		u8         padding[4];
	};

	typedef std::vector<Descriptor> DescriptorVector;
}

#endif // IMAGE_DISTANCE_DESCRIPTOR_H_