﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CBIREngine.Source.View.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram
{
	/// <summary>
	/// Interaction logic for HMMDHistogramView.xaml
	/// </summary>
	public partial class HMMDHistogramView : UserControl
	{
		public HMMDHistogramView()
		{
			InitializeComponent();
		}
	}
}
