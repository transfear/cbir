#ifndef FILESYS_CACHE_H_
#define FILESYS_CACHE_H_

namespace image::feature
{
	class Feature;
}

#include "filesys_CacheSettings.h"
#include "utils/utils_basictypes.h"
#include "utils/utils_stdlibwrapper.h"
#include "utils/utils_String.h"

#include "CBIRNative.h"

namespace filesys
{
	class Cache
	{
	public:
		static ::utils::StringPtr GetCachePath(const char* cacheFolder, const FeatureDescriptor& descriptor, bool bCompressCache);
		
		Cache(const CacheSettings& settings, const char* srcFile)
			: mpSettings(&settings), mSrcFile(std::make_unique<utils::String>(srcFile)) { }

		Cache(const Cache& other)
			: mpSettings(other.mpSettings), mSrcFile(std::make_unique<utils::String>(*other.GetSrcFile())) { }

		Cache(Cache&& other)
			: mpSettings(other.mpSettings), mSrcFile(std::move(other.mSrcFile)), mCachedFilePath(std::move(other.mCachedFilePath)) { }

		Cache& operator=(Cache&& other)
		{
			mpSettings      = other.mpSettings;
			mSrcFile        = std::move(other.mSrcFile);
			mCachedFilePath = std::move(other.mCachedFilePath);
			return *this;
		}

		bool Init();
		void SaveData(const image::feature::Feature& imageFeature) const;
		void LoadData(image::feature::Feature& imageFeature) const;

		const CacheSettings& GetCacheSettings() const
		{
			return *mpSettings;
		}

		const char* GetBaseCachePath() const
		{
			return mpSettings->GetCachePath();
		}

		const char* GetSrcFolder() const
		{
			return mpSettings->GetRootSearchPath();
		}

		const utils::StringPtr& GetSrcFile() const
		{
			return mSrcFile;
		}

		bool GetCompressCache() const
		{
			return mpSettings->GetCompressData();
		}

	private:

		Cache& operator=(const Cache&) = delete;


		const CacheSettings*  mpSettings;
		utils::StringPtr      mSrcFile;
		std::filesystem::path mCachedFilePath;


		static std::filesystem::path GetCachedFilePath(const char* rootSrcFolder, const char* srcFile, const char* cachePath);

		static constexpr const char* const smCacheVersion = "2";
		static constexpr const char* const smCacheSuffix  = ".cache";
	};
}

#endif // FILESYS_CACHE_H_