#ifndef IMAGE_COLORSPACE_HSV_H_
#define IMAGE_COLORSPACE_HSV_H_

#include "CBIRNative.h"
#include "utils/utils_basictypes.h"
#include "utils/utils_stdlibwrapper.h"
#include "image_colorspace_RGB.h"

namespace image::colorspace
{
	class HSV
	{
	public:

		using InputOptions = HSVHistogramParameters;
		enum { NumComponents = 3};

		static void GetDimSize(const InputOptions& inOptions, u32 outDims[NumComponents])
		{
			outDims[0] = inOptions.numBucketsH;
			outDims[1] = inOptions.numBucketsS;
			outDims[2] = inOptions.numBucketsV;
		}

		static u32 GetVolumeSize(const InputOptions& inOptions)
		{
			return static_cast<u32>(inOptions.numBucketsH * inOptions.numBucketsS * inOptions.numBucketsV);
		}

		template <typename SrcColorSpace>
		static void U8ToF32(const u8* inRGB, f32 outHSV[NumComponents]);
	};

	template <>
	static inline void HSV::U8ToF32<RGB>(const u8* inRGB, f32 outHSV[HSV::NumComponents])
	{
		// See https://en.wikipedia.org/wiki/HSL_and_HSV

		const f32 fR = static_cast<f32>(inRGB[0]) / 255.0f;
		const f32 fG = static_cast<f32>(inRGB[1]) / 255.0f;
		const f32 fB = static_cast<f32>(inRGB[2]) / 255.0f;

		const f32 fMin = std::min(fR, std::min(fG, fB));
		const f32 fMax = std::max(fR, std::max(fG, fB));

		if (fMin == fMax)
		{
			// Gray image
			outHSV[0] = 0;
			outHSV[1] = 0;
			outHSV[2] = fMin;
		}
		else
		{
			const f32 fDelta = fMax - fMin;

			f32 fD, fH;
			if (fR == fMin)
			{
				fD = fG - fB;
				fH = 3.0f;
			}
			else if (fB == fMin)
			{
				fD = fR - fG;
				fH = 1.0f;
			}
			else
			{
				fD = fB - fR;
				fH = 5.0;
			}

			const f32 fChroma = (fH - fD / fDelta) / 6.0f;
			const f32 fSaturation = fDelta / fMax;
			const f32 fValue = fMax;

			outHSV[0] = fChroma;
			outHSV[1] = fSaturation;
			outHSV[2] = fValue;
		}
	}
}

#endif // IMAGE_COLORSPACE_HSV_H_