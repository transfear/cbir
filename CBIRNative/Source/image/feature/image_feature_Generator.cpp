#include "image_feature_Generator.h"

namespace image::feature
{
	GeneratorResult Generator::Generate()
	{
		const descriptor::Descriptor& descript = mParams.GetDescriptor();

		feature::Feature imageFeature(descript);

		// Skip if file is already in cache
		filesys::Cache& cache = mParams.GetCache();
		const bool cacheValid = cache.Init();
		if (cacheValid)
		{
			cache.LoadData(imageFeature);
		}
		else
		{
			// Open file
			const char* imagePath = cache.GetSrcFile()->GetRaw();
			FILE* fp = fopen(imagePath, "rb");
			if (fp != nullptr)
			{
				// Decompress jpeg, then close file
				mJpegDecompressor.Decompress(fp);
				fclose(fp);

				// Generate the features from decompressed images
				const Image& decompressedImg = mJpegDecompressor.GetDecompressedImage();
				descript.Apply(decompressedImg, imageFeature);

				// Save feature in cache
				cache.SaveData(imageFeature);
			}
		}

		// TODO: save debug data instead of nullptr
		return std::make_pair(std::move(imageFeature), nullptr);
	}
}

