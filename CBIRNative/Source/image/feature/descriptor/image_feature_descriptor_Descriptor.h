#ifndef IMAGE_FEATURE_DESCRIPTOR_DESCRIPTOR_H_
#define IMAGE_FEATURE_DESCRIPTOR_DESCRIPTOR_H_

#include "applier/image_feature_descriptor_applier_BaseApplier.h"
#include "CBIRNative.h"
#include "utils/utils_stdlibwrapper.h"

namespace image::feature::descriptor
{
	class Descriptor
	{
	public:
		Descriptor(const FeatureDescriptor& descriptOpt);
		Descriptor(Descriptor&& other) : mBaseApplier(std::move(other.mBaseApplier)) { }
		Descriptor& operator=(Descriptor&& other)
		{
			mBaseApplier = std::move(other.mBaseApplier);
			return *this;
		}

		void Apply(const image::Image& inputImage, image::feature::Feature& outputFeature) const;

		u32 GetRegionCount() const
		{
			return mBaseApplier->GetRegionCount();
		}

		u32 GetFeatureCount() const
		{
			return mBaseApplier->GetFeatureCount();
		}

	private:

		template <class L> void InitApplier(const FeatureDescriptor& descriptOpt);
		template <class L> void InitColorHistogram(const ColorHistogramParameters& colorHistOpt);

		Descriptor(const Descriptor&) = delete;
		Descriptor& operator=(const Descriptor&) = delete;

		std::unique_ptr<image::feature::descriptor::applier::BaseApplier> mBaseApplier;
	};

	typedef std::vector<Descriptor> DescriptorVector;
}

#endif // IMAGE_FEATURE_DESCRIPTOR_DESCRIPTOR_H_