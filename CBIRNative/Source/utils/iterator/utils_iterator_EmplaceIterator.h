#ifndef UTILS_ITERATOR_EMPLACEITERATOR_H_
#define UTILS_ITERATOR_EMPLACEITERATOR_H_

#include "utils/utils_stdlibwrapper.h"

namespace utils::iterator
{
	template<typename T>
	using RemoveReference = typename std::remove_reference<T>::type;
	template<typename T>
	using Bare = typename std::remove_cv<RemoveReference<T>>::type;

	template<class Container>
	class EmplaceIterator
	{
	public:

		using iterator_category = std::output_iterator_tag;
		using value_type        = typename Container::value_type;
		using difference_type   = std::ptrdiff_t;
		using pointer           = typename Container::value_type*;
		using reference         = typename Container::value_type&;

		explicit EmplaceIterator(Container& x) : mpContainer(&x) {}

		template <typename Tuple>
		EmplaceIterator& operator=(Tuple&& t)
		{
			emplace_TupleExpand(std::forward<Tuple>(t), std::make_index_sequence < std::tuple_size<Bare<Tuple>>{} > {});
			return *this;
		}

		template <>
		EmplaceIterator& operator=(EmplaceIterator& other)
		{
			mpContainer = other.mpContainer;
			return *this;
		}

		EmplaceIterator& operator*()     { return *this; }
		EmplaceIterator& operator++()    { return *this; }
		EmplaceIterator& operator++(int) { return *this; }

	private:

		template <class Tuple, size_t... Is>
		void emplace_TupleExpand(Tuple&& t, std::index_sequence<Is...>)
		{
			mpContainer->emplace_back(std::forward<std::tuple_element<Is,Tuple>::type>(std::get<Is>(std::forward<Tuple>(t)))...);
		}


		Container* mpContainer;
	};
}

#endif // UTILS_ITERATOR_EMPLACEITERATOR_H_