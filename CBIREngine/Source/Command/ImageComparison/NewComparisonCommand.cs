﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using MDL = CBIREngine.Source.Model;
using VM  = CBIREngine.Source.ViewModel;

namespace CBIREngine.Source.Command.ImageComparison
{
	public class NewComparisonCommand : ICommand
	{
		private readonly VM.MainViewModel mVM;
		private readonly MDL.MainModel    mModel;
		public event EventHandler CanExecuteChanged;

		public NewComparisonCommand(VM.MainViewModel vm)
		{
			mVM = vm;
			mModel = vm.Model;
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			// Create the new ImageComparisonModel
			MDL.ImageComparison.ImageComparisonModel newImageComparison = new MDL.ImageComparison.ImageComparisonModel();
			mModel.Documents.Add(newImageComparison);

			// Make it so that it is automatically selected
			mVM.SelectedDocumentIdx = mVM.Documents.IndexOf(mVM.Documents.First(x => x.Model == newImageComparison));
		}

		public void RaiseCanExecuteChanged()
		{
			if (CanExecuteChanged != null)
			{
				CanExecuteChanged(this, EventArgs.Empty);
			}
		}
	}
}
