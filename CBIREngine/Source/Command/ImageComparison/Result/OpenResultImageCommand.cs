﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using VM = CBIREngine.Source.ViewModel.ImageComparison.Result;

namespace CBIREngine.Source.Command.ImageComparison.Result
{
	public class OpenResultImageCommand : ICommand
	{
		public event EventHandler CanExecuteChanged;

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			VM.ResultViewModel vm = parameter as VM.ResultViewModel;
			string imagePath = vm.ComparedImagePath;

			Process.Start(imagePath);
		}

		public void RaiseCanExecuteChanged()
		{
			if (CanExecuteChanged != null)
			{
				CanExecuteChanged(this, EventArgs.Empty);
			}
		}
	}
}
