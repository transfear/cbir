﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using CMD = CBIREngine.Source.Command.ImageComparison.InputParams.FeatureDescriptor.Locality;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.Locality;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor.Locality
{
	public abstract class BaseLocalityViewModel : BaseViewModel
	{
		private readonly MDL.BaseLocalityModel     mModel;
		private readonly CMD.ChangeLocalityCommand mChangeLocalityCommand;	// Owned by parent BaseFeatureDescriptorViewModel

		public BaseLocalityViewModel(MDL.BaseLocalityModel model, CMD.ChangeLocalityCommand changeLocalityCmd)
		{
			mModel = model;
			mModel.PropertyChanged += Model_PropertyChanged;
			mChangeLocalityCommand = changeLocalityCmd;
		}

		#region DataBindings
		public CBIRNative.eLocalityDescription LocalityType
		{
			get { return mModel.LocalityDescription; }
			set
			{
				if (value != mModel.LocalityDescription)
					mChangeLocalityCommand.Execute(value);
			}
		}
		#endregion

		#region HelperFunctions
		public static BaseLocalityViewModel CreateFromModel(MDL.BaseLocalityModel model, CMD.ChangeLocalityCommand changeLocalityCommand)
		{
			switch (model.LocalityDescription)
			{
				case CBIRNative.eLocalityDescription.kLocalityDescription_Global:
					{
						return new GlobalLocalityViewModel(model as MDL.GlobalLocalityModel, changeLocalityCommand);
					}
				case CBIRNative.eLocalityDescription.kLocalityDescription_CenterAndCorners:
					{
						return new CenterAndCornersLocalityViewModel(model as MDL.CenterAndCornersLocalityModel, changeLocalityCommand);
					}
				case CBIRNative.eLocalityDescription.kLocalityDescription_3Horizontal:
					{
						return new ThreeHorizontalLocalityViewModel(model as MDL.ThreeHorizontalLocalityModel, changeLocalityCommand);
					}
				case CBIRNative.eLocalityDescription.kLocalityDescription_3x3:
					{
						return new ThreeByThreeLocalityViewModel(model as MDL.ThreeByThreeLocalityModel, changeLocalityCommand);
					}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported feature descriptor: {0}", model.LocalityDescription.ToString()));
			return null;
		}
		#endregion

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
		}
		#endregion
	}
}
