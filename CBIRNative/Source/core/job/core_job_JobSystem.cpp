#include "core_job_JobSystem.h"
#include "core_job_DummyJob.h"

namespace core::job
{
	JobSystem::JobSystem(u32 numWorkers)
	: mNumWorkers(numWorkers)
	{
		mpWorkers = std::make_unique<WorkerThread[]>(numWorkers);
		for (u32 i = 0; i < mNumWorkers; ++i)
			mpWorkers[i].Start(i, this);
	}

	JobSystem::~JobSystem()
	{
		for (u32 i = 0; i < mNumWorkers; ++i)
			mpWorkers[i].Stop();

		for (u32 i = 0; i < mNumWorkers; ++i)
			PushJob(new DummyJob());

		for (u32 i = 0; i < mNumWorkers; ++i)
			mpWorkers[i].Join();
	}

	u32 JobSystem::GetWorkerCount() const
	{
		return mNumWorkers;
	}

	Job* JobSystem::PopJob()
	{
		Job* pJob = nullptr;

		// Wait for a job to be available
		std::unique_lock<std::mutex> lockQueue(mQueueMutex);
		mQueueCV.wait(lockQueue, [&] ()
		{
			pJob = mQueue.Dequeue();
			return pJob != nullptr;
		});

		return pJob;
	}

	void JobSystem::PushJob(Job* pJob)
	{
		// Wake one worker
		{
			std::lock_guard<std::mutex> lockQueue(mQueueMutex);
			mQueue.Enqueue(pJob);
		}
		mQueueCV.notify_one();
	}

	void JobSystem::CancelAllJobs()
	{
		// Empty the job queue
		{
			std::lock_guard<std::mutex> lockQueue(mQueueMutex);
			Job* pJob = mQueue.Dequeue();
			while (pJob != nullptr)
			{
				Job* pNext = mQueue.Dequeue();
				delete pJob;
				pJob = pNext;
			}
		}
	}
}