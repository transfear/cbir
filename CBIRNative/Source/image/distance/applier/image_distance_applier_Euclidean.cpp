#include "image_distance_applier_Euclidean.h"

namespace image::distance::applier
{
	dist_elem Euclidean::Apply(u32 uiRegionIdx, const image::feature::Feature& first, const image::feature::Feature& second) const
	{
		const u32 uiFeatureCount = first.GetFeatureCount();
		const u32 uiRegionCount  = first.GetRegionCount();
		const u32 uiElemCount    = uiFeatureCount / uiRegionCount;
		const u32 uiElemStart    = uiRegionCount * uiRegionIdx;
		
		const feat_elem* pA = first.GetData()  + uiElemStart;
		const feat_elem* pB = second.GetData() + uiElemStart;

		dist_elem dSum = 0.0;
		for (u32 i = 0; i < uiElemCount; ++i)
		{
			const dist_elem a = pA[i];
			const dist_elem b = pB[i];
			const dist_elem dDist = abs(a - b);
			dSum += dDist;
		}
		
		return dSum;
	}
}

