﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Region.GLCM
{
	public class GLCMRegionModel : BaseRegionModel
	{

		public GLCMRegionModel(ref CBIRNative.RegionResult nativeRegion, float fWeight) : base(ref nativeRegion, fWeight)
		{
			// TODO
		}

		public override CBIRNative.eDescriptionMethod DescriptionMethod
		{
			get { return CBIRNative.eDescriptionMethod.kDescriptionMethod_GLCM; }
		}
	}
}
