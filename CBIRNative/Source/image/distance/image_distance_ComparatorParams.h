#ifndef IMAGE_DISTANCE_COMPARATORPARAMS_H_
#define IMAGE_DISTANCE_COMPARATORPARAMS_H_

#include "image/feature/image_feature_Feature.h"
#include "image/feature/image_feature_GeneratorParams.h"
#include "image/distance/image_distance_Descriptor.h"
#include "utils/utils_stdlibwrapper.h"

namespace image::distance
{
	class ComparatorParams
	{
	public:
		ComparatorParams(const image::feature::Feature& refFeature, const image::feature::GeneratorParams& genParams, const Descriptor& descriptor)
			: mpDescriptor(&descriptor), mpRefFeature(&refFeature), mGenParams(genParams)
		{
		}

		ComparatorParams(const ComparatorParams& other)
			: mpDescriptor(other.mpDescriptor), mpRefFeature(other.mpRefFeature), mGenParams(other.mGenParams)
		{
		}

		ComparatorParams(ComparatorParams&& other)
			: mpDescriptor(other.mpDescriptor), mpRefFeature(other.mpRefFeature), mGenParams(std::move(other.mGenParams))
		{
		}

		ComparatorParams& operator=(ComparatorParams&& other)
		{
			mpDescriptor = other.mpDescriptor;
			mpRefFeature = other.mpRefFeature;
			mGenParams   = std::move(other.mGenParams);

			return *this;
		}

		const image::feature::Feature& GetRefFeature() const
		{
			return *mpRefFeature;
		}

		const image::feature::GeneratorParams& GetGenParams() const
		{
			return mGenParams;
		}

		const Descriptor& GetDescriptor() const
		{
			return *mpDescriptor;
		}

	private:

		ComparatorParams& operator=(const ComparatorParams&) = delete;

		const Descriptor*               mpDescriptor;
		const image::feature::Feature*  mpRefFeature;
		image::feature::GeneratorParams mGenParams;
	};

	typedef std::vector<ComparatorParams> ComparatorParamsVector;
}

#endif // IMAGE_DISTANCE_COMPARATORPARAMS_H_