﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor.ColorHistogram
{
	public class ColorHistogramEnumMap
	{
		public CBIRNative.eHistogramColorSpace EnumValue { get; set; }
		public string StringValue { get; set; }

		static public List<ColorHistogramEnumMap> EnumList { get; set; } = new List<ColorHistogramEnumMap>()
		{
			new ColorHistogramEnumMap() { EnumValue = CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HSV,  StringValue = "HSV" },
			new ColorHistogramEnumMap() { EnumValue = CBIRNative.eHistogramColorSpace.kHistogramColorSpace_RGB,  StringValue = "RGB" },
			new ColorHistogramEnumMap() { EnumValue = CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HMMD, StringValue = "HMMD" },
		};
	}
}
