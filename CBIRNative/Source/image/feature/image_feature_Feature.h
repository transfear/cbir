#ifndef IMAGE_FEATURE_FEATURE_H_
#define IMAGE_FEATURE_FEATURE_H_

#include "utils/utils_stdlibwrapper.h"
#include "utils/utils_basictypes.h"
#include "image/feature/descriptor/image_feature_descriptor_Descriptor.h"

namespace image::feature
{
	class Feature
	{
	public:
		Feature(const descriptor::Descriptor& descript) : muiNumFeatures(descript.GetFeatureCount()), muiNumRegions(descript.GetRegionCount()) { }
		Feature(Feature&& other) : mFeatureBuffer(std::move(other.mFeatureBuffer)), muiNumFeatures(other.muiNumFeatures), muiNumRegions(other.muiNumRegions) { }
		Feature& operator=(Feature&& other)
		{
			mFeatureBuffer = std::move(other.mFeatureBuffer);
			muiNumFeatures = other.muiNumFeatures;
			muiNumRegions  = other.muiNumRegions;
			return *this;
		}

		void SetData(std::unique_ptr<feat_elem[]>& featureData)
		{
			mFeatureBuffer = std::move(featureData);
		}

		feat_elem* GetData()
		{
			return mFeatureBuffer.get();
		}

		const feat_elem* GetData() const
		{
			return mFeatureBuffer.get();
		}

		u32 GetDataSize() const
		{
			return sizeof(feat_elem) * muiNumFeatures;
		}

		u32 GetFeatureCount() const
		{
			return muiNumFeatures;
		}

		u32 GetRegionCount() const
		{
			return muiNumRegions;
		}

	private:
		Feature(const Feature&) = delete;
		Feature& operator=(const Feature&) = delete;

		std::unique_ptr<feat_elem[]> mFeatureBuffer;
		u32 muiNumFeatures = 0;
		u32 muiNumRegions = 0;
	};

	typedef std::vector<Feature> FeatureVector;
}

#endif // IMAGE_FEATURE_FEATURE_H_