﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Region.ColorHistogram
{
	public class HMMDHistogramRegionModel : BaseColorHistogramRegionModel
	{
		public HMMDHistogramRegionModel(ref CBIRNative.HMMDHistogramParameters nativeHisto, ref CBIRNative.RegionResult nativeRegion, float fWeight) : base(ref nativeRegion, fWeight) { }

		public override CBIRNative.eHistogramColorSpace ColorSpace
		{
			get { return CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HMMD; }
		}
	}
}
