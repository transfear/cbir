﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using MDL = CBIREngine.Source.Model.ImageComparison;
using VM  = CBIREngine.Source.ViewModel.ImageComparison;

namespace CBIREngine.Source.Command.ImageComparison
{
	public class StartStopCommand : ICommand
	{
		private readonly VM.ImageComparisonViewModel mVM;
		private readonly MDL.ImageComparisonModel    mModel;
		public event EventHandler CanExecuteChanged;

		public StartStopCommand(VM.ImageComparisonViewModel vm)
		{
			mVM    = vm;
			mModel = vm.Model;
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			if (mModel.Engine == null)
			{
				// Not running, start it
				mModel.Engine = Backend.ImageComparison.Engine.CreateEngine(mModel);
				mModel.Engine.Start();
			}
			else
			{
				// Already running, stop it
				// mModel.Engine will be released automatically when their task ends
				mModel.Engine.Stop();
			}
		}

		public void RaiseCanExecuteChanged()
		{
			if (CanExecuteChanged != null)
			{
				CanExecuteChanged(this, EventArgs.Empty);
			}
		}
	}
}
