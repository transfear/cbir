#include "filesys_Cache.h"
#include "image/feature/image_feature_Feature.h"
#include "utils/utils_String.h"
#include "utils/utils_externalwrapper.h"
#include "utils/utils_MemoryStream.h"

namespace filesys
{
	::utils::StringPtr Cache::GetCachePath(const char* cacheFolder, const FeatureDescriptor& descriptor, bool bCompressCache)
	{
		// Add the cache version to the base path
		std::filesystem::path cachePath(cacheFolder);
		cachePath /= Cache::smCacheVersion;
		
		// Append a SHA258 hash using the provided descriptor and wheter or not debug data is needed
		std::vector<u8> rawDataBuf;
		rawDataBuf.push_back(bCompressCache ? 0u : 1u);
		rawDataBuf.push_back(descriptor.descriptionMethod);
		rawDataBuf.push_back(descriptor.localityDescription);
		switch (descriptor.descriptionMethod)
		{
			case kDescriptionMethod_ColorHistogram:
			{
				rawDataBuf.push_back(descriptor.colorHist.colorSpace);
				switch (descriptor.colorHist.colorSpace)
				{
					case kHistogramColorSpace_RGB:
					{
						rawDataBuf.push_back(descriptor.colorHist.rgbParams.numBucketsR);
						rawDataBuf.push_back(descriptor.colorHist.rgbParams.numBucketsG);
						rawDataBuf.push_back(descriptor.colorHist.rgbParams.numBucketsB);
						break;
					}
					case kHistogramColorSpace_HSV:
					{
						rawDataBuf.push_back(descriptor.colorHist.hsvParams.numBucketsH);
						rawDataBuf.push_back(descriptor.colorHist.hsvParams.numBucketsS);
						rawDataBuf.push_back(descriptor.colorHist.hsvParams.numBucketsV);
						break;
					}
					case kHistogramColorSpace_HMMD:
					{
						rawDataBuf.push_back(descriptor.colorHist.hmmdParams.numBucketsH);
						rawDataBuf.push_back(descriptor.colorHist.hmmdParams.numBucketsMin);
						rawDataBuf.push_back(descriptor.colorHist.hmmdParams.numBucketsMax);
						rawDataBuf.push_back(descriptor.colorHist.hmmdParams.numBucketsD);
						break;
					}
					default:
					{
						assert(false);
					}
				}
				break;
			}
			case kDescriptionMethod_GLCM:
			default:
			{
				assert(false);
			}
		}
		
		std::string sha256Str;
		picosha2::hash256_hex_string(rawDataBuf, sha256Str);
		cachePath /= sha256Str;

		// Check if suffixed-version of the cache folder already exists, create it if it's not the case
		if (!std::filesystem::is_directory(cachePath))
		{
			std::error_code err;
			if (!std::filesystem::create_directories(cachePath, err))
			{
				assert(false);
				return {};
			}
		}

		const std::string cachedPathStr = cachePath.string();
		return ::utils::StringPtr(std::make_unique< ::utils::String>(cachedPathStr.c_str()));
	}

	bool Cache::Init()
	{
		// Compute cached file path
		const char* srcFile = mSrcFile->GetRaw();
		mCachedFilePath = Cache::GetCachedFilePath(mpSettings->GetRootSearchPath(), srcFile, mpSettings->GetCachePath());

		// Check if source file is valid
		const std::filesystem::path srcFilePath(srcFile);
		assert(std::filesystem::is_regular_file(srcFilePath));

		// Check if cached file is valid
		if (!std::filesystem::is_regular_file(mCachedFilePath))
			return false;

		// Check if cached file is more recent than source file
		const std::filesystem::file_time_type srcTime   = std::filesystem::last_write_time(srcFilePath);
		const std::filesystem::file_time_type cacheTime = std::filesystem::last_write_time(mCachedFilePath);
		
		const bool bCacheMoreRecent = cacheTime > srcTime;
		return bCacheMoreRecent;
	}

	void Cache::SaveData(const image::feature::Feature& imageFeature) const
	{
		const char* featureDataBuf  = reinterpret_cast<const char*>(imageFeature.GetData());
		const u32   featureDataSize = imageFeature.GetDataSize();

		// Concatenate data to write
		const u32 saveBufSize = featureDataSize + sizeof(featureDataSize);
		std::unique_ptr<u8[]> saveBuf(std::make_unique<u8[]>(saveBufSize));
		utils::MemoryStream saveStream(saveBuf.get(), saveBufSize);
		saveStream << featureDataSize;
		saveStream.Write(featureDataBuf, featureDataSize);

		// Check if destination directory exists, create it if it does not
		std::filesystem::path destDir(mCachedFilePath);
		assert(destDir.has_filename());
		destDir.remove_filename();
		if (!std::filesystem::is_directory(destDir))
		{
			std::error_code err;
			if (!std::filesystem::create_directories(destDir, err))
				assert(err.value() == 0);
		}
		
		// Check if we need to compress the data
		std::unique_ptr<u8[]> compressedData;
		char* pWriteData      = reinterpret_cast<char*>(saveBuf.get());
		u32   uiWriteDataSize = saveBufSize;
		if (mpSettings->GetCompressData())
		{
			const u32 uiSafeCompressSize = saveBufSize * 2;
			compressedData = std::make_unique<u8[]>(uiSafeCompressSize);
			
			unsigned long uiCompressedSize = uiSafeCompressSize;
			const int z_result = compress2(compressedData.get(), &uiCompressedSize, reinterpret_cast<Bytef*>(pWriteData), uiWriteDataSize, Z_BEST_COMPRESSION);
			assert(z_result == Z_OK);

			pWriteData = reinterpret_cast<char*>(compressedData.get());
			uiWriteDataSize = uiCompressedSize;
		}

		// Write file
		std::ofstream outFile(mCachedFilePath.generic_string(), std::ios_base::out | std::ios_base::binary);
		outFile.write(reinterpret_cast<const char*>(&saveBufSize), sizeof(saveBufSize)); // always write the uncompressed size at the start of the buffer
		outFile.write(pWriteData, uiWriteDataSize);
		outFile.close();

		// Make sure writing to cache occurred correctly
		assert(outFile.good());
	}

	void Cache::LoadData(image::feature::Feature& imageFeature) const
	{
		assert(std::filesystem::is_regular_file(mCachedFilePath));
		
		u32 uiFeatureDataSize;
		std::unique_ptr<feat_elem[]> featureData(std::make_unique<feat_elem[]>(imageFeature.GetFeatureCount()));

		// Read file
		u32 uiUncompressedSize = 0;
		std::ifstream inFile(mCachedFilePath.generic_string(), std::ios_base::in | std::ios_base::binary);
		inFile.read(reinterpret_cast<char*>(&uiUncompressedSize), sizeof(uiUncompressedSize));

		std::unique_ptr<u8[]> loadBuf(std::make_unique<u8[]>(uiUncompressedSize));
		utils::MemoryStream loadStream(loadBuf.get(), uiUncompressedSize);

		if (mpSettings->GetCompressData())
		{
			// File is compressed, uncompress it
			const u64 uiCompressedSize = std::filesystem::file_size(mCachedFilePath) - sizeof(uiUncompressedSize);
			std::unique_ptr<u8[]> compressedBuf(std::make_unique<u8[]>(uiCompressedSize));
			inFile.read(reinterpret_cast<char*>(compressedBuf.get()), static_cast<std::streamsize>(uiCompressedSize));
			assert((u64)inFile.gcount() == uiCompressedSize);
			
			unsigned long uiZSize = uiUncompressedSize;
			const int z_result = uncompress(loadBuf.get(), &uiZSize, compressedBuf.get(), static_cast<uLong>(uiCompressedSize));
			assert(z_result == Z_OK);
			assert(uiZSize == uiUncompressedSize);
		}
		else
		{
			inFile.read(reinterpret_cast<char*>(loadBuf.get()), uiUncompressedSize);
			assert((u64)inFile.gcount() == uiUncompressedSize);
		}
		assert(inFile);	// Make sure reading from cache occurred correctly
		inFile.close();

		// Read features
		loadStream >> uiFeatureDataSize;
		assert(uiFeatureDataSize == imageFeature.GetDataSize());
		loadStream.Read(featureData.get(), uiFeatureDataSize);
		imageFeature.SetData(featureData);
	}

	std::filesystem::path Cache::GetCachedFilePath(const char* rootSrcFolder, const char* srcFile, const char* cachePath)
	{
		std::filesystem::path srcFilePath(srcFile);
		std::filesystem::path pathToImage(srcFilePath.lexically_relative(rootSrcFolder));

		// TODO: Use path::lexically_relative when VS2017 supports it, it would be more robust. For now
		// simply skip the start of srcFile to use only the relative part.
		//srcFile += strlen(rootSrcFolder);

		std::filesystem::path cachedFilePath(cachePath);
		cachedFilePath /= pathToImage;
		cachedFilePath += Cache::smCacheSuffix;

		return cachedFilePath;
	}
}
