#include "utils_String.h"
#include "utils_stdlibwrapper.h"

namespace utils
{
	template <typename T>
	StringT<T>::StringT()
		: mRaw(new T[1]), mLength(0)
	{
		mRaw[0] = '\0';
	}

	template <typename T>
	StringT<T>::StringT(const T* toCopy)
	{
		mLength = strlen(toCopy);
		mRaw = new T[mLength + 1];
		strcpy(mRaw, toCopy);
	}

	template <typename T>
	StringT<T>::StringT(const StringT<T>& toCopy)
	{
		mLength = toCopy.GetLength();
		mRaw = new T[mLength + 1];
		strcpy(mRaw, toCopy.GetRaw());
	}

	template <typename T>
	StringT<T>::~StringT()
	{
		delete [] mRaw;
	}

	template <typename T>
	u64 StringT<T>::GetLength() const
	{
		return mLength;
	}

	template <typename T>
	T* StringT<T>::GetRaw()
	{
		return mRaw;
	}

	template <typename T>
	const T* StringT<T>::GetRaw() const
	{
		return mRaw;
	}

	template <typename T>
	StringT<T>::operator T*()
	{
		return GetRaw();
	}

	template <typename T>
	StringT<T>::operator const T*() const
	{
		return GetRaw();
	}

	template <typename T>
	void StringT<T>::Append(const T* toCopy)
	{
		const u64 suffixLen = strlen(toCopy);
		const T* prefix = mRaw;

		const u64 newLength = mLength + suffixLen;
		mRaw = new T[newLength + 1];

		strcpy(mRaw, prefix);
		strcat(mRaw, toCopy);
		mLength = newLength;

		delete [] prefix;
	}

	template <typename T>
	const bool StringT<T>::EndsWith(const T* end) const
	{
		const u64 endLen = strlen(end);
		if (endLen > mLength)
			return false;

		std::reverse_iterator<const T*> rBegin(end + endLen);
		std::reverse_iterator<const T*> rEnd(end);
		std::reverse_iterator<const T*> myBegin(mRaw + mLength);

		const bool endsWith = std::equal(rBegin, rEnd, myBegin);
		return endsWith;
	}

	template <typename T>
	bool StringT<T>::CaseInsensitiveCompare(const T& a, const T& b)
	{
		return toupper(a) == toupper(b);
	}

	template <typename T>
	const bool StringT<T>::EndsWithI(const T* end) const
	{
		const u64 endLen = strlen(end);
		if (endLen > mLength)
			return false;

		std::reverse_iterator<const T*> rBegin(end + endLen);
		std::reverse_iterator<const T*> rEnd(end);
		std::reverse_iterator<const T*> myBegin(mRaw + mLength);

		const bool endsWith = std::equal(rBegin, rEnd, myBegin, CaseInsensitiveCompare);
		return endsWith;
	}


	// Force template instantiation
	template class StringT<>;
}