﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.InputParams.DistanceDescriptor
{
	public abstract class BaseDistanceDescriptorModel : BaseModel
	{
		public abstract CBIRNative.eDistanceMethod DistanceMethod { get; }

		public static BaseDistanceDescriptorModel CreateFromEnumValue(CBIRNative.eDistanceMethod enumValue)
		{
			switch (enumValue)
			{
				case CBIRNative.eDistanceMethod.kDistanceMethod_ChiSquared:
					{
						return new ChiSquaredDistanceModel();
						break;
					}
				case CBIRNative.eDistanceMethod.kDistanceMethod_EarthMover:
					{
						return new EarthMoverDistanceModel();
						break;
					}
				case CBIRNative.eDistanceMethod.kDistanceMethod_Euclidean:
					{
						return new EuclideanDistanceModel();
						break;
					}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported distance descriptor: {0}", enumValue.ToString()));
			return null;
		}
	}
}
