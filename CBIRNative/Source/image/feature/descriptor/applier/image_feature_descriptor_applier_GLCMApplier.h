#ifndef IMAGE_DESCRIPTOR_GLCMAPPLIER_H_
#define IMAGE_DESCRIPTOR_GLCMAPPLIER_H_

#include "image_feature_descriptor_applier_BaseApplier.h"
#include "image/image_Image.h"
#include "image/feature/image_feature_Feature.h"

namespace image::feature::descriptor::applier
{
	class GLCMApplier : public BaseApplier
	{
	public:
		GLCMApplier();

		virtual void Apply(const image::Image& inputImage, feature::Feature& outputFeature) const override;
		virtual u32 GetRegionCount() const override;

	private:

	};
}

#endif // IMAGE_DESCRIPTOR_GLCMAPPLIER_H_