#ifndef IMAGE_LOCALITY_CENTERANDCORNERS_H_
#define IMAGE_LOCALITY_CENTERANDCORNERS_H_

#include "utils/utils_basictypes.h"
#include "utils/utils_stdlibwrapper.h"

namespace image::locality
{
	class CenterAndCorners
	{
	public:
		enum
		{
			kNumRegions = 5
		};

		static constexpr u32 GetRegionIndex(u32 x, u32 y, u32 w, u32 h)
		{
			const u32 ellipsisXRadius = w >> 2;
			const u32 ellipsisYRadius = h >> 2;

			const u32 ellipsisXRadiusSqr = ellipsisXRadius * ellipsisXRadius;
			const u32 ellipsisYRadiusSqr = ellipsisYRadius * ellipsisYRadius;

			const u32 centerX = w >> 1;
			const u32 centerY = h >> 1;

			const s32 distToCenterX = static_cast<s32>(x - centerX);
			const s32 distToCenterY = static_cast<s32>(y - centerY);

			const u32 distToCenterXSqr = static_cast<u32>(distToCenterX * distToCenterX);
			const u32 distToCenterYSqr = static_cast<u32>(distToCenterY * distToCenterY);

			// https://math.stackexchange.com/questions/76457/check-if-a-point-is-within-an-ellipse
			const f32 fRatio = (f32)distToCenterXSqr / (f32)ellipsisXRadiusSqr + (f32)distToCenterYSqr / (f32)ellipsisYRadiusSqr;
			if (fRatio <= 1.0f)
				return 4;

			if (x < centerX)
				return y < centerY ? 0u : 2u;

			return y < centerY ? 1u : 3u;
		}
	};
}

#endif // IMAGE_LOCALITY_CENTERANDCORNERS_H_