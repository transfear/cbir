﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CMD = CBIREngine.Source.Command.ImageComparison.InputParams.FeatureDescriptor;
using MDL = CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor.GLCM;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor.GLCM
{
	public class GLCMDescriptorViewModel : BaseFeatureDescriptorViewModel
	{
		public GLCMDescriptorViewModel(MDL.GLCMDescriptorModel model, CMD.ChangeFeatureDescriptorCommand changeDescriptCmd) : base(model, changeDescriptCmd)
		{
			model.PropertyChanged += Model_PropertyChanged;
		}

		#region ModelListener
		private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
		}
		#endregion
	}
}
