#include "image_distance_applier_EarthMover.h"

#include "external/FastEMD/emd_hat.h"

namespace
{
	enum EMDMode_e
	{
		kEMDMode_Hat,
		kEMDMode_Rubner,
	};

	constexpr EMDMode_e EMD_MODE = kEMDMode_Hat;
}

namespace image::distance::applier
{
	dist_elem EarthMover::Apply(u32 uiRegionIdx, const image::feature::Feature& first, const image::feature::Feature& second) const
	{
		// TODO: http://robotics.stanford.edu/~rubner/emd/emd.c

		const u32 uiFeatureCount = first.GetFeatureCount();
		const u32 uiRegionCount  = first.GetRegionCount();
		const u32 uiElemCount    = uiFeatureCount / uiRegionCount;
		const u32 uiElemStart    = uiRegionCount * uiRegionIdx;

		const feat_elem* pA = first.GetData()  + uiElemStart;
		const feat_elem* pB = second.GetData() + uiElemStart;

		dist_elem dDistance = 0.0;
		if constexpr (EMD_MODE == kEMDMode_Hat)
		{
			// Fill a matrix of ground distances between a and b
			std::vector<std::vector<dist_elem>> distMtx(uiElemCount, std::vector<dist_elem>(uiElemCount, 0));
			for (u32 uiY = 0; uiY < uiElemCount; ++uiY)
			{
				const dist_elem a = pA[uiY];
				std::vector<dist_elem>& rowVec = distMtx[uiY];
				for (u32 uiX = 0; uiX < uiElemCount; ++uiX)
				{
					const dist_elem b = pB[uiX];
					const dist_elem dDist = abs(a - b);
					rowVec[uiX] = dDist;
				}
			}

			// Move features to a temp vector - TODO: modify emd_hat to support raw pointers
			std::vector<dist_elem> vecA(pA, pA + uiElemCount);
			std::vector<dist_elem> vecB(pB, pB + uiElemCount);
			EmdHat::emd_hat_gd_metric<dist_elem, EmdHat::NO_FLOW> generator;
			dDistance = generator(vecA, vecB, distMtx);
		}
	

		return dDistance;
	}
}

