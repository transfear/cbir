#ifndef IMAGE_COLORSPACE_RGB_H_
#define IMAGE_COLORSPACE_RGB_H_

#include "CBIRNative.h"
#include "utils/utils_basictypes.h"

namespace image::colorspace
{
	class RGB
	{
	public:

		using InputOptions = RGBHistogramParameters;
		enum { NumComponents = 3 };

		static void GetDimSize(const InputOptions& inOptions, u32 outDims[NumComponents])
		{
			outDims[0] = inOptions.numBucketsR;
			outDims[1] = inOptions.numBucketsG;
			outDims[2] = inOptions.numBucketsB;
		}

		static u32 GetVolumeSize(const InputOptions& inOptions)
		{
			return static_cast<u32>(inOptions.numBucketsR * inOptions.numBucketsG * inOptions.numBucketsB);
		}

		template <class SrcColorSpace>
		static void U8ToF32(const u8* inRGB, f32 outRGB[NumComponents]);

		template <>
		void U8ToF32<RGB>(const u8* inRGB, f32 outRGB[NumComponents])
		{
			outRGB[0] = static_cast<f32>(inRGB[0]) / 255.0f;
			outRGB[1] = static_cast<f32>(inRGB[1]) / 255.0f;
			outRGB[2] = static_cast<f32>(inRGB[2]) / 255.0f;
		}
	};
}

#endif // IMAGE_COLORSPACE_RGB_H_