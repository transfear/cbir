﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Region.ColorHistogram
{
	public abstract class BaseColorHistogramRegionModel : BaseRegionModel
	{
		public BaseColorHistogramRegionModel(ref CBIRNative.RegionResult nativeRegion, float fWeight) : base(ref nativeRegion, fWeight) { }

		public abstract CBIRNative.eHistogramColorSpace ColorSpace { get; }

		public override CBIRNative.eDescriptionMethod DescriptionMethod
		{
			get { return CBIRNative.eDescriptionMethod.kDescriptionMethod_ColorHistogram; }
		}

		public static BaseColorHistogramRegionModel CreateFromParams(ref CBIRNative.ColorHistogramParameters nativeHist, ref CBIRNative.RegionResult nativeRegion, float fWeight)
		{
			CBIRNative.eHistogramColorSpace enumValue = nativeHist.colorSpace;
			switch (enumValue)
			{
				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_RGB:
				{
					return new RGBHistogramRegionModel(ref nativeHist.union.rgbParams, ref nativeRegion, fWeight);
				}
				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HSV:
				{
					return new HSVHistogramRegionModel(ref nativeHist.union.hsvParams, ref nativeRegion, fWeight);
				}
				case CBIRNative.eHistogramColorSpace.kHistogramColorSpace_HMMD:
				{
					return new HMMDHistogramRegionModel(ref nativeHist.union.hmmdParams, ref nativeRegion, fWeight);
				}
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Unsupported descriptor method: {0}", enumValue.ToString()));
			return null;
		}

	}
}
