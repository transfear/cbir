#ifndef UTILS_EXTERNALWRAPPER_H_
#define UTILS_EXTERNALWRAPPER_H_

#include "utils_stdlibwrapper.h"

#pragma warning(push, 3)

#pragma warning(disable: 4244)
#pragma warning(disable: 4365)
#pragma warning(disable: 4577)
#pragma warning(disable: 4820)

#include "jpeglib.h"
#include "external/PicoSHA2/picosha2.h"
#include "zlib.h"

#pragma warning(pop)

#endif // UTILS_EXTERNALWRAPPER_H_