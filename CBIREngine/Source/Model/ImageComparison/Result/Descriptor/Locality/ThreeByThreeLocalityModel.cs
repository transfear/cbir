﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using CBIREngine.Source.Utils;

namespace CBIREngine.Source.Model.ImageComparison.Result.Descriptor.Locality
{
	public class ThreeByThreeLocalityModel : BaseResultDescriptorModel
	{
		private readonly Region.BaseRegionModel mTopLeft;
		private readonly Region.BaseRegionModel mTopCenter;
		private readonly Region.BaseRegionModel mTopRight;
		private readonly Region.BaseRegionModel mCenterLeft;
		private readonly Region.BaseRegionModel mCenter;
		private readonly Region.BaseRegionModel mCenterRight;
		private readonly Region.BaseRegionModel mBotLeft;
		private readonly Region.BaseRegionModel mBotCenter;
		private readonly Region.BaseRegionModel mBotRight;

		public ThreeByThreeLocalityModel(ref CBIRNative.DescriptorResult nativeDescript) : base(ref nativeDescript)
		{
			System.Diagnostics.Debug.Assert(nativeDescript.inParams.feature.localityDescription == CBIRNative.eLocalityDescription.kLocalityDescription_3x3);
			System.Diagnostics.Debug.Assert(nativeDescript.numRegions == 9);

			CBIRNative.RegionResult[] regionArray = MarshalUtils.GetArray<CBIRNative.RegionResult>(nativeDescript.pRegionResults, 9);
			unsafe
			{ 
				mTopLeft     = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[0], nativeDescript.inParams.feature.pRegionWeights[0]);
				mTopCenter   = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[1], nativeDescript.inParams.feature.pRegionWeights[1]);
				mTopRight    = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[2], nativeDescript.inParams.feature.pRegionWeights[2]);
				mCenterLeft  = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[3], nativeDescript.inParams.feature.pRegionWeights[3]);
				mCenter      = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[4], nativeDescript.inParams.feature.pRegionWeights[4]);
				mCenterRight = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[5], nativeDescript.inParams.feature.pRegionWeights[5]);
				mBotLeft     = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[6], nativeDescript.inParams.feature.pRegionWeights[6]);
				mBotCenter   = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[7], nativeDescript.inParams.feature.pRegionWeights[7]);
				mBotRight    = Region.BaseRegionModel.CreateFromParams(ref nativeDescript, ref regionArray[8], nativeDescript.inParams.feature.pRegionWeights[8]);
			}
		}

		public override int NumRegions
		{
			get { return 9; }
		}

		public override CBIRNative.eLocalityDescription LocalityType
		{
			get { return CBIRNative.eLocalityDescription.kLocalityDescription_3x3; }
		}

		public Region.BaseRegionModel TopLeft
		{
			get { return mTopLeft; }
		}

		public Region.BaseRegionModel TopCenter
		{
			get { return mTopCenter; }
		}

		public Region.BaseRegionModel TopRight
		{
			get { return mTopRight; }
		}

		public Region.BaseRegionModel CenterLeft
		{
			get { return mCenterLeft; }
		}

		public Region.BaseRegionModel Center
		{
			get { return mCenter; }
		}

		public Region.BaseRegionModel CenterRight
		{
			get { return mCenterRight; }
		}

		public Region.BaseRegionModel BotLeft
		{
			get { return mBotLeft; }
		}

		public Region.BaseRegionModel BotCenter
		{
			get { return mBotCenter; }
		}

		public Region.BaseRegionModel BotRight
		{
			get { return mBotRight; }
		}

		public override Region.BaseRegionModel GetRegionAt(int idx)
		{
			switch (idx)
			{
				case 0:
					return TopLeft;
				case 1:
					return TopCenter;
				case 2:
					return TopRight;
				case 3:
					return CenterLeft;
				case 4:
					return Center;
				case 5:
					return CenterRight;
				case 6:
					return BotLeft;
				case 7:
					return BotCenter;
				case 8:
					return BotRight;
			}

			System.Diagnostics.Debug.Assert(false, String.Format("Invalid weight index: {0}", idx));
			return null;
		}
	}
}
