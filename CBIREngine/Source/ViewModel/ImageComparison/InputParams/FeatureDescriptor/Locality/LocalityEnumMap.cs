﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor.Locality
{
	public class LocalityEnumMap
	{
		public CBIRNative.eLocalityDescription EnumValue { get; set; }
		public string StringValue { get; set; }

		static public List<LocalityEnumMap> EnumList { get; set; } = new List<LocalityEnumMap>()
		{
			new LocalityEnumMap() { EnumValue = CBIRNative.eLocalityDescription.kLocalityDescription_Global,           StringValue = "Global" },
			new LocalityEnumMap() { EnumValue = CBIRNative.eLocalityDescription.kLocalityDescription_CenterAndCorners, StringValue = "Center and corners" },
			new LocalityEnumMap() { EnumValue = CBIRNative.eLocalityDescription.kLocalityDescription_3Horizontal,      StringValue = "3 horizontal bands" },
			new LocalityEnumMap() { EnumValue = CBIRNative.eLocalityDescription.kLocalityDescription_3x3,              StringValue = "3x3 grid" }
		};
	}
}
