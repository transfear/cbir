﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CBIREngine.Source.Utils;

namespace CBIREngine.Source.Model.ImageComparison.Result
{
	public class ResultModel : BaseModel
	{
		private string mReferenceImagePath;
		private string mComparedImagePath;
		private double mDistToRef;

		private readonly ObservableRangeCollection<Descriptor.BaseResultDescriptorModel> mDescriptors = new ObservableRangeCollection<Descriptor.BaseResultDescriptorModel>();

		public ResultModel() { }

		public string ReferenceImagePath
		{
			get { return mReferenceImagePath; }
			set
			{
				mReferenceImagePath = value;
				OnPropertyChanged();
			}
		}

		public string ComparedImagePath
		{
			get { return mComparedImagePath; }
			set
			{
				mComparedImagePath = value;
				OnPropertyChanged();
			}
		}

		public double DistToRef
		{
			get { return mDistToRef; }
			set
			{
				mDistToRef = value;
				OnPropertyChanged();
			}
		}

		public ObservableRangeCollection<Descriptor.BaseResultDescriptorModel> Descriptors
		{
			get { return mDescriptors; }
		}
	}
}
