﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

using API;
using VM = CBIREngine.Source.ViewModel.ImageComparison.InputParams.FeatureDescriptor;

namespace CBIREngine.Source.View.ImageComparison.InputParams.FeatureDescriptor
{
	class FeatureDescriptorDataTemplateSelector : DataTemplateSelector
	{
		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			FrameworkElement fe = container as FrameworkElement;
			VM.BaseFeatureDescriptorViewModel vm = item as VM.BaseFeatureDescriptorViewModel;

			if (vm != null)
			{
				switch (vm.FeatureType)
				{
					case CBIRNative.eDescriptionMethod.kDescriptionMethod_ColorHistogram:
						{
							return fe.FindResource("ColorHistogramDataTemplate") as DataTemplate;
						}

					case CBIRNative.eDescriptionMethod.kDescriptionMethod_GLCM:
						{
							return fe.FindResource("GLCMDataTemplate") as DataTemplate;
						}
				}
			}

			return base.SelectTemplate(item, container);
		}
	}
}
