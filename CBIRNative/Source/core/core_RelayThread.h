#ifndef CORE_RELAYTHREAD_H_
#define CORE_RELAYTHREAD_H_

#include "utils/utils_basictypes.h"
#include "utils/utils_stdlibwrapper.h"

namespace core
{
	template <typename ResultType, typename CancelPredicate>
	class RelayThread
	{
	public:

		using RelayCallback = bool(*)(u64, ResultType const* const*, u32);
		using Element      = std::unique_ptr<u8[]>;
		using ElementVec   = std::vector<Element>;
		
		RelayThread(RelayCallback cb, u64 userData, CancelPredicate cancelPred) : mCallback(cb), mUserData(userData), mCancelPred(cancelPred)
		{
			mElements.reserve(INITIAL_RESERVE);
			mThread = std::thread([this] { this->DoWork(); });
		}

		~RelayThread()
		{
			mbQuit = true;
			mElementCV.notify_one();
			mThread.join();
		}
		
		void PushElement(Element& elem)
		{
			{
				std::lock_guard<std::mutex> lockElems(mElementMutex);
				mElements.push_back(std::move(elem));
			}
			mElementCV.notify_one();
		}

	private:
		void DoWork()
		{
			ElementVec localVec;
			localVec.reserve(INITIAL_RESERVE);

			const u64 userData = mUserData;
			while (!mbQuit || !mElements.empty())
			{
				{
					// Wait for something to relay
					std::unique_lock<std::mutex> lockElems(mElementMutex);
					mElementCV.wait(lockElems, [&]
					{
						return !mElements.empty() || mbQuit;
					});

					// Swap content with gather vector
					localVec.swap(mElements);
				}
				
				// The following will only work if unique_ptr<T> is same size as ResultType*
				static_assert(sizeof(std::unique_ptr<u8[]>) == sizeof(ResultType*));
				ResultType const* const* ppResults  = reinterpret_cast<ResultType const* const*>(localVec.data());
				const u32       numResults = static_cast<u32>(localVec.size());
				
				bool bContinue = mCallback(userData, ppResults, numResults);

				if (!bContinue)
				{
					// Handle cases when the user wants to abort
					mbQuit = true;
					mCancelPred();
				}

				localVec.clear();

				// Elements in localVec must be trivially copyable since their dtors won't be called
				static_assert(std::is_trivially_copyable_v<ResultType>);
			}
		}

		RelayThread(const RelayThread&) = delete;
		RelayThread& operator=(const RelayThread&) = delete;

		static constexpr u32 INITIAL_RESERVE = 1024;

		RelayCallback   mCallback;
		ElementVec      mElements;
		u64             mUserData;
		CancelPredicate mCancelPred;
		
		std::mutex              mElementMutex;
		std::condition_variable mElementCV;

		std::thread mThread;

		volatile bool mbQuit = false;
	};
}

#endif // CORE_RELAYTHREAD_H_