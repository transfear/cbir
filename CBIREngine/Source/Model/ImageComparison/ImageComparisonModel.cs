﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CBIREngine.Source.Backend.ImageComparison;
using CBIREngine.Source.Utils;

namespace CBIREngine.Source.Model.ImageComparison
{
    public class ImageComparisonModel : BaseDocumentModel
    {
		private readonly InputParams.InputParamsModel                  mInputParams = new InputParams.InputParamsModel();
		private readonly ObservableRangeCollection<Result.ResultModel> mResults     = new ObservableRangeCollection<Result.ResultModel>();
		private Engine mEngine;

		public ImageComparisonModel() { }

		public InputParams.InputParamsModel InputParams
		{
			get { return mInputParams; }
		}

		public ObservableRangeCollection<Result.ResultModel> Results
		{
			get { return mResults; }
		}

		public Engine Engine
		{
			get { return mEngine; }
			set
			{
				mEngine = value;
				OnPropertyChanged();
			}
		}
	}
}
