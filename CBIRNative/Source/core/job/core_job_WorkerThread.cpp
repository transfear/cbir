#include "core_job_WorkerThread.h"
#include "core_job_Job.h"
#include "core_job_JobSystem.h"

namespace core::job
{
	WorkerThread::WorkerThread()
	{

	}

	WorkerThread::~WorkerThread()
	{

	}

	void WorkerThread::Start(u32 workerId, JobSystem* pJobSys)
	{
		mWorkerId = workerId;
		mpJobSys  = pJobSys;

		// Start the thread
		assert(!mbThreadRunning);
		mbThreadRunning = true;
		mThread = std::thread( [this] { this->DoWork(); } );
	}

	void WorkerThread::Stop()
	{
		assert(mbThreadRunning);
		mbThreadRunning = false;
	}

	void WorkerThread::Join()
	{
		mThread.join();
	}

	u32 WorkerThread::GetId() const
	{
		return mWorkerId;
	}

	void WorkerThread::DoWork()
	{
		JobSystem* pJobSys = mpJobSys;

		while (mbThreadRunning)
		{
			JobPtr pJob(pJobSys->PopJob());
			if (pJob.get() != nullptr)
				pJob->Execute();
		}
	}
}