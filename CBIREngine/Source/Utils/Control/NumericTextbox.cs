﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CBIREngine.Source.Utils.Control
{
    public class NumericTextbox : TextBox
    {
		public bool IsFloatAllowed { get; set; } = false;

		public NumericTextbox()
		{
			PreviewTextInput += PreviewTextInputHandler;
			DataObject.AddPastingHandler(this, TextBoxPastingHandler);
		}

		private bool IsTextAllowed(string text)
		{
			if (this.Text == string.Empty && text == "-")
				return true;

			if (this.IsFloatAllowed)
			{
				if (this.Text != string.Empty && text == ".")
					return true;

				float fDummy;
				return float.TryParse(text, out fDummy);
			}

			int iDummy;
			return int.TryParse(text, out iDummy);
		}

		private void PreviewTextInputHandler(object sender, TextCompositionEventArgs e)
		{
			e.Handled = !IsTextAllowed(e.Text);
		}

		private void TextBoxPastingHandler(object sender, DataObjectPastingEventArgs e)
		{
			if (e.DataObject.GetDataPresent(typeof(String)))
			{
				String text = (String)e.DataObject.GetData(typeof(String));
				if (!IsTextAllowed(text))
				{
					e.CancelCommand();
				}
			}
			else
			{
				e.CancelCommand();
			}
		}
    }
}
