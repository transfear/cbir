#ifndef IMAGE_DISTANCE_APPLIER_BASEAPPLIER_H_
#define IMAGE_DISTANCE_APPLIER_BASEAPPLIER_H_

#include "image/feature/image_feature_Feature.h"

namespace image::distance::applier
{
	class BaseApplier
	{
	public:
		virtual ~BaseApplier() { }
		virtual dist_elem Apply(u32 uiRegionIdx, const image::feature::Feature& first, const image::feature::Feature& second) const = 0;
	};
}

#endif // IMAGE_DISTANCE_APPLIER_BASEAPPLIER_H_