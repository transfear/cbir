﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;
using Dist = CBIREngine.Source.Model.ImageComparison.InputParams.DistanceDescriptor;
using Feat = CBIREngine.Source.Model.ImageComparison.InputParams.FeatureDescriptor;

namespace CBIREngine.Source.Model.ImageComparison.InputParams
{
	public class DescriptorParamsModel : BaseModel
	{
		private float mfWeight = 1.0f;
		private FeatureDescriptor.BaseFeatureDescriptorModel   mFeature  = new FeatureDescriptor.ColorHistogram.HSVHistogramModel();
		private DistanceDescriptor.BaseDistanceDescriptorModel mDistance = new DistanceDescriptor.ChiSquaredDistanceModel();
		
		public DescriptorParamsModel() { }
		public DescriptorParamsModel(ref CBIRNative.DescriptorParameters nativeDescript)
		{
			mfWeight  = nativeDescript.weight;
			mFeature  = Feat.BaseFeatureDescriptorModel.CreateFromParams(ref nativeDescript.feature);
			mDistance = Dist.BaseDistanceDescriptorModel.CreateFromEnumValue(nativeDescript.distance.distanceMethod);
		}

		public float Weight
		{
			get { return mfWeight; }
			set
			{
				mfWeight = value;
				OnPropertyChanged();
			}
		}

		public FeatureDescriptor.BaseFeatureDescriptorModel FeatureDescriptor
		{
			get { return mFeature; }
			set
			{
				mFeature = value;
				OnPropertyChanged();
			}
		}

		public DistanceDescriptor.BaseDistanceDescriptorModel DistanceDescriptor
		{
			get { return mDistance; }
			set
			{
				mDistance = value;
				OnPropertyChanged();
			}
		}
	}
}
