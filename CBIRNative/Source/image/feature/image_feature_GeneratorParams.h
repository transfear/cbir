#ifndef IMAGE_FEATURE_GENERATORPARAMS_H_
#define IMAGE_FEATURE_GENERATORPARAMS_H_

#include "filesys/filesys_Cache.h"
#include "image/feature/descriptor/image_feature_descriptor_Descriptor.h"
#include "utils/utils_String.h"
#include "utils/utils_stdlibwrapper.h"

namespace image::feature
{
	class GeneratorParams
	{
	public:
		GeneratorParams(const filesys::CacheSettings& cacheSettings, const char* imagePath, const descriptor::Descriptor& imageDescriptor)
			: mpDescriptor(&imageDescriptor), mCache(cacheSettings, imagePath)
		{
		}

		GeneratorParams(const GeneratorParams& other)
			: mpDescriptor(other.mpDescriptor), mCache(other.GetCache())
		{
		}

		GeneratorParams(GeneratorParams&& other)
			: mpDescriptor(other.mpDescriptor), mCache(std::move(other.mCache))
		{
		}

		GeneratorParams& operator=(GeneratorParams&& other)
		{
			mpDescriptor         = other.mpDescriptor;
			mCache               = std::move(other.mCache);
			return *this;
		}

		const descriptor::Descriptor& GetDescriptor() const
		{
			return *mpDescriptor;
		}

		const filesys::Cache& GetCache() const
		{
			return mCache;
		}

		filesys::Cache& GetCache()
		{
			return mCache;
		}

	private:
		GeneratorParams& operator=(const GeneratorParams&) = delete;

		const descriptor::Descriptor* mpDescriptor;
		filesys::Cache                mCache;
	};

	typedef std::vector<GeneratorParams> GeneratorParamsVector;
}

#endif // IMAGE_FEATURE_GENERATORPARAMS_H_