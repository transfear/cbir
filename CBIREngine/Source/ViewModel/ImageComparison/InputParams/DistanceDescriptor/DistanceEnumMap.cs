﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using API;

namespace CBIREngine.Source.ViewModel.ImageComparison.InputParams.DistanceDescriptor
{
	public class DistanceEnumMap
	{
		public CBIRNative.eDistanceMethod EnumValue { get; set; }
		public string StringValue { get; set; }

		static public List<DistanceEnumMap> EnumList { get; set; } = new List<DistanceEnumMap>()
		{
			new DistanceEnumMap() { EnumValue = CBIRNative.eDistanceMethod.kDistanceMethod_Euclidean,  StringValue = "Euclidean" },
			new DistanceEnumMap() { EnumValue = CBIRNative.eDistanceMethod.kDistanceMethod_ChiSquared, StringValue = "Chi squared" },
			new DistanceEnumMap() { EnumValue = CBIRNative.eDistanceMethod.kDistanceMethod_EarthMover, StringValue = "Earth mover" },
		};
	}
}
